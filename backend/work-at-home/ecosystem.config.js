module.exports = {
  apps: [
    {
      name: 'Work at home',
      script: 'index.js',
      instances: 1,
      autorestart: true,
      watch: false,
      max_memory_restart: '1G',
      env: {
        NODE_ENV: 'development',
      },
      env_production: {
        NODE_ENV: 'production',
      },
    },
  ],

  deploy: {
    production: {
      user: 'toma',
      host: '206.189.60.2',
      ref: 'origin/develop',
      repo: 'git@gitlab.com:timraketa/work-at-home.git',
      path: '/home/toma/',
      ssh_options: ['StrictHostKeyChecking=no', 'PasswordAuthentication=no'],
      'pre-deploy-local': '',
      'post-deploy':
        'cd infrastructure && docker-compose down --volumes && docker-compose up -d && cd ../frontend/work-at-home && npm install && npm run build && cd ../../backend/work-at-home && npm install && npm run build && pm2 startOrRestart ecosystem.config.js --env production',
    },
  },
}
