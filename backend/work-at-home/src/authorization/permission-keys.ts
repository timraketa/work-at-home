export const enum PermissionKeys {
  // admin
  CreateUser = 'CreateUser',
  DeleteUsers = 'DeleteUsers',

  // project manager
  CreateProject = 'CreateProject',
  DeleteProject = 'DeleteProject',
  UpdateProject = 'UpdateProject',
  UpdateChecklist = 'UpdateChecklist',
  CreateChecklist = 'CreateChecklist',
  CreateCheckpoint = 'CreateCheckpoint',
  UpdateCheckpoint = 'UpdateCheckpoint',
  DeleteCheckpoint = 'DeleteCheckpoint',
  DeleteChecklist = 'DeleteChecklist',
  UpdateTeam = 'UpdateTeam',
  CreateTeam = 'CreateTeam',
  DeleteTeam = 'DeleteTeam',
  ProjectStatistics = 'ProjectStatistics',

  // worker
  ListUsers = 'ListUsers',
  WorkDay = 'WorkDay',
  WorkHour = 'WorkHour',
  EditUser = 'EditUser',
  ListProjects = 'ListProjects',
  UserStatistics = 'UserStatistics',
  ListWorkDays = 'ListWorkDays',
  UpdateWorkDays = 'UpdateWorkDays'
}
