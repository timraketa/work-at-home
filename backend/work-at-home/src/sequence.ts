import {
  FindRoute,
  InvokeMethod,
  InvokeMiddleware,
  ParseParams,
  Reject,
  RequestContext,
  Send,
  SequenceActions,
  SequenceHandler
} from '@loopback/rest'
import {
  AuthenticateFn,
  AuthenticationBindings,
  AUTHENTICATION_STRATEGY_NOT_FOUND,
  USER_PROFILE_NOT_FOUND
} from '@loopback/authentication'
import { inject } from '@loopback/core'
import path from 'path'

export class MySequence implements SequenceHandler {
  @inject(SequenceActions.INVOKE_MIDDLEWARE, { optional: true })
  protected invokeMiddleware: InvokeMiddleware = () => false

  constructor(
    @inject(SequenceActions.FIND_ROUTE) protected findRoute: FindRoute,
    @inject(SequenceActions.PARSE_PARAMS) protected parseParams: ParseParams,
    @inject(SequenceActions.INVOKE_METHOD) protected invoke: InvokeMethod,
    @inject(SequenceActions.SEND) public send: Send,
    @inject(SequenceActions.REJECT) public reject: Reject,
    @inject(AuthenticationBindings.AUTH_ACTION)
    protected authenticateRequest: AuthenticateFn
  ) {}

  async handle(context: RequestContext) {
    try {
      const { request, response } = context
      const route = this.findRoute(request)
      const finished = await this.invokeMiddleware(context)
      if (finished) return

      await this.authenticateRequest(request)

      const args = await this.parseParams(request, route)
      const result = await this.invoke(route, args)
      this.send(response, result)
    } catch (err) {
      if (
        err.code === AUTHENTICATION_STRATEGY_NOT_FOUND ||
        err.code === USER_PROFILE_NOT_FOUND
      ) {
        Object.assign(err, { statusCode: 401 /* Unauthorized */ })
      }

      if (err.statusCode === 404) {
        context.response.sendFile(
          path.join(
            __dirname,
            '../../../frontend/work-at-home/build/index.html'
          )
        )
      } else {
        this.reject(context, err)
      }
    }
  }
}
