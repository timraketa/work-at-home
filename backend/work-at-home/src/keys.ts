import { AuthenticationBindings } from '@loopback/authentication'
import { BindingKey } from '@loopback/core'
import { JWTStrategy } from './authentication-stratgies/jwt-stratgies'
import { PasswordHasher } from './services/hash.password'
import { JWTService } from './services/jwt-service'
import { MyUserService } from './services/user-service'
import { EmailService } from './services/email-service'

export namespace TokenServiceConstants {
  export const TOKEN_SECRET_VALUE = '138asda8213'
  export const TOKEN_EXPIRES_IN_VALUE = '7h'
}
export namespace TokenServiceBindings {
  export const TOKEN_SECRET = BindingKey.create<string>(
    'authentication.jwt.secret'
  )
  export const TOKEN_EXPIRES_IN = BindingKey.create<string>(
    'authentication.jwt.expiresIn'
  )
  export const TOKEN_SERVICE = BindingKey.create<JWTService>(
    'services.jwt.service'
  )
}

export namespace PasswordHasherBindings {
  export const PASSWORD_HASHER = BindingKey.create<PasswordHasher>(
    'services.hasher'
  )
  export const ROUNDS = BindingKey.create<number>('services.hasher.rounds')
}

export namespace UserServiceBindings {
  export const USER_SERVICE = BindingKey.create<MyUserService>(
    'services.user.service'
  )
}

export namespace EmailServiceBindings {
  export const SEND_MAIL = BindingKey.create<EmailService>(
    'services.email.send'
  )
}
