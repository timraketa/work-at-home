import nodemailer from 'nodemailer'

export class EmailService {
  constructor() {}

  // create readonly transporter object using the default SMTP transport
  readonly transporter = nodemailer.createTransport({
    service: process.env.EMAIL_SERVICE_MAIL_SERVICE,
    auth: {
      user: process.env.EMAIL_SERVICE_AUTH_USER,
      pass: process.env.EMAIL_SERVICE_AUTH_PASS
    }
  })

  async notRegisteredSendMail(mail: string, username: string) {
    await this.transporter.sendMail({
      from: `"Work-at-home" <${process.env.EMAIL_SERVICE_AUTH_USER}>`,
      to: mail,
      subject: '[Work@Home] Come define your credentials!',
      text: `Please proceed to this link to fulfill your account!\nHere is your username: ${username}`,
      html: `<p>Please proceed to this <a href=\"${process.env.EMAIL_SERVICE_SIGNUP_URL}/${username}\">link (${process.env.EMAIL_SERVICE_SIGNUP_URL}/${username})</a> to fulfill your account!<br>Here is your username: ${username}</p>`
    })
  }

  async registeredSendMail(mail: string, firm: string) {
    await this.transporter.sendMail({
      from: `"Work-at-home" <${process.env.EMAIL_SERVICE_AUTH_USER}>`,
      to: mail,
      subject: '[Work@Home] New firm connected!',
      text: `You are now connected to ${firm}`,
      html: `<p>You are now connected to ${firm}</p>`
    })
  }
}
