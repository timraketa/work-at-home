import { inject } from '@loopback/core'
import { repository } from '@loopback/repository'
import { HttpErrors } from '@loopback/rest'
import { PermissionKeys } from '../authorization/permission-keys'
import { PasswordHasherBindings } from '../keys'
import { User } from '../models'
import { RoleRepository } from '../repositories/role.repository'
import {
  Credentials,
  UserProfile,
  UserRepository
} from '../repositories/user.repository'
import { BcryptHasher } from './hash.password'

export class MyUserService {
  constructor(
    @repository(UserRepository)
    public userRepository: UserRepository,
    @repository(RoleRepository)
    public roleRepository: RoleRepository,

    // @inject('service.hasher')
    @inject(PasswordHasherBindings.PASSWORD_HASHER)
    public hasher: BcryptHasher
  ) {}
  async verifyCredentials(credentials: Credentials): Promise<User> {
    // implement this method
    const foundUser = await this.userRepository.findOne({
      where: {
        username: credentials.username
      }
    })

    if (!foundUser) {
      throw new HttpErrors.NotFound('user not found')
    }
    const passwordMatched = await this.hasher.comparePassword(
      credentials.password,
      foundUser.password
    )
    if (!passwordMatched)
      throw new HttpErrors.Unauthorized('password is not valid')
    return foundUser
  }

  async convertToUserProfile(user: User): Promise<UserProfile> {
    const { password, ...userProfile } = { ...user }
    let permissions: PermissionKeys[] = []

    const roles = await this.userRepository.roles(user.id).find({
      include: [
        {
          relation: 'permissions'
        }
      ]
    })

    roles.forEach(role => {
      permissions.push(...role.permissions.map(permission => permission.name))
    })

    return {
      ...userProfile,
      permissions
    }
  }
}
