import { Entity, model, property } from '@loopback/repository'

@model({
  settings: {
    idInjection: false,
    postgresql: { schema: 'public', table: 'team-team_member' }
  }
})
export class TeamTeamMember extends Entity {
  @property({
    type: 'string',
    required: true,
    id: 1,
    postgresql: {
      columnName: 'team_member_id',
      dataType: 'uuid',
      dataLength: null,
      dataPrecision: null,
      dataScale: null,
      nullable: 'NO'
    }
  })
  teamMemberId: string

  @property({
    type: 'string',
    required: true,
    id: 2,
    postgresql: {
      columnName: 'team_id',
      dataType: 'uuid',
      dataLength: null,
      dataPrecision: null,
      dataScale: null,
      nullable: 'NO'
    }
  })
  teamId: string;
  // Define well-known properties here

  // Indexer property to allow additional data
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  [prop: string]: any

  constructor(data?: Partial<TeamTeamMember>) {
    super(data)
  }
}

export interface TeamTeamMemberRelations {
  // describe navigational properties here
}

export type TeamTeamMemberWithRelations = TeamTeamMember &
  TeamTeamMemberRelations
