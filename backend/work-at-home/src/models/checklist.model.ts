import { Entity, model, property, hasMany } from '@loopback/repository'
import { ChecklistItem } from './checklist-item.model'

@model({
  settings: {
    idInjection: false,
    postgresql: { schema: 'public', table: 'checklist' }
  }
})
export class Checklist extends Entity {
  @property({
    type: 'string',
    id: 1,
    generated: true,
    useDefaultIdType: false,
    postgresql: {
      columnName: 'id',
      dataType: 'uuid',
      dataLength: null,
      dataPrecision: null,
      dataScale: null,
      nullable: 'NO'
    }
  })
  id: string

  @property({
    type: 'string',
    required: true,
    postgresql: {
      columnName: 'checkpoint_id',
      dataType: 'uuid',
      dataLength: null,
      dataPrecision: null,
      dataScale: null,
      nullable: 'NO'
    }
  })
  checkpointId: string

  @property({
    type: 'string',
    required: true,
    length: 255,
    postgresql: {
      columnName: 'name',
      dataType: 'character varying',
      dataLength: 255,
      dataPrecision: null,
      dataScale: null,
      nullable: 'NO'
    }
  })
  name: string

  @hasMany(() => ChecklistItem)
  checklistItems: ChecklistItem[];
  // Define well-known properties here

  // Indexer property to allow additional data
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  [prop: string]: any

  constructor(data?: Partial<Checklist>) {
    super(data)
  }
}

export interface ChecklistRelations {
  // describe navigational properties here
}

export type ChecklistWithRelations = Checklist & ChecklistRelations
