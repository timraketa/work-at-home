import { Entity, model, property, belongsTo } from '@loopback/repository'
import { WorkDay } from './work-day.model'

@model({
  settings: {
    idInjection: false,
    postgresql: { schema: 'public', table: 'work_hour' }
  }
})
export class WorkHour extends Entity {
  @property({
    type: 'string',
    id: 1,
    generated: true,
    useDefaultIdType: false,
    postgresql: {
      columnName: 'id',
      dataType: 'uuid',
      dataLength: null,
      dataPrecision: null,
      dataScale: null,
      nullable: 'NO'
    }
  })
  id: string

  @property({
    type: 'string',
    required: true,
    length: 255,
    postgresql: {
      columnName: 'description',
      dataType: 'character varying',
      dataLength: 255,
      dataPrecision: null,
      dataScale: null,
      nullable: 'NO'
    }
  })
  description: string

  @belongsTo(
    () => WorkDay,
    { name: 'workDay' },
    {
      type: 'string',
      required: true,
      postgresql: {
        columnName: 'work_day_id',
        dataType: 'uuid',
        dataLength: null,
        dataPrecision: null,
        dataScale: null,
        nullable: 'NO'
      }
    }
  )
  workDayId: string

  @property({
    type: 'string',
    required: true,
    postgresql: {
      columnName: 'project_id',
      dataType: 'uuid',
      dataLength: null,
      dataPrecision: null,
      dataScale: null,
      nullable: 'NO'
    }
  })
  projectId: string;

  // Define well-known properties here

  // Indexer property to allow additional data
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  [prop: string]: any

  constructor(data?: Partial<WorkHour>) {
    super(data)
  }
}

export interface WorkHourRelations {
  // describe navigational properties here
}

export type WorkHourWithRelations = WorkHour & WorkHourRelations
