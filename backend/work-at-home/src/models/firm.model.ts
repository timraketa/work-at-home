import {
  Entity,
  model,
  property,
  hasMany,
  hasOne,
  belongsTo
} from '@loopback/repository'
import { User } from './user.model'
import { FirmUser } from './firm-user.model'
import { BusinessModel } from './business-model.model'

@model({
  settings: {
    idInjection: false,
    postgresql: { schema: 'public', table: 'firm' }
  }
})
export class Firm extends Entity {
  @property({
    type: 'string',
    generated: true,
    useDefaultIdType: false,
    id: 1,
    postgresql: {
      columnName: 'id',
      dataType: 'uuid',
      dataLength: null,
      dataPrecision: null,
      dataScale: null,
      nullable: 'NO'
    }
  })
  id: string

  @property({
    type: 'string',
    required: true,
    postgresql: {
      columnName: 'admin_id',
      dataType: 'uuid',
      dataLength: null,
      dataPrecision: null,
      dataScale: null,
      nullable: 'NO'
    }
  })
  adminId: string

  @property({
    type: 'string',
    required: true,
    length: 255,
    postgresql: {
      columnName: 'name',
      dataType: 'character varying',
      dataLength: 255,
      dataPrecision: null,
      dataScale: null,
      nullable: 'NO'
    }
  })
  name: string
  @hasMany(() => User, { through: { model: () => FirmUser } })
  users: User[]

  @belongsTo(
    () => BusinessModel,
    { name: 'businessModel' },
    {
      type: 'number',
      required: true,
      scale: 0,
      postgresql: {
        columnName: 'model_id',
        dataType: 'bigint',
        dataLength: null,
        dataPrecision: null,
        dataScale: 0,
        nullable: 'NO'
      }
    }
  )
  modelId: number;
  // Define well-known properties here

  // Indexer property to allow additional data
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  [prop: string]: any

  constructor(data?: Partial<Firm>) {
    super(data)
  }
}

export interface FirmRelations {
  // describe navigational properties here
}

export type FirmWithRelations = Firm & FirmRelations
