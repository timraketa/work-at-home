import { Entity, model, property } from '@loopback/repository'

@model({
  settings: {
    idInjection: false,
    postgresql: { schema: 'public', table: 'user-user_role-firm' }
  }
})
export class UserUserRoleFirm extends Entity {
  @property({
    type: 'string',
    required: true,
    id: 1,
    postgresql: {
      columnName: 'user_id',
      dataType: 'uuid',
      dataLength: null,
      dataPrecision: null,
      dataScale: null,
      nullable: 'NO'
    }
  })
  userId: string

  @property({
    type: 'number',
    required: true,
    scale: 0,
    id: 2,
    postgresql: {
      columnName: 'role_id',
      dataType: 'bigint',
      dataLength: null,
      dataPrecision: null,
      dataScale: 0,
      nullable: 'NO'
    }
  })
  roleId: number

  @property({
    type: 'string',
    required: true,
    id: 3,
    postgresql: {
      columnName: 'firm_id',
      dataType: 'uuid',
      dataLength: null,
      dataPrecision: null,
      dataScale: null,
      nullable: 'NO'
    }
  })
  firmId: string;

  // Define well-known properties here

  // Indexer property to allow additional data
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  [prop: string]: any

  constructor(data?: Partial<UserUserRoleFirm>) {
    super(data)
  }
}

export interface UserUserRoleFirmRelations {
  // describe navigational properties here
}

export type UserUserRoleFirmWithRelations = UserUserRoleFirm &
  UserUserRoleFirmRelations
