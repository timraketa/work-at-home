import {
  Entity,
  model,
  property,
  hasMany,
  hasOne,
  belongsTo
} from '@loopback/repository'
import { User } from './user.model'
import { TeamTeamMember } from './team-team-member.model'
import { Project } from './project.model'

@model({
  settings: {
    idInjection: false,
    postgresql: { schema: 'public', table: 'team' }
  }
})
export class Team extends Entity {
  @property({
    type: 'string',
    id: 1,
    generated: true,
    useDefaultIdType: false,
    postgresql: {
      columnName: 'id',
      dataType: 'uuid',
      dataLength: null,
      dataPrecision: null,
      dataScale: null,
      nullable: 'NO'
    }
  })
  id: string

  @belongsTo(
    () => User,
    { name: 'teamLead' },
    {
      type: 'string',
      required: true,
      postgresql: {
        columnName: 'team_lead_id',
        dataType: 'uuid',
        dataLength: null,
        dataPrecision: null,
        dataScale: null,
        nullable: 'NO'
      }
    }
  )
  teamLeadId: string

  @property({
    type: 'string',
    required: true,
    length: 255,
    postgresql: {
      columnName: 'name',
      dataType: 'character varying',
      dataLength: 255,
      dataPrecision: null,
      dataScale: null,
      nullable: 'NO'
    }
  })
  name: string

  @property({
    type: 'string',
    required: true,
    length: 255,
    postgresql: {
      columnName: 'description',
      dataType: 'character varying',
      dataLength: 255,
      dataPrecision: null,
      dataScale: null,
      nullable: 'NO'
    }
  })
  description: string

  @belongsTo(
    () => Project,
    { name: 'project' },
    {
      type: 'number',
      required: true,
      postgresql: {
        columnName: 'project_id',
        dataType: 'uuid',
        dataLength: null,
        dataPrecision: null,
        dataScale: null,
        nullable: 'NO'
      }
    }
  )
  projectId: string

  @hasMany(() => User, {
    through: { model: () => TeamTeamMember, keyTo: 'teamMemberId' }
  })
  users: User[];
  // Define well-known properties here

  // Indexer property to allow additional data
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  [prop: string]: any

  constructor(data?: Partial<Team>) {
    super(data)
  }
}

export interface TeamRelations {
  // describe navigational properties here
}

export type TeamWithRelations = Team & TeamRelations
