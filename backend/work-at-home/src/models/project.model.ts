import {
  Entity,
  model,
  property,
  hasMany,
  belongsTo
} from '@loopback/repository'
import { Team } from './team.model'
import { WorkHour } from './work-hour.model'
import { Checkpoint } from './checkpoint.model'
import { User } from './user.model'
import { Firm } from './firm.model'

@model({
  settings: {
    idInjection: false,
    postgresql: { schema: 'public', table: 'project' }
  }
})
export class Project extends Entity {
  @property({
    type: 'string',
    id: 1,
    generated: true,
    useDefaultIdType: false,
    postgresql: {
      columnName: 'id',
      dataType: 'uuid',
      dataLength: null,
      dataPrecision: null,
      dataScale: null,
      nullable: 'NO'
    }
  })
  id: string

  @property({
    type: 'number',
    required: true,
    postgresql: {
      columnName: 'min_daily_hours',
      dataType: 'numeric',
      dataLength: null,
      dataPrecision: null,
      dataScale: null,
      nullable: 'NO'
    }
  })
  minDailyHours: number

  @property({
    type: 'number',
    required: true,
    postgresql: {
      columnName: 'max_weekly_hours',
      dataType: 'numeric',
      dataLength: null,
      dataPrecision: null,
      dataScale: null,
      nullable: 'NO'
    }
  })
  maxWeeklyHours: number

  @property({
    type: 'boolean',
    required: true,
    postgresql: {
      columnName: 'reminder_enabled',
      dataType: 'boolean',
      dataLength: null,
      dataPrecision: null,
      dataScale: null,
      nullable: 'NO'
    }
  })
  reminderEnabled: boolean

  @belongsTo(
    () => User,
    { name: 'projectManager' },
    {
      type: 'string',
      required: true,
      postgresql: {
        columnName: 'project_manager_id',
        dataType: 'uuid',
        dataLength: null,
        dataPrecision: null,
        dataScale: null,
        nullable: 'NO'
      }
    }
  )
  projectManagerId: string

  @property({
    type: 'string',
    required: true,
    length: 255,
    postgresql: {
      columnName: 'name',
      dataType: 'character varying',
      dataLength: 255,
      dataPrecision: null,
      dataScale: null,
      nullable: 'NO'
    }
  })
  name: string

  @belongsTo(
    () => Firm,
    { name: 'firm' },
    {
      type: 'string',
      required: true,
      postgresql: {
        columnName: 'firm_id',
        dataType: 'uuid',
        dataLength: null,
        dataPrecision: null,
        dataScale: null,
        nullable: 'NO'
      }
    }
  )
  firmId: string

  @property({
    type: 'string',
    required: true,
    length: 255,
    postgresql: {
      columnName: 'description',
      dataType: 'character varying',
      dataLength: 255,
      dataPrecision: null,
      dataScale: null,
      nullable: 'NO'
    }
  })
  description: string

  @property({
    type: 'number',
    required: true,
    postgresql: {
      columnName: 'budget',
      dataType: 'numeric',
      dataLength: null,
      dataPrecision: null,
      dataScale: null,
      nullable: 'NO'
    }
  })
  budget: number

  @property({
    type: 'date',
    required: true,
    postgresql: {
      columnName: 'due_date',
      dataType: 'date',
      dataLength: null,
      dataPrecision: null,
      dataScale: null,
      nullable: 'NO'
    }
  })
  dueDate: string

  @hasMany(() => WorkHour)
  workHours: WorkHour[]

  @hasMany(() => Checkpoint)
  checkpoints: Checkpoint[]

  @hasMany(() => Team)
  teams: Team[];

  // Define well-known properties here

  // Indexer property to allow additional data
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  [prop: string]: any

  constructor(data?: Partial<Project>) {
    super(data)
  }
}

export interface ProjectRelations {
  // describe navigational properties here
}

export type ProjectWithRelations = Project & ProjectRelations
