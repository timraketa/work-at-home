import { Entity, model, property } from '@loopback/repository'
import { PermissionKeys } from '../authorization/permission-keys'

@model({
  settings: {
    idInjection: false,
    postgresql: { schema: 'public', table: 'permission' }
  }
})
export class Permission extends Entity {
  @property({
    type: 'string',
    required: true,
    length: 255,
    postgresql: {
      columnName: 'name',
      dataType: 'character varying',
      dataLength: 255,
      dataPrecision: null,
      dataScale: null,
      nullable: 'NO'
    }
  })
  name: PermissionKeys

  @property({
    type: 'string',
    generated: true,
    useDefaultIdType: false,
    id: 1,
    postgresql: {
      columnName: 'id',
      dataType: 'uuid',
      dataLength: null,
      dataPrecision: null,
      dataScale: null,
      nullable: 'NO'
    }
  })
  id: string

  @property({
    type: 'number',
    required: true,
    scale: 0,
    postgresql: {
      columnName: 'role_id',
      dataType: 'integer',
      dataLength: null,
      dataPrecision: null,
      dataScale: 0,
      nullable: 'NO'
    }
  })
  roleId: number;

  // Define well-known properties here

  // Indexer property to allow additional data
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  [prop: string]: any

  constructor(data?: Partial<Permission>) {
    super(data)
  }
}

export interface PermissionRelations {
  // describe navigational properties here
}

export type PermissionWithRelations = Permission & PermissionRelations
