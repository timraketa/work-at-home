import { Entity, model, property, hasMany } from '@loopback/repository'
import { Permission } from './permission.model'

@model({
  settings: {
    idInjection: false,
    postgresql: { schema: 'public', table: 'role' }
  }
})
export class Role extends Entity {
  @property({
    type: 'number',
    required: true,
    scale: 0,
    id: 1,
    useDefaultIdType: false,
    postgresql: {
      columnName: 'id',
      dataType: 'bigint',
      dataLength: null,
      dataPrecision: null,
      dataScale: 0,
      nullable: 'NO'
    }
  })
  id: number

  @property({
    type: 'string',
    required: true,
    length: 255,
    postgresql: {
      columnName: 'name',
      dataType: 'character varying',
      dataLength: 255,
      dataPrecision: null,
      dataScale: null,
      nullable: 'NO'
    }
  })
  name: string

  @hasMany(() => Permission)
  permissions: Permission[];
  // Define well-known properties here

  // Indexer property to allow additional data
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  [prop: string]: any

  constructor(data?: Partial<Role>) {
    super(data)
  }
}

export interface RoleRelations {
  // describe navigational properties here
}

export type RoleWithRelations = Role & RoleRelations
