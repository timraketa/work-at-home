import { Entity, model, property } from '@loopback/repository'

@model({
  settings: {
    idInjection: false,
    postgresql: { schema: 'public', table: 'firm-user' }
  }
})
export class FirmUser extends Entity {
  @property({
    type: 'string',
    id: 1,
    required: true,
    postgresql: {
      columnName: 'firm_id',
      dataType: 'uuid',
      dataLength: null,
      dataPrecision: null,
      dataScale: null,
      nullable: 'NO'
    }
  })
  firmId: string

  @property({
    type: 'string',
    required: true,
    id: 2,
    postgresql: {
      columnName: 'user_id',
      dataType: 'uuid',
      dataLength: null,
      dataPrecision: null,
      dataScale: null,
      nullable: 'NO'
    }
  })
  userId: string

  @property({
    type: 'string',
    required: true,
    length: 255,
    postgresql: {
      columnName: 'firm_user_email',
      dataType: 'character varying',
      dataLength: 255,
      dataPrecision: null,
      dataScale: null,
      nullable: 'NO'
    }
  })
  firmUserEmail: string;

  // Define well-known properties here

  // Indexer property to allow additional data
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  [prop: string]: any

  constructor(data?: Partial<FirmUser>) {
    super(data)
  }
}

export interface FirmUserRelations {
  // describe navigational properties here
}

export type FirmUserWithRelations = FirmUser & FirmUserRelations
