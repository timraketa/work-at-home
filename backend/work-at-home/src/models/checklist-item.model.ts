import { Entity, model, property } from '@loopback/repository'

@model({
  settings: {
    idInjection: false,
    postgresql: { schema: 'public', table: 'checklist_item' }
  }
})
export class ChecklistItem extends Entity {
  @property({
    type: 'string',
    id: 1,
    generated: true,
    useDefaultIdType: false,
    postgresql: {
      columnName: 'id',
      dataType: 'uuid',
      dataLength: null,
      dataPrecision: null,
      dataScale: null,
      nullable: 'NO'
    }
  })
  id: string

  @property({
    type: 'string',
    required: true,
    postgresql: {
      columnName: 'checklist_id',
      dataType: 'uuid',
      dataLength: null,
      dataPrecision: null,
      dataScale: null,
      nullable: 'NO'
    }
  })
  checklistId: string

  @property({
    type: 'string',
    required: true,
    length: 255,
    postgresql: {
      columnName: 'description',
      dataType: 'character varying',
      dataLength: 255,
      dataPrecision: null,
      dataScale: null,
      nullable: 'NO'
    }
  })
  description: string

  @property({
    type: 'boolean',
    required: true,
    postgresql: {
      columnName: 'is_checked',
      dataType: 'boolean',
      dataLength: null,
      dataPrecision: null,
      dataScale: null,
      nullable: 'NO'
    }
  })
  isChecked: boolean;

  // Define well-known properties here

  // Indexer property to allow additional data
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  [prop: string]: any

  constructor(data?: Partial<ChecklistItem>) {
    super(data)
  }
}

export interface ChecklistItemRelations {
  // describe navigational properties here
}

export type ChecklistItemWithRelations = ChecklistItem & ChecklistItemRelations
