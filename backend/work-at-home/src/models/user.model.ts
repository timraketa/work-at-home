import { Entity, model, property, hasMany } from '@loopback/repository'
import { Role } from './role.model'
import { UserUserRoleFirm } from './user-user-role-firm.model'
import { WorkDay } from './work-day.model'
import { Team } from './team.model'
import { TeamTeamMember } from './team-team-member.model'
import { Firm } from './firm.model'
import { FirmUser } from './firm-user.model'

@model({
  settings: {
    idInjection: false,
    postgresql: { schema: 'public', table: 'user' },
    hiddenProperties: ['password']
  }
})
export class User extends Entity {
  @property({
    type: 'string',
    id: 1,
    generated: true,
    useDefaultIdType: false,
    postgresql: {
      columnName: 'id',
      dataType: 'uuid',
      dataLength: null,
      dataPrecision: null,
      dataScale: null,
      nullable: 'NO'
    }
  })
  id: string

  @property({
    type: 'string',
    required: true,
    length: 255,
    postgresql: {
      columnName: 'username',
      dataType: 'character varying',
      dataLength: 255,
      dataPrecision: null,
      dataScale: null,
      nullable: 'NO'
    }
  })
  username: string

  @property({
    type: 'string',
    length: 255,
    postgresql: {
      columnName: 'password',
      dataType: 'character varying',
      dataLength: 255,
      dataPrecision: null,
      dataScale: null,
      nullable: 'YES'
    }
  })
  password: string

  @property({
    type: 'string',
    length: 255,
    postgresql: {
      columnName: 'first_name',
      dataType: 'character varying',
      dataLength: 255,
      dataPrecision: null,
      dataScale: null,
      nullable: 'YES'
    }
  })
  firstName: string

  @property({
    type: 'string',
    length: 255,
    postgresql: {
      columnName: 'last_name',
      dataType: 'character varying',
      dataLength: 255,
      dataPrecision: null,
      dataScale: null,
      nullable: 'YES'
    }
  })
  lastName: string

  @property({
    type: 'number',
    postgresql: {
      columnName: 'hourly_rate',
      dataType: 'numeric',
      dataLength: null,
      dataPrecision: null,
      dataScale: null,
      nullable: 'YES'
    }
  })
  hourlyRate: number

  @property({
    type: 'string',
    length: 255,
    postgresql: {
      columnName: 'phone_number',
      dataType: 'character varying',
      dataLength: 255,
      dataPrecision: null,
      dataScale: null,
      nullable: 'YES'
    }
  })
  phoneNumber: string

  @property({
    type: 'boolean',
    generated: true,
    postgresql: {
      columnName: 'is_registered',
      dataType: 'boolean',
      dataPrecision: null,
      dataScale: null,
      nullable: 'NO'
    }
  })
  isRegistered: boolean

  @hasMany(() => Role, { through: { model: () => UserUserRoleFirm } })
  roles: Role[]

  @hasMany(() => WorkDay)
  workDays: WorkDay[]

  @hasMany(() => Firm, { through: { model: () => FirmUser } })
  firms: Firm[]

  @hasMany(() => Team, {
    through: { model: () => TeamTeamMember, keyFrom: 'teamMemberId' }
  })
  teams: Team[];
  // Define well-known properties here

  // Indexer property to allow additional data
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  [prop: string]: any

  constructor(data?: Partial<User>) {
    super(data)
  }
}

export interface UserRelations {
  // describe navigational properties here
}

export type UserWithRelations = User & UserRelations
