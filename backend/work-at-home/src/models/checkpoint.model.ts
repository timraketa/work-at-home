import { Entity, model, property, hasMany } from '@loopback/repository'
import { Checklist } from './checklist.model'

@model({
  settings: {
    idInjection: false,
    postgresql: { schema: 'public', table: 'checkpoint' }
  }
})
export class Checkpoint extends Entity {
  @property({
    type: 'string',
    id: 1,
    generated: true,
    useDefaultIdType: false,
    postgresql: {
      columnName: 'id',
      dataType: 'uuid',
      dataLength: null,
      dataPrecision: null,
      dataScale: null,
      nullable: 'NO'
    }
  })
  id: string

  @property({
    type: 'date',
    required: true,
    postgresql: {
      columnName: 'date',
      dataType: 'date',
      dataLength: null,
      dataPrecision: null,
      dataScale: null,
      nullable: 'NO'
    }
  })
  date: string

  @property({
    type: 'string',
    required: true,
    postgresql: {
      columnName: 'project_id',
      dataType: 'uuid',
      dataLength: null,
      dataPrecision: null,
      dataScale: null,
      nullable: 'NO'
    }
  })
  projectId: string

  @property({
    type: 'string',
    length: 255,
    postgresql: {
      columnName: 'description',
      dataType: 'character varying',
      dataLength: 255,
      dataPrecision: null,
      dataScale: null,
      nullable: 'YES'
    }
  })
  description?: string

  @property({
    type: 'string',
    required: true,
    length: 255,
    postgresql: {
      columnName: 'name',
      dataType: 'character varying',
      dataLength: 255,
      dataPrecision: null,
      dataScale: null,
      nullable: 'NO'
    }
  })
  name: string

  @hasMany(() => Checklist)
  checklists: Checklist[];
  // Define well-known properties here

  // Indexer property to allow additional data
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  [prop: string]: any

  constructor(data?: Partial<Checkpoint>) {
    super(data)
  }
}

export interface CheckpointRelations {
  // describe navigational properties here
}

export type CheckpointWithRelations = Checkpoint & CheckpointRelations
