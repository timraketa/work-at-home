import { Entity, model, property, hasMany } from '@loopback/repository'
import { WorkHour } from './work-hour.model'

@model({
  settings: {
    idInjection: false,
    postgresql: { schema: 'public', table: 'work_day' }
  }
})
export class WorkDay extends Entity {
  @property({
    type: 'string',
    id: 1,
    generated: true,
    useDefaultIdType: false,
    postgresql: {
      columnName: 'id',
      dataType: 'uuid',
      dataLength: null,
      dataPrecision: null,
      dataScale: null,
      nullable: 'NO'
    }
  })
  id: string

  @property({
    type: 'boolean',
    required: true,
    postgresql: {
      columnName: 'is_free',
      dataType: 'boolean',
      dataLength: null,
      dataPrecision: null,
      dataScale: null,
      nullable: 'NO'
    }
  })
  isFree: boolean

  @property({
    type: 'date',
    required: true,
    postgresql: {
      columnName: 'date',
      dataType: 'date',
      dataLength: null,
      dataPrecision: null,
      dataScale: null,
      nullable: 'NO'
    }
  })
  date: string

  @property({
    type: 'string',
    required: true,
    postgresql: {
      columnName: 'user_id',
      dataType: 'uuid',
      dataLength: null,
      dataPrecision: null,
      dataScale: null,
      nullable: 'NO'
    }
  })
  userId: string

  @hasMany(() => WorkHour)
  workHours: WorkHour[];
  // Define well-known properties here

  // Indexer property to allow additional data
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  [prop: string]: any

  constructor(data?: Partial<WorkDay>) {
    super(data)
  }
}

export interface WorkDayRelations {
  // describe navigational properties here
}

export type WorkDayWithRelations = WorkDay & WorkDayRelations
