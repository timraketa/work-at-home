import { inject, lifeCycleObserver, LifeCycleObserver } from '@loopback/core'
import { juggler } from '@loopback/repository'

const config = {
  name: 'WorkAtHomeDb',
  connector: 'postgresql',
  host: 'localhost',
  port: 55432,
  user: 'user',
  password: 'pass',
  database: 'work-at-home'
}

// Observe application's life cycle to disconnect the datasource when
// application is stopped. This allows the application to be shut down
// gracefully. The `stop()` method is inherited from `juggler.DataSource`.
// Learn more at https://loopback.io/doc/en/lb4/Life-cycle.html
@lifeCycleObserver('datasource')
export class WorkAtHomeDbDataSource
  extends juggler.DataSource
  implements LifeCycleObserver {
  static dataSourceName = 'WorkAtHomeDb'
  static readonly defaultConfig = config

  constructor(
    @inject('datasources.config.WorkAtHomeDb', { optional: true })
    dsConfig: object = config
  ) {
    super(dsConfig)
  }
}
