import {
  DefaultCrudRepository,
  repository,
  HasManyRepositoryFactory
} from '@loopback/repository'
import { Checkpoint, CheckpointRelations, Checklist } from '../models'
import { WorkAtHomeDbDataSource } from '../datasources'
import { inject, Getter } from '@loopback/core'
import { ChecklistRepository } from './checklist.repository'

export class CheckpointRepository extends DefaultCrudRepository<
  Checkpoint,
  typeof Checkpoint.prototype.id,
  CheckpointRelations
> {
  public readonly checklists: HasManyRepositoryFactory<
    Checklist,
    typeof Checkpoint.prototype.id
  >

  constructor(
    @inject('datasources.WorkAtHomeDb') dataSource: WorkAtHomeDbDataSource,
    @repository.getter('ChecklistRepository')
    protected checklistRepositoryGetter: Getter<ChecklistRepository>
  ) {
    super(Checkpoint, dataSource)
    this.checklists = this.createHasManyRepositoryFactoryFor(
      'checklists',
      checklistRepositoryGetter
    )
    this.registerInclusionResolver(
      'checklists',
      this.checklists.inclusionResolver
    )
  }
}
