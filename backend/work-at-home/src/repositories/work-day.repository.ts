import {
  DefaultCrudRepository,
  repository,
  HasManyRepositoryFactory
} from '@loopback/repository'
import { WorkDay, WorkDayRelations, WorkHour } from '../models'
import { WorkAtHomeDbDataSource } from '../datasources'
import { inject, Getter } from '@loopback/core'
import { WorkHourRepository } from './work-hour.repository'

export class WorkDayRepository extends DefaultCrudRepository<
  WorkDay,
  typeof WorkDay.prototype.id,
  WorkDayRelations
> {
  public readonly workHours: HasManyRepositoryFactory<
    WorkHour,
    typeof WorkDay.prototype.id
  >

  constructor(
    @inject('datasources.WorkAtHomeDb') dataSource: WorkAtHomeDbDataSource,
    @repository.getter('WorkHourRepository')
    protected workHourRepositoryGetter: Getter<WorkHourRepository>
  ) {
    super(WorkDay, dataSource)
    this.workHours = this.createHasManyRepositoryFactoryFor(
      'workHours',
      workHourRepositoryGetter
    )
    this.registerInclusionResolver(
      'workHours',
      this.workHours.inclusionResolver
    )
  }
}
