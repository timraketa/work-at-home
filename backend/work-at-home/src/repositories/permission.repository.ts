import { DefaultCrudRepository } from '@loopback/repository'
import { Permission, PermissionRelations } from '../models'
import { WorkAtHomeDbDataSource } from '../datasources'
import { inject } from '@loopback/core'

export class PermissionRepository extends DefaultCrudRepository<
  Permission,
  typeof Permission.prototype.id,
  PermissionRelations
> {
  constructor(
    @inject('datasources.WorkAtHomeDb') dataSource: WorkAtHomeDbDataSource
  ) {
    super(Permission, dataSource)
  }
}
