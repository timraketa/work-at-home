import {
  DefaultCrudRepository,
  repository,
  BelongsToAccessor
} from '@loopback/repository'
import { WorkHour, WorkHourRelations, WorkDay } from '../models'
import { WorkAtHomeDbDataSource } from '../datasources'
import { inject, Getter } from '@loopback/core'
import { WorkDayRepository } from './work-day.repository'

export class WorkHourRepository extends DefaultCrudRepository<
  WorkHour,
  typeof WorkHour.prototype.id,
  WorkHourRelations
> {
  public readonly workDay: BelongsToAccessor<
    WorkDay,
    typeof WorkHour.prototype.id
  >

  constructor(
    @inject('datasources.WorkAtHomeDb') dataSource: WorkAtHomeDbDataSource,
    @repository.getter('WorkDayRepository')
    protected workDayRepositoryGetter: Getter<WorkDayRepository>
  ) {
    super(WorkHour, dataSource)
    this.workDay = this.createBelongsToAccessorFor(
      'workDay',
      workDayRepositoryGetter
    )
    this.registerInclusionResolver('workDay', this.workDay.inclusionResolver)
  }
}
