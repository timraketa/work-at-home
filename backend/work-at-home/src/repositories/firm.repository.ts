import {
  DefaultCrudRepository,
  repository,
  HasManyThroughRepositoryFactory,
  HasOneRepositoryFactory,
  BelongsToAccessor
} from '@loopback/repository'
import { Firm, FirmRelations, User, FirmUser, BusinessModel } from '../models'
import { WorkAtHomeDbDataSource } from '../datasources'
import { inject, Getter } from '@loopback/core'
import { FirmUserRepository } from './firm-user.repository'
import { UserRepository } from './user.repository'
import { BusinessModelRepository } from './business-model.repository'

export class FirmRepository extends DefaultCrudRepository<
  Firm,
  typeof Firm.prototype.id,
  FirmRelations
> {
  public readonly users: HasManyThroughRepositoryFactory<
    User,
    typeof User.prototype.id,
    FirmUser,
    typeof Firm.prototype.id
  >

  public readonly businessModel: BelongsToAccessor<
    BusinessModel,
    typeof Firm.prototype.id
  >

  constructor(
    @inject('datasources.WorkAtHomeDb') dataSource: WorkAtHomeDbDataSource,
    @repository.getter('FirmUserRepository')
    protected firmUserRepositoryGetter: Getter<FirmUserRepository>,
    @repository.getter('UserRepository')
    protected userRepositoryGetter: Getter<UserRepository>,
    @repository.getter('BusinessModelRepository')
    protected businessModelRepositoryGetter: Getter<BusinessModelRepository>
  ) {
    super(Firm, dataSource)
    this.businessModel = this.createBelongsToAccessorFor(
      'businessModel',
      businessModelRepositoryGetter
    )
    this.registerInclusionResolver(
      'businessModel',
      this.businessModel.inclusionResolver
    )
    this.users = this.createHasManyThroughRepositoryFactoryFor(
      'users',
      userRepositoryGetter,
      firmUserRepositoryGetter
    )
    this.registerInclusionResolver('users', this.users.inclusionResolver)
  }
}
