import { DefaultCrudRepository } from '@loopback/repository'
import { TeamTeamMember, TeamTeamMemberRelations } from '../models'
import { WorkAtHomeDbDataSource } from '../datasources'
import { inject } from '@loopback/core'

export class TeamTeamMemberRepository extends DefaultCrudRepository<
  TeamTeamMember,
  typeof TeamTeamMember.prototype.teamMemberId,
  TeamTeamMemberRelations
> {
  constructor(
    @inject('datasources.WorkAtHomeDb') dataSource: WorkAtHomeDbDataSource
  ) {
    super(TeamTeamMember, dataSource)
  }
}
