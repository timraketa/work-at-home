import {
  DefaultCrudRepository,
  repository,
  HasManyRepositoryFactory,
  BelongsToAccessor
} from '@loopback/repository'
import {
  Project,
  ProjectRelations,
  Team,
  WorkHour,
  Checkpoint,
  User,
  Firm
} from '../models'
import { WorkAtHomeDbDataSource } from '../datasources'
import { inject, Getter } from '@loopback/core'
import { TeamRepository } from './team.repository'
import { WorkHourRepository } from './work-hour.repository'
import { CheckpointRepository } from './checkpoint.repository'
import { UserRepository } from './user.repository'
import { FirmRepository } from './firm.repository'

export class ProjectRepository extends DefaultCrudRepository<
  Project,
  typeof Project.prototype.id,
  ProjectRelations
> {
  public readonly workHours: HasManyRepositoryFactory<
    WorkHour,
    typeof Project.prototype.id
  >
  public readonly checkpoints: HasManyRepositoryFactory<
    Checkpoint,
    typeof Project.prototype.id
  >

  public readonly teams: HasManyRepositoryFactory<
    Team,
    typeof Project.prototype.id
  >

  public readonly projectManager: BelongsToAccessor<
    User,
    typeof Project.prototype.id
  >

  public readonly firm: BelongsToAccessor<Firm, typeof Project.prototype.id>

  constructor(
    @inject('datasources.WorkAtHomeDb') dataSource: WorkAtHomeDbDataSource,
    @repository.getter('TeamRepository')
    protected teamRepositoryGetter: Getter<TeamRepository>,
    @repository.getter('WorkHourRepository')
    protected workHourRepositoryGetter: Getter<WorkHourRepository>,
    @repository.getter('CheckpointRepository')
    protected checkpointRepositoryGetter: Getter<CheckpointRepository>,
    @repository.getter('UserRepository')
    protected userRepositoryGetter: Getter<UserRepository>,
    @repository.getter('FirmRepository')
    protected firmRepositoryGetter: Getter<FirmRepository>
  ) {
    super(Project, dataSource)
    this.firm = this.createBelongsToAccessorFor('firm', firmRepositoryGetter)
    this.registerInclusionResolver('firm', this.firm.inclusionResolver)
    this.projectManager = this.createBelongsToAccessorFor(
      'projectManager',
      userRepositoryGetter
    )
    this.registerInclusionResolver(
      'projectManager',
      this.projectManager.inclusionResolver
    )
    this.teams = this.createHasManyRepositoryFactoryFor(
      'teams',
      teamRepositoryGetter
    )
    this.registerInclusionResolver('teams', this.teams.inclusionResolver)
    this.workHours = this.createHasManyRepositoryFactoryFor(
      'workHours',
      workHourRepositoryGetter
    )
    this.registerInclusionResolver(
      'workHours',
      this.workHours.inclusionResolver
    )
    this.checkpoints = this.createHasManyRepositoryFactoryFor(
      'checkpoints',
      checkpointRepositoryGetter
    )
    this.registerInclusionResolver(
      'checkpoints',
      this.checkpoints.inclusionResolver
    )
  }
}
