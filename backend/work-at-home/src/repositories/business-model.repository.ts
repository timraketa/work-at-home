import { DefaultCrudRepository } from '@loopback/repository'
import { BusinessModel, BusinessModelRelations } from '../models'
import { WorkAtHomeDbDataSource } from '../datasources'
import { inject } from '@loopback/core'

export class BusinessModelRepository extends DefaultCrudRepository<
  BusinessModel,
  typeof BusinessModel.prototype.id,
  BusinessModelRelations
> {
  constructor(
    @inject('datasources.WorkAtHomeDb') dataSource: WorkAtHomeDbDataSource
  ) {
    super(BusinessModel, dataSource)
  }
}
