import {
  DefaultCrudRepository,
  repository,
  HasManyRepositoryFactory
} from '@loopback/repository'
import { Role, RoleRelations, Permission } from '../models'
import { WorkAtHomeDbDataSource } from '../datasources'
import { inject, Getter } from '@loopback/core'
import { PermissionRepository } from './permission.repository'

export class RoleRepository extends DefaultCrudRepository<
  Role,
  typeof Role.prototype.id,
  RoleRelations
> {
  public readonly permissions: HasManyRepositoryFactory<
    Permission,
    typeof Role.prototype.id
  >

  constructor(
    @inject('datasources.WorkAtHomeDb') dataSource: WorkAtHomeDbDataSource,
    @repository.getter('PermissionRepository')
    protected permissionRepositoryGetter: Getter<PermissionRepository>
  ) {
    super(Role, dataSource)
    this.permissions = this.createHasManyRepositoryFactoryFor(
      'permissions',
      permissionRepositoryGetter
    )
    this.registerInclusionResolver(
      'permissions',
      this.permissions.inclusionResolver
    )
  }
}
