import {
  DefaultCrudRepository,
  repository,
  HasManyRepositoryFactory
} from '@loopback/repository'
import { Checklist, ChecklistRelations, ChecklistItem } from '../models'
import { WorkAtHomeDbDataSource } from '../datasources'
import { inject, Getter } from '@loopback/core'
import { ChecklistItemRepository } from './checklist-item.repository'

export class ChecklistRepository extends DefaultCrudRepository<
  Checklist,
  typeof Checklist.prototype.id,
  ChecklistRelations
> {
  public readonly checklistItems: HasManyRepositoryFactory<
    ChecklistItem,
    typeof Checklist.prototype.id
  >

  constructor(
    @inject('datasources.WorkAtHomeDb') dataSource: WorkAtHomeDbDataSource,
    @repository.getter('ChecklistItemRepository')
    protected checklistItemRepositoryGetter: Getter<ChecklistItemRepository>
  ) {
    super(Checklist, dataSource)
    this.checklistItems = this.createHasManyRepositoryFactoryFor(
      'checklistItems',
      checklistItemRepositoryGetter
    )
    this.registerInclusionResolver(
      'checklistItems',
      this.checklistItems.inclusionResolver
    )
  }
}
