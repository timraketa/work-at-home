import { DefaultCrudRepository } from '@loopback/repository'
import { UserUserRoleFirm, UserUserRoleFirmRelations } from '../models'
import { WorkAtHomeDbDataSource } from '../datasources'
import { inject } from '@loopback/core'

export class UserUserRoleFirmRepository extends DefaultCrudRepository<
  UserUserRoleFirm,
  typeof UserUserRoleFirm.prototype.userId,
  UserUserRoleFirmRelations
> {
  constructor(
    @inject('datasources.WorkAtHomeDb') dataSource: WorkAtHomeDbDataSource
  ) {
    super(UserUserRoleFirm, dataSource)
  }
}
