import { DefaultCrudRepository } from '@loopback/repository'
import { FirmUser, FirmUserRelations } from '../models'
import { WorkAtHomeDbDataSource } from '../datasources'
import { inject } from '@loopback/core'

export class FirmUserRepository extends DefaultCrudRepository<
  FirmUser,
  typeof FirmUser.prototype.firmId,
  FirmUserRelations
> {
  constructor(
    @inject('datasources.WorkAtHomeDb') dataSource: WorkAtHomeDbDataSource
  ) {
    super(FirmUser, dataSource)
  }
}
