import {
  DefaultCrudRepository,
  repository,
  HasManyThroughRepositoryFactory,
  HasManyRepositoryFactory
} from '@loopback/repository'
import {
  User,
  UserRelations,
  Role,
  UserUserRoleFirm,
  Team,
  TeamTeamMember,
  WorkDay,
  Firm,
  FirmUser
} from '../models'
import { WorkAtHomeDbDataSource } from '../datasources'
import { inject, Getter, bind, BindingScope } from '@loopback/core'
import { PermissionKeys } from '../authorization/permission-keys'
import { UserUserRoleFirmRepository } from './user-user-role-firm.repository'
import { RoleRepository } from './role.repository'
import { WorkDayRepository } from './work-day.repository'
import { TeamTeamMemberRepository } from './team-team-member.repository'
import { TeamRepository } from './team.repository'
import { FirmUserRepository } from './firm-user.repository'
import { FirmRepository } from './firm.repository'

export type Credentials = {
  username: string
  password: string
}

export type UserProfile = {
  id: string
  username: string
  firstName: string
  lastName: string
  hourlyRate: number
  phoneNumber: string
  permissions: PermissionKeys[]
}

@bind({ scope: BindingScope.SINGLETON })
export class UserRepository extends DefaultCrudRepository<
  User,
  typeof User.prototype.id,
  UserRelations
> {
  public readonly roles: HasManyThroughRepositoryFactory<
    Role,
    typeof Role.prototype.id,
    UserUserRoleFirm,
    typeof User.prototype.id
  >

  public readonly workDays: HasManyRepositoryFactory<
    WorkDay,
    typeof User.prototype.id
  >

  public readonly firms: HasManyThroughRepositoryFactory<
    Firm,
    typeof Firm.prototype.id,
    FirmUser,
    typeof User.prototype.id
  >

  public readonly teams: HasManyThroughRepositoryFactory<
    Team,
    typeof Team.prototype.id,
    TeamTeamMember,
    typeof User.prototype.id
  >

  constructor(
    @inject('datasources.WorkAtHomeDb') dataSource: WorkAtHomeDbDataSource,
    @repository.getter('UserUserRoleFirmRepository')
    protected userUserRoleFirmRepositoryGetter: Getter<UserUserRoleFirmRepository>,
    @repository.getter('RoleRepository')
    protected roleRepositoryGetter: Getter<RoleRepository>,
    @repository.getter('WorkDayRepository')
    protected workDayRepositoryGetter: Getter<WorkDayRepository>,
    @repository.getter('FirmUserRepository')
    protected firmUserRepositoryGetter: Getter<FirmUserRepository>,
    @repository.getter('FirmRepository')
    protected firmRepositoryGetter: Getter<FirmRepository>,
    @repository.getter('TeamTeamMemberRepository')
    protected teamTeamMemberRepositoryGetter: Getter<TeamTeamMemberRepository>,
    @repository.getter('TeamRepository')
    protected teamRepositoryGetter: Getter<TeamRepository>
  ) {
    super(User, dataSource)
    this.teams = this.createHasManyThroughRepositoryFactoryFor(
      'teams',
      teamRepositoryGetter,
      teamTeamMemberRepositoryGetter
    )
    this.registerInclusionResolver('teams', this.teams.inclusionResolver)
    this.firms = this.createHasManyThroughRepositoryFactoryFor(
      'firms',
      firmRepositoryGetter,
      firmUserRepositoryGetter
    )
    this.registerInclusionResolver('firms', this.firms.inclusionResolver)
    this.workDays = this.createHasManyRepositoryFactoryFor(
      'workDays',
      workDayRepositoryGetter
    )
    this.registerInclusionResolver('workDays', this.workDays.inclusionResolver)
    this.roles = this.createHasManyThroughRepositoryFactoryFor(
      'roles',
      roleRepositoryGetter,
      userUserRoleFirmRepositoryGetter
    )
    this.registerInclusionResolver('roles', this.roles.inclusionResolver)
  }
}
