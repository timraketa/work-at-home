import {
  DefaultCrudRepository,
  repository,
  HasManyThroughRepositoryFactory,
  HasOneRepositoryFactory,
  BelongsToAccessor
} from '@loopback/repository'
import { Team, TeamRelations, User, TeamTeamMember, Project } from '../models'
import { WorkAtHomeDbDataSource } from '../datasources'
import { inject, Getter, bind, BindingScope } from '@loopback/core'
import { TeamTeamMemberRepository } from './team-team-member.repository'
import { UserRepository } from './user.repository'
import { ProjectRepository } from './project.repository'

@bind({ scope: BindingScope.SINGLETON })
export class TeamRepository extends DefaultCrudRepository<
  Team,
  typeof Team.prototype.id,
  TeamRelations
> {
  public readonly project: BelongsToAccessor<Project, typeof Team.prototype.id>

  public readonly users: HasManyThroughRepositoryFactory<
    User,
    typeof User.prototype.id,
    TeamTeamMember,
    typeof Team.prototype.id
  >

  public readonly teamLead: BelongsToAccessor<User, typeof Team.prototype.id>

  constructor(
    @inject('datasources.WorkAtHomeDb') dataSource: WorkAtHomeDbDataSource,
    @repository.getter('TeamTeamMemberRepository')
    protected teamTeamMemberRepositoryGetter: Getter<TeamTeamMemberRepository>,
    @repository.getter('UserRepository')
    protected userRepositoryGetter: Getter<UserRepository>,
    @repository.getter('ProjectRepository')
    protected projectRepositoryGetter: Getter<ProjectRepository>
  ) {
    super(Team, dataSource)
    this.teamLead = this.createBelongsToAccessorFor(
      'teamLead',
      userRepositoryGetter
    )
    this.registerInclusionResolver('teamLead', this.teamLead.inclusionResolver)
    this.users = this.createHasManyThroughRepositoryFactoryFor(
      'users',
      userRepositoryGetter,
      teamTeamMemberRepositoryGetter
    )
    this.registerInclusionResolver('users', this.users.inclusionResolver)
    this.project = this.createBelongsToAccessorFor(
      'project',
      projectRepositoryGetter
    )
    this.registerInclusionResolver('project', this.project.inclusionResolver)
  }
}
