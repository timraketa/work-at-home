import { DefaultCrudRepository } from '@loopback/repository'
import { ChecklistItem, ChecklistItemRelations } from '../models'
import { WorkAtHomeDbDataSource } from '../datasources'
import { inject } from '@loopback/core'

export class ChecklistItemRepository extends DefaultCrudRepository<
  ChecklistItem,
  typeof ChecklistItem.prototype.id,
  ChecklistItemRelations
> {
  constructor(
    @inject('datasources.WorkAtHomeDb') dataSource: WorkAtHomeDbDataSource
  ) {
    super(ChecklistItem, dataSource)
  }
}
