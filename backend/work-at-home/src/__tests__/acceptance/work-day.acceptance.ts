import { Client, expect } from '@loopback/testlab'
import { WorkAtHomeApplication } from '../..'
import { UserRepository, WorkDayRepository } from '../../repositories'
import { setupApplication } from './test-helper'

import Joi from 'joi'

describe('WorkDay', () => {
  let app: WorkAtHomeApplication
  let client: Client
  let token: string
  let dayId: string
  let currentUserId: string

  before('setupApplication', async () => {
    ;({ app, client } = await setupApplication())

    await client
      .post('/users/login')
      .send({
        username: 'adminTest',
        password: 'test'
      })
      .expect(200)
      .then(res => {
        if (res) token = res.body.token
      })

    currentUserId = (
      await (await app.getRepository(UserRepository)).findOne(
        { where: { username: 'adminTest' } },
        { fields: { id: true } }
      )
    )?.id as string
  })

  after(async () => {
    await (await app.getRepository(WorkDayRepository)).deleteById(dayId)
    await app.stop()
  })

  it('adds new work day', async () => {
    let expected = Joi.object({
      id: Joi.string().uuid(),
      isFree: false,
      date: '2021-01-15T22:35:31.193Z',
      userId: currentUserId
    })

    let res = await client
      .post('/users/work-days')
      .auth(token, { type: 'bearer' })
      .send({
        isFree: false,
        date: '2021-01-15T22:35:31.193Z'
      })
      .expect(200)

    expect(expected.validate(res.body).error).to.equal(undefined)

    dayId = res.body.id
  })
})
