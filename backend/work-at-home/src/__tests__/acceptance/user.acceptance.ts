import { Client, expect } from '@loopback/testlab'
import { WorkAtHomeApplication } from '../..'
import { UserRepository } from '../../repositories'
import { setupApplication } from './test-helper'

import Joi from 'joi'

describe('User', () => {
  let app: WorkAtHomeApplication
  let client: Client
  let token: string

  before('setupApplication', async () => {
    ;({ app, client } = await setupApplication())

    await client
      .post('/users/login')
      .send({
        username: 'adminTest',
        password: 'test'
      })
      .expect(200)
      .then(res => {
        if (res) token = res.body.token
      })
  })

  after(async () => {
    await (await app.getRepository(UserRepository)).deleteAll({
      username: 'newTestUser'
    })
    await app.stop()
  })

  it('adds new user (worker)', async () => {
    await client
      .post('/users')
      .auth(token, { type: 'bearer' })
      .send({
        email: 'new.test.user@gmail.com',
        roles: [13, 14],
        username: 'newTestUser'
      })
      .expect(204)
  }).timeout(10000)

  it('user signs up with password that is shorter than 4 characters', async () => {
    await client
      .post('/users/signup/newTestUser')
      .send({
        password: 'tes',
        firstName: 'NewTestUser',
        lastName: 'User',
        phoneNumber: '0996969696',
        hourlyRate: 80
      })
      .expect(422)
  })

  it('user signs up', async () => {
    let expected = Joi.object({
      id: Joi.string().uuid(),
      username: 'newTestUser',
      firstName: 'NewTestUser',
      lastName: 'User',
      hourlyRate: 80,
      phoneNumber: '0996969696',
      isRegistered: true
    })

    let res = await client
      .post('/users/signup/newTestUser')
      .send({
        password: 'test',
        firstName: 'NewTestUser',
        lastName: 'User',
        phoneNumber: '0996969696',
        hourlyRate: 80
      })
      .expect(200)
      .expect('content-type', 'application/json')

    expect(expected.validate(res.body).error).to.equal(undefined)
  })
})
