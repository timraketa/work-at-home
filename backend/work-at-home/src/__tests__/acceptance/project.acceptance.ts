import { Client, expect } from '@loopback/testlab'
import { WorkAtHomeApplication } from '../..'
import { ProjectRepository, UserRepository } from '../../repositories'
import { setupApplication } from './test-helper'
import Joi from 'joi'

describe('Project', () => {
  let app: WorkAtHomeApplication
  let client: Client
  let token: string
  let projectId: string
  let currentUserId: string

  before('setupApplication', async () => {
    ;({ app, client } = await setupApplication())

    await client
      .post('/users/login')
      .send({
        username: 'adminTest',
        password: 'test'
      })
      .expect(200)
      .then(res => {
        if (res) token = res.body.token
      })

    currentUserId = (
      await (await app.getRepository(UserRepository)).findOne(
        { where: { username: 'adminTest' } },
        { fields: { id: true } }
      )
    )?.id as string
  })

  after(async () => {
    await (await app.getRepository(ProjectRepository)).deleteById(projectId)
    await app.stop()
  })

  it('adds new project', async () => {
    let expected = Joi.object({
      id: Joi.string().uuid(),
      minDailyHours: 4,
      maxWeeklyHours: 50,
      reminderEnabled: true,
      projectManagerId: currentUserId,
      name: 'TestProject',
      firmId: '903bac99-4a97-4963-9aa8-da0c9caa884b',
      description: 'Testing project',
      budget: 40000,
      dueDate: '2021-12-22T13:28:59.609Z'
    })

    let res = await client
      .post('/projects')
      .auth(token, { type: 'bearer' })
      .send({
        minDailyHours: 4,
        maxWeeklyHours: 50,
        reminderEnabled: true,
        name: 'TestProject',
        firmId: '903bac99-4a97-4963-9aa8-da0c9caa884b',
        description: 'Testing project',
        budget: 40000,
        dueDate: '2021-12-22T13:28:59.609Z'
      })
      .expect(200)
      .expect('content-type', 'application/json')

    expect(expected.validate(res.body).error).to.equal(undefined)

    projectId = res.body.id
  })

  it('edits existing project', async () => {
    let expected = Joi.object({
      id: projectId,
      minDailyHours: '4',
      maxWeeklyHours: '100',
      reminderEnabled: true,
      projectManagerId: currentUserId,
      name: 'TestProjectChangedName',
      firmId: '903bac99-4a97-4963-9aa8-da0c9caa884b',
      description: 'Testing project',
      budget: '60000',
      dueDate: '2021-12-22T00:00:00.000Z',
      projectManager: Joi.any(),
      firm: Joi.any(),
      workHours: Joi.array(),
      checkpoints: Joi.array(),
      teams: Joi.array()
    })

    await client
      .patch('/projects/' + projectId)
      .auth(token, { type: 'bearer' })
      .send({
        minDailyHours: 4,
        maxWeeklyHours: 100,
        reminderEnabled: true,
        name: 'TestProjectChangedName',
        firmId: '903bac99-4a97-4963-9aa8-da0c9caa884b',
        description: 'Testing project',
        budget: 60000,
        dueDate: '2021-12-22T00:00:00.000Z'
      })
      .expect(204)

    let res = await client
      .get('/projects/' + projectId)
      .auth(token, { type: 'bearer' })
      .expect('content-type', 'application/json')
      .expect(200)

    expect(expected.validate(res.body).error).to.equal(undefined)
  })

  it('tries to edit project whose project manager is not current user', async () => {
    const someProjectId = (
      await (await app.getRepository(ProjectRepository)).findOne(
        { where: { projectManagerId: { neq: currentUserId } } },
        { fields: { id: true } }
      )
    )?.id

    await client
      .patch('/projects/' + someProjectId)
      .auth(token, { type: 'bearer' })
      .send({
        minDailyHours: 4,
        maxWeeklyHours: 100,
        reminderEnabled: true,
        name: 'TestProjectChangedName',
        firmId: '903bac99-4a97-4963-9aa8-da0c9caa884b',
        description: 'Testing project',
        budget: 60000,
        dueDate: '2022-12-22T13:28:59.609Z'
      })
      .expect(405)
  })
})
