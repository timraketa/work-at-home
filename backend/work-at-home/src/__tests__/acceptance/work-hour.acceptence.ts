import { Client, expect } from '@loopback/testlab'
import { WorkAtHomeApplication } from '../..'
import { WorkHourRepository } from '../../repositories'
import { setupApplication } from './test-helper'

import Joi from 'joi'

describe('WorkHour', () => {
  let app: WorkAtHomeApplication
  let client: Client
  let token: string
  let workHourId: string

  before('setupApplication', async () => {
    ;({ app, client } = await setupApplication())

    await client
      .post('/users/login')
      .send({
        username: 'adminTest',
        password: 'test'
      })
      .expect(200)
      .then(res => {
        if (res) token = res.body.token
      })
  })

  after(async () => {
    await (await app.getRepository(WorkHourRepository)).deleteById(workHourId)
    await app.stop()
  })

  it('adds new work hour', async () => {
    let expected = Joi.object({
      id: Joi.string().uuid(),
      description: 'Work hour for test',
      workDayId: 'c6a74cd0-44f2-11eb-b378-0242ac130002',
      projectId: '5625d36e-44f2-11eb-b378-0242ac130002'
    })

    let res = await client
      .post('/work-days/c6a74cd0-44f2-11eb-b378-0242ac130002/work-hours')
      .auth(token, { type: 'bearer' })
      .send({
        description: 'Work hour for test',
        projectId: '5625d36e-44f2-11eb-b378-0242ac130002'
      })
      .expect(200)

    expect(expected.validate(res.body).error).to.equal(undefined)

    workHourId = res.body.id
  })

  it("adds new work hour to work day that doesn't exist", async () => {
    await client
      .post('/work-days/92c37fe4-0c7e-41c1-af83-cdc0f5292a97/work-hours')
      .auth(token, { type: 'bearer' })
      .send({
        description: 'Work hour for test',
        projectId: '5625d36e-44f2-11eb-b378-0242ac130002'
      })
      .expect(400)
  })
})
