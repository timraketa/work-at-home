import { authenticate, AuthenticationBindings } from '@loopback/authentication'
import { Getter, inject } from '@loopback/core'
import { repository } from '@loopback/repository'
import { get, getModelSchemaRef } from '@loopback/rest'
import { MyUserProfile } from '../auth-types'
import { PermissionKeys } from '../authorization/permission-keys'
import { HomePageListDTO } from '../dto/HomePageListDTO'
import { UserFirmDTO } from '../dto/UserFirmDTO'
import { Firm, Permission } from '../models'
import {
  FirmRepository,
  FirmUserRepository,
  RoleRepository,
  UserRepository,
  UserUserRoleFirmRepository
} from '../repositories'

export class UserFirmController {
  constructor(
    @repository(UserRepository) protected userRepository: UserRepository,
    @repository(UserUserRoleFirmRepository)
    protected userUserRoleFirmRepository: UserUserRoleFirmRepository,
    @repository(FirmUserRepository)
    protected readonly firmUserRepository: FirmUserRepository,
    @repository(RoleRepository)
    protected readonly roleRepository: RoleRepository,
    @repository(FirmRepository)
    protected readonly firmRepository: FirmRepository,
    @inject.getter(AuthenticationBindings.CURRENT_USER)
    protected getCurrentUser: Getter<MyUserProfile>
  ) {}

  @get('/users/firms', {
    responses: {
      '200': {
        description: 'Array of User has many Firm through FirmUser',
        content: {
          'application/json': {
            schema: { type: 'array', items: getModelSchemaRef(UserFirmDTO) }
          }
        }
      }
    }
  })
  @authenticate({
    strategy: 'jwt',
    options: {
      required: [PermissionKeys.ListUsers]
    }
  })
  async find(): Promise<UserFirmDTO[]> {
    const userId = (await this.getCurrentUser()).id

    const firmUsers = await this.firmUserRepository.find({
      where: {
        userId
      }
    })

    const userUserRoleFirms = await this.userUserRoleFirmRepository.find({
      where: {
        userId
      }
    })

    const roles = await this.roleRepository.find()

    const roleMap = new Map<number, string>()

    roles.forEach(role => {
      roleMap.set(role.id, role.name)
    })

    const userUserRoleFirmMap = new Map<string, string[]>()

    userUserRoleFirms.forEach(uurf => {
      let temp: string[] = []

      if (userUserRoleFirmMap.get(uurf.firmId)) {
        temp = userUserRoleFirmMap.get(uurf.firmId)!
      }

      temp.push(roleMap.get(uurf.roleId)!)

      userUserRoleFirmMap.set(uurf.firmId, temp)
    })

    const firmIds = [...userUserRoleFirmMap.keys()]

    const firms = await this.firmRepository.find({
      where: { id: { inq: firmIds } }
    })

    const firmNameMap = new Map<string, string>()

    firms.forEach(firm => {
      firmNameMap.set(firm.id, firm.name)
    })

    const firmEmailMap = new Map<string, { email: string; name: string }>()

    firmUsers.forEach(firmUser => {
      firmEmailMap.set(firmUser.firmId, {
        email: firmUser.firmUserEmail,
        name: firmNameMap.get(firmUser.firmId)!
      })
    })

    return [...userUserRoleFirmMap.entries()].map(firmIdRoles => {
      const h = firmEmailMap.get(firmIdRoles[0])!
      return {
        firmId: firmIdRoles[0],
        firmName: h.name,
        firmEmail: h.email,
        roles: firmIdRoles[1]
      }
    })
  }

  @get('/users/firms/admin', {
    responses: {
      '200': {
        description: 'Array of firms that the user is an admin of',
        content: {
          'application/json': {
            schema: {
              type: 'array',
              items: {
                properties: {
                  id: {
                    type: 'string'
                  },
                  name: {
                    type: 'string'
                  }
                }
              }
            }
          }
        }
      }
    }
  })
  @authenticate({
    strategy: 'jwt',
    options: {
      required: [PermissionKeys.ListUsers]
    }
  })
  async adminFirms(): Promise<Pick<Firm, 'id' | 'name'>[]> {
    const userId = (await this.getCurrentUser()).id

    return this.firmRepository.find({
      where: {
        adminId: userId
      }
    })
  }

  @get('/users/home', {
    responses: {
      '200': {
        description: 'Array of users with firm roles',
        content: {
          'application/json': {
            schema: { type: 'array', items: getModelSchemaRef(UserFirmDTO) }
          }
        }
      }
    }
  })
  async homePage(): Promise<HomePageListDTO[]> {
    const users = await this.userRepository.find()

    const userUserRoleFirms = await this.userUserRoleFirmRepository.find()

    const roles = await this.roleRepository.find()

    const firms = await this.firmRepository.find()

    const firmMap = new Map<string, string>()

    firms.forEach(firm => firmMap.set(firm.id, firm.name))

    const roleMap = new Map<number, string>()

    roles.forEach(role => {
      roleMap.set(role.id, role.name)
    })

    const userMap: Map<string, string> = new Map()

    users.forEach(user => {
      userMap.set(user.id, user.username)
    })

    const userUserRoleFirmMap = new Map<string, Map<string, string[]>>()

    userUserRoleFirms.forEach(uurf => {
      let temp: Map<string, string[]> = new Map()

      if (userUserRoleFirmMap.get(uurf.userId)) {
        temp = userUserRoleFirmMap.get(uurf.userId)!
      }

      let roles: string[] = []
      if (temp.get(uurf.firmId)) {
        roles = temp.get(uurf.firmId)!
      }

      roles.push(roleMap.get(uurf.roleId)!)
      temp.set(uurf.firmId, roles)

      userUserRoleFirmMap.set(uurf.userId, temp)
    })

    const returnType: HomePageListDTO[] = []

    userUserRoleFirmMap.forEach((firmRoleMap, userId) => {
      firmRoleMap.forEach((roles, firmId) => {
        returnType.push({
          username: userMap.get(userId)!,
          firmName: firmMap.get(firmId)!,
          roles
        })
      })
    })

    return returnType
  }
}
