import { repository } from '@loopback/repository'
import {
  get,
  getModelSchemaRef,
  HttpErrors,
  param,
  post,
  requestBody
} from '@loopback/rest'
import moment from 'moment'
import { DayDTO } from '../dto/DayDTO'
import { StatisticsRequestDTO } from '../dto/StatisticsRequestDTO'
import { StatisticsResponseDTO } from '../dto/StatisticsResponseDTO'
import { StatisticsProjectResponseDTO } from '../dto/StatisticsProjectResponseDTO'
import { Project, User, WorkHour, WorkDay } from '../models'
import {
  WorkDayRepository,
  WorkHourRepository,
  UserRepository,
  ProjectRepository
} from '../repositories'
import { Getter, inject } from '@loopback/core'
import { AuthenticationBindings, authenticate } from '@loopback/authentication'
import { MyUserProfile } from '../auth-types'
import { PermissionKeys } from '../authorization/permission-keys'

export class StatisticsController {
  constructor(
    @repository(WorkDayRepository)
    protected workDayRepository: WorkDayRepository,
    @repository(WorkHourRepository)
    protected workHourRepository: WorkHourRepository,
    @repository(UserRepository)
    protected userRepository: UserRepository,
    @repository(ProjectRepository)
    protected projectRepository: ProjectRepository,
    @inject.getter(AuthenticationBindings.CURRENT_USER)
    protected getCurrentUser: Getter<MyUserProfile>
  ) {}

  @post('/statistics/user', {
    responses: {
      '200': {
        description: 'Work hours between two dates for every day',
        content: {
          'application/json': {
            schema: {
              type: 'array',
              items: getModelSchemaRef(StatisticsResponseDTO, {
                includeRelations: true
              })
            }
          }
        }
      }
    }
  })
  @authenticate({
    strategy: 'jwt',
    options: {
      required: [PermissionKeys.UserStatistics]
    }
  })
  async find(
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(StatisticsRequestDTO)
        }
      }
    })
    body: Omit<StatisticsRequestDTO, 'projectId'>
  ): Promise<StatisticsResponseDTO> {
    const userId = (await this.getCurrentUser()).id

    if (!this.userRepository.exists(userId)) {
      throw new HttpErrors.UnprocessableEntity(`User doesn't exist.`)
    }

    const fromDate = moment.utc(new Date(body.from)).startOf('day') //body.from is date with time stamp in UCT (== Z) time zone, new Date() returns same date with timestamp in GMT+1h time zone
    const toDate = moment.utc(new Date(body.to)).endOf('day')

    const userWorkDays = await this.workDayRepository.find({
      where: {
        userId: userId,
        date: {
          between: [fromDate.toDate(), toDate.toDate()]
        },
        isFree: false
      }
    })

    const userHourlyRate = (await this.userRepository.findById(userId))
      .hourlyRate

    const span = moment.duration(toDate.diff(fromDate)).asDays()
    const daysDto: DayDTO[] = []

    const userWorkDayIds = userWorkDays.map(day => day.id)
    const allHours = await this.workHourRepository.find({
      where: {
        workDayId: {
          inq: userWorkDayIds
        }
      }
    })

    for (let i = 0; i <= span; i++) {
      var flag = true

      for (let j = 0; j < userWorkDays.length; j++) {
        const day = userWorkDays[j]
        const dayDate = moment
          .utc(new Date(day.date))
          .add(1, 'hour')
          .startOf('day')
        const madeUpDate = moment
          .utc(new Date(body.from))
          .startOf('day')
          .add(i, 'day')

        if (dayDate.isBetween(madeUpDate, madeUpDate, 'days', '[]')) {
          flag = false

          const hours: WorkHour[] = []
          allHours.forEach(function (hour) {
            if (hour.workDayId == day.id) hours.push(hour)
          })

          daysDto.push({
            projects: [],
            dayHours: hours.length,
            dayPrice: userHourlyRate * hours.length
          })

          break
        }
      }

      if (flag) {
        daysDto.push({
          projects: [],
          dayHours: 0,
          dayPrice: 0
        })
      }
    }

    return {
      from: body.from,
      to: body.to,
      days: daysDto
    }
  }

  @post('/statistics/user/projects', {
    responses: {
      '200': {
        description:
          'Work hours for every day between two dates, separated by projects',
        content: {
          'application/json': {
            schema: {
              type: 'array',
              items: getModelSchemaRef(StatisticsResponseDTO, {
                includeRelations: true
              })
            }
          }
        }
      }
    }
  })
  @authenticate({
    strategy: 'jwt',
    options: {
      required: [PermissionKeys.UserStatistics]
    }
  })
  async findByProjects(
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(StatisticsRequestDTO)
        }
      }
    })
    body: Omit<StatisticsRequestDTO, 'projectId'>
  ): Promise<StatisticsResponseDTO> {
    const userId = (await this.getCurrentUser()).id

    if (!this.userRepository.exists(userId)) {
      throw new HttpErrors.UnprocessableEntity(`User doesn't exist.`)
    }

    const fromDate = moment.utc(new Date(body.from)).startOf('day')
    const toDate = moment.utc(new Date(body.to)).endOf('day')

    const userWorkDays = await this.workDayRepository.find({
      where: {
        userId: userId,
        date: {
          between: [fromDate.toDate(), toDate.toDate()]
        },
        isFree: false
      }
    })

    const userHourlyRate = (await this.userRepository.findById(userId))
      .hourlyRate

    const span = moment.duration(toDate.diff(fromDate)).asDays()
    const daysDto: DayDTO[] = []

    const userWorkDayIds = userWorkDays.map(day => day.id)
    const allHours = await this.workHourRepository.find({
      where: {
        workDayId: {
          inq: userWorkDayIds
        }
      }
    })
    let allDayProjectsIds = allHours.map(hour => hour.projectId)
    let allDayProjects = await this.projectRepository.find({
      where: {
        id: {
          inq: allDayProjectsIds
        }
      }
    })

    for (let i = 0; i <= span; i++) {
      var flag = true

      for (let j = 0; j < userWorkDays.length; j++) {
        const day = userWorkDays[j]
        const dayDate = moment
          .utc(new Date(day.date))
          .add(1, 'hour')
          .startOf('day')
        const madeUpDate = moment
          .utc(new Date(body.from))
          .startOf('day')
          .add(i, 'day')

        if (dayDate.isBetween(madeUpDate, madeUpDate, 'days', '[]')) {
          flag = false

          const hours: WorkHour[] = []
          allHours.forEach(function (hour) {
            if (hour.workDayId == day.id) hours.push(hour)
          })

          let dayProjectsIds = hours.map(hour => hour.projectId)
          dayProjectsIds = [...new Set(dayProjectsIds)]

          let dayProjects: Project[] = []
          allDayProjects.forEach(function (project) {
            if (dayProjectsIds.includes(project.id)) dayProjects.push(project)
          })
          dayProjects = [...new Set(dayProjects)]

          let projects = dayProjects.map(project => {
            return {
              projectId: project.id,
              projectName: project.name,
              hours: 0,
              price: 0
            }
          })

          hours.forEach(function (hour) {
            projects.forEach(function (project) {
              if (hour.projectId == project.projectId) {
                project.hours += 1
                project.price += +userHourlyRate
              }
            })
          })

          const projectsReturn = projects.map(project => {
            return {
              projectName: project.projectName,
              hours: project.hours,
              price: project.price
            }
          })

          daysDto.push({
            projects: projectsReturn,
            dayHours: hours.length,
            dayPrice: userHourlyRate * hours.length
          })

          break
        }
      }

      if (flag) {
        daysDto.push({
          projects: [],
          dayHours: 0,
          dayPrice: 0
        })
      }
    }

    return {
      from: body.from,
      to: body.to,
      days: daysDto
    }
  }

  @post('/statistics/workHours/projects', {
    responses: {
      '200': {
        description: 'Array of Checkpoint has many Checklist',
        content: {
          'application/json': {
            schema: {
              type: 'array',
              items: getModelSchemaRef(StatisticsProjectResponseDTO, {
                exclude: ['spending'],
                partial: true
              })
            }
          }
        }
      }
    }
  })
  @authenticate({
    strategy: 'jwt',
    options: {
      required: [PermissionKeys.ProjectStatistics]
    }
  })
  async getProjectWorkHours(
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(StatisticsRequestDTO, {
            partial: true
          })
        }
      }
    })
    body: StatisticsRequestDTO
  ): Promise<Omit<StatisticsProjectResponseDTO, 'spending'>[]> {
    const project = await this.projectRepository.findById(body.projectId)
    const userId = (await this.getCurrentUser()).id

    if (!project) {
      throw new HttpErrors.UnprocessableEntity('Project not found')
    }

    if (project.projectManagerId !== userId) {
      throw new HttpErrors.Forbidden(
        'You have to be a project manager of this project to view its stats!'
      )
    }

    const workDayIds = (
      await this.workHourRepository.find(
        { where: { projectId: body.projectId } },
        { fields: { workDayId: true } }
      )
    ).map(hour => hour.workDayId)

    const fromDate = moment.utc(new Date(body.from)).startOf('day')
    const toDate = moment.utc(new Date(body.to)).endOf('day')

    const arr = (
      await this.workDayRepository.find(
        {
          where: {
            id: { inq: workDayIds },
            date: {
              between: [fromDate.toDate(), toDate.toDate()]
            }
          }
        },
        { fields: { id: true, date: true } }
      )
    ).map(d => {
      const date = new Date(d.date)
      date.setHours(1, 0, 0, 0)
      const id = d.id
      return { id, date }
    })
    const datesMap = new Map(arr.map(i => [i.id, i.date]))
    const dates: Date[] = []
    workDayIds.forEach(id => {
      if (datesMap.get(id) != undefined) dates.push(datesMap.get(id)!!)
    })

    const checkedDates: number[] = []
    const workHoursEachDay: Omit<
      StatisticsProjectResponseDTO,
      'spending'
    >[] = []

    dates.forEach(d => {
      if (!checkedDates.includes(d.getTime())) {
        checkedDates.push(d.getTime())
        const workHours = dates.filter(date => date.getTime() === d.getTime())
          .length
        const date = d
        workHoursEachDay.push({ date, workHours })
      }
    })

    workHoursEachDay.sort((a, b) =>
      a.date.getTime() > b.date.getTime() ? 1 : -1
    )
    return workHoursEachDay
  }

  @post('/statistics/spending/projects', {
    responses: {
      '200': {
        description: 'Array of Checkpoint has many Checklist',
        content: {
          'application/json': {
            schema: {
              type: 'array',
              items: getModelSchemaRef(StatisticsProjectResponseDTO, {
                exclude: ['workHours'],
                partial: true
              })
            }
          }
        }
      }
    }
  })
  @authenticate({
    strategy: 'jwt',
    options: {
      required: [PermissionKeys.ProjectStatistics]
    }
  })
  async getProjectSpending(
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(StatisticsRequestDTO, {
            partial: true
          })
        }
      }
    })
    body: StatisticsRequestDTO
  ): Promise<Omit<StatisticsProjectResponseDTO, 'workHours'>[]> {
    const workDayIds = (
      await this.workHourRepository.find(
        { where: { projectId: body.projectId } },
        { fields: { workDayId: true } }
      )
    ).map(hour => hour.workDayId)

    const arr = (await this.userRepository.find(undefined, {
      fields: { id: true, hourlyRate: true }
    })) as Pick<User, 'id' | 'hourlyRate'>[]
    const hourlyRates = new Map(arr.map(i => [i.id, i.hourlyRate]))

    const fromDate = moment.utc(new Date(body.from)).startOf('day')
    const toDate = moment.utc(new Date(body.to)).endOf('day')

    const arr2 = (
      await this.workDayRepository.find(
        {
          where: {
            id: { inq: workDayIds },
            date: {
              between: [fromDate.toDate(), toDate.toDate()]
            }
          }
        },
        { fields: { id: true, date: true, userId: true } }
      )
    ).map(d => {
      const id = d.id
      const date = new Date(d.date)
      date.setHours(1, 0, 0, 0)
      const hourlyRate = hourlyRates.get(d.userId)!!
      return { id, date, hourlyRate }
    })
    const hoursMap = new Map(
      arr2.map(i => [i.id, { date: i.date, hourlyRate: i.hourlyRate }])
    )

    const hours: { date: Date; hourlyRate: number }[] = []
    workDayIds.forEach(id => {
      if (hoursMap.get(id) != undefined) hours.push(hoursMap.get(id)!!)
    })

    const checkedDates: number[] = []
    const spendingEachDay: Omit<
      StatisticsProjectResponseDTO,
      'workHours'
    >[] = []
    hours.forEach(hour => {
      if (!checkedDates.includes(hour.date.getTime())) {
        checkedDates.push(hour.date.getTime())
        const spending = hours
          .filter(h => h.date.getTime() === hour.date.getTime())
          .reduce((sum, current) => +sum + +current.hourlyRate, 0)
        const date = hour.date
        spendingEachDay.push({ date, spending })
      }
    })

    spendingEachDay.sort((a, b) =>
      a.date.getTime() > b.date.getTime() ? 1 : -1
    )
    return spendingEachDay
  }
}
