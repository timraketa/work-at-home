import { authenticate } from '@loopback/authentication'
import {
  Count,
  CountSchema,
  Filter,
  repository,
  Where
} from '@loopback/repository'
import {
  del,
  get,
  getModelSchemaRef,
  getWhereSchemaFor,
  param,
  patch,
  post,
  requestBody
} from '@loopback/rest'
import { PermissionKeys } from '../authorization/permission-keys'
import { IdsDTO } from '../dto/IdsDTO'
import { Checklist, ChecklistItem } from '../models'
import { ChecklistRepository } from '../repositories'

export class ChecklistChecklistItemController {
  constructor(
    @repository(ChecklistRepository)
    protected checklistRepository: ChecklistRepository
  ) {}

  @post('/checklists/{id}/checklist-items', {
    responses: {
      '200': {
        description: 'Checklist model instance',
        content: {
          'application/json': { schema: getModelSchemaRef(ChecklistItem) }
        }
      }
    }
  })
  @authenticate({
    strategy: 'jwt',
    options: {
      required: [PermissionKeys.CreateChecklist]
    }
  })
  async create(
    @param.path.string('id') id: typeof Checklist.prototype.id,
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(ChecklistItem, {
            title: 'NewChecklistItemInChecklist',
            exclude: ['id'],
            optional: ['checklistId']
          })
        }
      }
    })
    checklistItem: Omit<ChecklistItem, 'id'>
  ): Promise<ChecklistItem> {
    return this.checklistRepository.checklistItems(id).create(checklistItem)
  }

  @patch('/checklists/{id}/checklist-items', {
    responses: {
      '200': {
        description: 'Checklist.ChecklistItem PATCH success count',
        content: { 'application/json': { schema: CountSchema } }
      }
    }
  })
  @authenticate({
    strategy: 'jwt',
    options: {
      required: [PermissionKeys.UpdateChecklist]
    }
  })
  async patch(
    @param.path.string('id') id: string,
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(ChecklistItem, {
            exclude: ['id'],
            partial: true
          })
        }
      }
    })
    checklistItem: Partial<ChecklistItem>,
    @param.query.object('where', getWhereSchemaFor(ChecklistItem))
    where?: Where<ChecklistItem>
  ): Promise<Count> {
    return this.checklistRepository
      .checklistItems(id)
      .patch(checklistItem, where)
  }

  @del('/checklists/{id}/checklist-items', {
    responses: {
      '200': {
        description: 'Checklist.ChecklistItem DELETE success count',
        content: { 'application/json': { schema: CountSchema } }
      }
    }
  })
  @authenticate({
    strategy: 'jwt',
    options: {
      required: [PermissionKeys.DeleteChecklist]
    }
  })
  async delete(
    @param.path.string('id') id: string,
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(IdsDTO)
        }
      }
    })
    itemIds: IdsDTO
  ): Promise<Count> {
    return this.checklistRepository
      .checklistItems(id)
      .delete({ id: { inq: itemIds.ids } })
  }
}
