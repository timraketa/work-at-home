import { authenticate } from '@loopback/authentication'
import { Count, CountSchema, Filter, repository } from '@loopback/repository'
import {
  del,
  get,
  getModelSchemaRef,
  param,
  post,
  requestBody
} from '@loopback/rest'
import { PermissionKeys } from '../authorization/permission-keys'
import { IdsDTO } from '../dto/IdsDTO'
import { Team, User } from '../models'
import {
  TeamRepository,
  TeamTeamMemberRepository,
  UserRepository
} from '../repositories'

export class TeamUserController {
  constructor(
    @repository(TeamRepository) protected teamRepository: TeamRepository,
    @repository(UserRepository) protected userRepository: UserRepository,
    @repository(TeamTeamMemberRepository)
    protected teamTeamMemberRepository: TeamTeamMemberRepository
  ) {}

  @post('/teams/{id}/users', {
    responses: {
      '200': {
        description: 'Array of User connected to Team',
        content: {
          'application/json': {
            schema: { type: 'array', items: getModelSchemaRef(User) }
          }
        }
      }
    }
  })
  @authenticate({
    strategy: 'jwt',
    options: {
      required: [PermissionKeys.UpdateTeam]
    }
  })
  async create(
    @param.path.string('id') id: typeof Team.prototype.id,
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(IdsDTO)
        }
      }
    })
    usersIds: IdsDTO
  ): Promise<Omit<User, 'password'>[]> {
    const teamTeamMembersToCreate = usersIds.ids.map(memberId => {
      return {
        teamMemberId: memberId,
        teamId: id
      }
    })

    await this.teamTeamMemberRepository.createAll(teamTeamMembersToCreate)
    return this.userRepository.find({
      where: { id: { inq: usersIds.ids } },
      fields: { password: false }
    })
  }

  @del('/teams/{id}/users', {
    responses: {
      '200': {
        description: 'Team.User DELETE success count',
        content: { 'application/json': { schema: CountSchema } }
      }
    }
  })
  @authenticate({
    strategy: 'jwt',
    options: {
      required: [PermissionKeys.UpdateTeam]
    }
  })
  async delete(
    @param.path.string('id') id: string,
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(IdsDTO)
        }
      }
    })
    usersIds: IdsDTO
  ): Promise<Count> {
    return this.teamTeamMemberRepository.deleteAll({
      teamMemberId: { inq: usersIds.ids },
      teamId: id
    })
  }
}
