import { authenticate, AuthenticationBindings } from '@loopback/authentication'
import { Getter, inject } from '@loopback/core'
import {
  Count,
  CountSchema,
  FilterExcludingWhere,
  repository,
  Where
} from '@loopback/repository'
import {
  del,
  get,
  getModelSchemaRef,
  HttpErrors,
  param,
  patch,
  post,
  requestBody
} from '@loopback/rest'
import { MyUserProfile } from '../auth-types'
import { PermissionKeys } from '../authorization/permission-keys'
import { CreateUserDTO } from '../dto'
import {
  PasswordHasherBindings,
  TokenServiceBindings,
  UserServiceBindings
} from '../keys'
import { User } from '../models'
import {
  Credentials,
  FirmRepository,
  UserRepository,
  UserUserRoleFirmRepository,
  FirmUserRepository,
  RoleRepository
} from '../repositories'
import { validateCredentials } from '../services'
import { BcryptHasher } from '../services/hash.password'
import { JWTService } from '../services/jwt-service'
import { MyUserService } from '../services/user-service'
import { EmailService } from '../services/email-service'
import { EmailServiceBindings } from '../keys'
import { CredentialsDTO } from '../dto/CredentialsDTO'
import { UserSignUpDTO } from '../dto/UserSignUpDTO'
import _ from 'lodash'

export class UserController {
  constructor(
    @inject(TokenServiceBindings.TOKEN_SERVICE)
    protected jwtService: JWTService,
    @inject(UserServiceBindings.USER_SERVICE)
    protected userService: MyUserService,
    @inject(PasswordHasherBindings.PASSWORD_HASHER)
    protected hasher: BcryptHasher,
    @repository(UserRepository)
    protected userRepository: UserRepository,
    @repository(UserUserRoleFirmRepository)
    protected userUserRoleFirmRepository: UserUserRoleFirmRepository,
    @repository(FirmRepository)
    protected firmRepository: FirmRepository,
    @repository(FirmUserRepository)
    protected firmUserRepository: FirmUserRepository,
    @repository(RoleRepository)
    protected roleRepository: RoleRepository,
    @inject.getter(AuthenticationBindings.CURRENT_USER)
    protected getCurrentUser: Getter<MyUserProfile>,
    @inject(EmailServiceBindings.SEND_MAIL)
    protected emailService: EmailService
  ) {}

  @post('/users/signup/{username}', {
    responses: {
      '200': {
        description: 'Sign up',
        content: {
          'application/json': {
            schema: getModelSchemaRef(User, {
              title: 'NewUser',
              exclude: ['password']
            })
          }
        }
      }
    }
  })
  async signup(
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(UserSignUpDTO)
        }
      }
    })
    userData: Omit<User, 'id' | 'username'>,
    @param.path.string('username') username: string
  ) {
    validateCredentials({
      username: username,
      password: userData.password
    })

    const user = await this.userRepository.findOne({
      where: {
        username: username
      }
    })

    if (!user) {
      throw new HttpErrors.UnprocessableEntity(
        'User with username: ' + username + " doesn't exist."
      )
    }

    if (user.isRegistered) {
      throw new HttpErrors.UnprocessableEntity(
        'This user is already registered.'
      )
    }

    userData.password = await this.hasher.hashPassword(userData.password)

    user.password = userData.password
    user.firstName = userData.firstName
    user.lastName = userData.lastName
    user.hourlyRate = userData.hourlyRate
    user.phoneNumber = userData.phoneNumber
    user.isRegistered = true

    await this.userRepository.save(user, {
      where: {
        id: user.id
      }
    })

    const { password, ...returnType } = { ...user }
    return returnType
  }

  @post('/users/login', {
    responses: {
      '200': {
        description: 'Token',
        content: {
          'application/json': {
            schema: {
              type: 'object',
              properties: {
                token: {
                  type: 'string'
                }
              }
            }
          }
        }
      }
    }
  })
  async login(
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(CredentialsDTO)
        }
      }
    })
    credentials: Credentials
  ): Promise<{ token: string }> {
    const user = await this.userService.verifyCredentials({
      username: credentials.username,
      password: credentials.password
    })

    const userProfile = await this.userService.convertToUserProfile(user)

    const token = await this.jwtService.generateToken(userProfile)
    return { token }
  }

  @post('/users', {
    responses: {
      '200': {
        description: 'User created'
      }
    }
  })
  @authenticate({
    strategy: 'jwt',
    options: {
      required: [PermissionKeys.CreateUser]
    }
  })
  async create(
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(CreateUserDTO)
        }
      }
    })
    createUser: CreateUserDTO
  ): Promise<void> {
    const user = await this.userRepository.findOne({
      where: {
        username: createUser.username
      }
    })

    let userId: string
    let userDidExist: boolean = false

    if (user) {
      userId = user.id
      userDidExist = true
    } else {
      const user = await this.userRepository.create({
        username: createUser.username
      })

      userId = user.id
    }

    const firm = await this.firmRepository.findById(createUser.firmId)

    if (!firm) {
      throw new HttpErrors.UnprocessableEntity('Firm does not exist')
    }

    const entitiesToCreate = createUser.roles.map(roleId => {
      return {
        roleId,
        userId,
        firmId: firm.id
      }
    })

    await this.userUserRoleFirmRepository.createAll(entitiesToCreate)

    await this.firmUserRepository.create({
      firmId: firm.id,
      userId: userId,
      firmUserEmail: createUser.email
    })

    if (userDidExist) {
      await this.emailService.registeredSendMail(createUser.email, firm.name)
    } else {
      await this.emailService.notRegisteredSendMail(
        createUser.email,
        createUser.username
      )
    }
  }

  @get('/users/firmId={firmId}', {
    responses: {
      '200': {
        description: 'Array of User model instances',
        content: {
          'application/json': {
            schema: {
              type: 'array',
              items: getModelSchemaRef(User, { includeRelations: true })
            }
          }
        }
      }
    }
  })
  @authenticate({
    strategy: 'jwt',
    options: {
      required: [PermissionKeys.ListUsers]
    }
  })
  async find(
    @param.path.string('firmId') firmId: string
  ): Promise<Omit<User, 'password'>[]> {
    const currUser = await this.getCurrentUser()

    const firm = await this.userRepository.firms(currUser.id).find({
      where: {
        id: firmId
      },
      include: [
        {
          relation: 'users'
        }
      ]
    })

    if (!firm.length) {
      throw new HttpErrors.UnprocessableEntity('Firm not found!')
    }

    const userRoleFirm = await this.userUserRoleFirmRepository.find({
      where: {
        firmId
      }
    })

    const grouped = _.groupBy(userRoleFirm, 'userId')

    const roles = await this.roleRepository.find()

    const roleMap = new Map<number, string>()

    roles.forEach(role => {
      roleMap.set(role.id, role.name)
    })

    return firm[0].users.map(user => {
      return {
        ...user,
        firmRoles: grouped[user.id].map(
          userRoleFirm => roleMap.get(userRoleFirm.roleId)!
        )
      }
    })
  }

  @get('/users/id', {
    responses: {
      '200': {
        description: 'User model instance',
        content: {
          'application/json': {
            schema: getModelSchemaRef(User, { includeRelations: true })
          }
        }
      }
    }
  })
  @authenticate({
    strategy: 'jwt',
    options: {
      required: []
    }
  })
  async findById(
    @param.filter(User, { exclude: 'where' })
    filter?: FilterExcludingWhere<User>
  ): Promise<Omit<User, 'password'>> {
    const id = (await this.getCurrentUser()).id

    return this.userRepository.findById(id, {
      ...filter,
      fields: { password: false }
    })
  }

  @patch('/users', {
    responses: {
      '204': {
        description: 'User PATCH success'
      }
    }
  })
  @authenticate({
    strategy: 'jwt',
    options: {
      required: [PermissionKeys.EditUser]
    }
  })
  async updateById(
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(User, {
            exclude: ['id', 'password'],
            partial: true
          })
        }
      }
    })
    user: Omit<User, 'password' | 'id'>
  ): Promise<void> {
    const id = (await this.getCurrentUser()).id

    await this.userRepository.updateById(id, user)
  }

  @del('/users', {
    responses: {
      '204': {
        description: 'User DELETE success'
      }
    }
  })
  @authenticate({
    strategy: 'jwt',
    options: {
      required: [PermissionKeys.DeleteUsers]
    }
  })
  async deleteById(
    @requestBody.array(
      { schema: { type: 'string' } },
      { description: 'an array of user ids', required: true }
    )
    userIds: string[]
  ): Promise<void> {
    await this.userRepository.deleteAll({ id: { inq: userIds } })
  }
}
