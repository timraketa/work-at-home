import { authenticate } from '@loopback/authentication'
import {
  Count,
  CountSchema,
  Filter,
  repository,
  Where
} from '@loopback/repository'
import {
  del,
  get,
  getModelSchemaRef,
  getWhereSchemaFor,
  param,
  patch,
  post,
  requestBody
} from '@loopback/rest'
import { PermissionKeys } from '../authorization/permission-keys'
import { IdsDTO } from '../dto/IdsDTO'
import { Checkpoint, Checklist } from '../models'
import { ChecklistRepository, CheckpointRepository } from '../repositories'

export class CheckpointChecklistController {
  constructor(
    @repository(CheckpointRepository)
    protected checkpointRepository: CheckpointRepository
  ) {}

  @post('/checkpoints/{id}/checklists', {
    responses: {
      '200': {
        description: 'Checkpoint model instance',
        content: {
          'application/json': { schema: getModelSchemaRef(Checklist) }
        }
      }
    }
  })
  @authenticate({
    strategy: 'jwt',
    options: {
      required: [PermissionKeys.CreateChecklist]
    }
  })
  async create(
    @param.path.string('id') id: typeof Checkpoint.prototype.id,
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(Checklist, {
            title: 'NewChecklistInCheckpoint',
            exclude: ['id'],
            optional: ['checkpointId']
          })
        }
      }
    })
    checklist: Omit<Checklist, 'id'>
  ): Promise<Checklist> {
    return this.checkpointRepository.checklists(id).create(checklist)
  }

  @patch('/checkpoints/{id}/checklists', {
    responses: {
      '200': {
        description: 'Checkpoint.Checklist PATCH success count',
        content: { 'application/json': { schema: CountSchema } }
      }
    }
  })
  @authenticate({
    strategy: 'jwt',
    options: {
      required: [PermissionKeys.UpdateChecklist]
    }
  })
  async patch(
    @param.path.string('id') id: string,
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(Checklist, {
            exclude: ['id'],
            partial: true
          })
        }
      }
    })
    checklist: Partial<Checklist>,
    @param.query.object('where', getWhereSchemaFor(Checklist))
    where?: Where<Checklist>
  ): Promise<Count> {
    return this.checkpointRepository.checklists(id).patch(checklist, where)
  }

  @del('/checkpoints/{id}/checklists', {
    responses: {
      '200': {
        description: 'Checkpoint.Checklist DELETE success count',
        content: { 'application/json': { schema: CountSchema } }
      }
    }
  })
  @authenticate({
    strategy: 'jwt',
    options: {
      required: [PermissionKeys.DeleteChecklist]
    }
  })
  async delete(
    @param.path.string('id') id: string,
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(IdsDTO)
        }
      }
    })
    checklistIds: IdsDTO
  ): Promise<Count> {
    return this.checkpointRepository
      .checklists(id)
      .delete({ id: { inq: checklistIds.ids } })
  }
}
