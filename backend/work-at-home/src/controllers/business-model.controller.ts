import { Filter, repository } from '@loopback/repository'
import { param, get, getModelSchemaRef } from '@loopback/rest'
import { BusinessModel } from '../models'
import { BusinessModelRepository } from '../repositories'

export class BusinessModelController {
  constructor(
    @repository(BusinessModelRepository)
    public businessModelRepository: BusinessModelRepository
  ) {}
  @get('/business-models', {
    responses: {
      '200': {
        description: 'Array of BusinessModel model instances',
        content: {
          'application/json': {
            schema: {
              type: 'array',
              items: getModelSchemaRef(BusinessModel, {
                includeRelations: true
              })
            }
          }
        }
      }
    }
  })
  async find(
    @param.filter(BusinessModel) filter?: Filter<BusinessModel>
  ): Promise<BusinessModel[]> {
    return this.businessModelRepository.find(filter)
  }
}
