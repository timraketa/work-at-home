import { authenticate } from '@loopback/authentication'
import {
  Count,
  CountSchema,
  Filter,
  repository,
  Where
} from '@loopback/repository'
import {
  del,
  get,
  getModelSchemaRef,
  getWhereSchemaFor,
  param,
  patch,
  post,
  requestBody
} from '@loopback/rest'
import { PermissionKeys } from '../authorization/permission-keys'
import { IdsDTO } from '../dto/IdsDTO'
import { Project, Checkpoint } from '../models'
import { ProjectRepository } from '../repositories'

export class ProjectCheckpointController {
  constructor(
    @repository(ProjectRepository)
    protected projectRepository: ProjectRepository
  ) {}

  @get('/projects/{id}/checkpoints', {
    responses: {
      '200': {
        description: 'Array of Project has many Checkpoint',
        content: {
          'application/json': {
            schema: { type: 'array', items: getModelSchemaRef(Checkpoint) }
          }
        }
      }
    }
  })
  @authenticate({
    strategy: 'jwt',
    options: {
      required: []
    }
  })
  async find(
    @param.path.string('id') id: string,
    @param.query.object('filter') filter?: Filter<Checkpoint>
  ): Promise<Checkpoint[]> {
    return this.projectRepository.checkpoints(id).find({
      ...filter,
      include: [
        {
          relation: 'checklists',
          scope: {
            include: [
              {
                relation: 'checklistItems'
              }
            ]
          }
        }
      ]
    })
  }

  @post('/projects/{id}/checkpoints', {
    responses: {
      '200': {
        description: 'Project model instance',
        content: {
          'application/json': { schema: getModelSchemaRef(Checkpoint) }
        }
      }
    }
  })
  @authenticate({
    strategy: 'jwt',
    options: {
      required: [PermissionKeys.CreateCheckpoint]
    }
  })
  async create(
    @param.path.string('id') id: typeof Project.prototype.id,
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(Checkpoint, {
            title: 'NewCheckpointInProject',
            exclude: ['id'],
            optional: ['projectId']
          })
        }
      }
    })
    checkpoint: Omit<Checkpoint, 'id'>
  ): Promise<Checkpoint> {
    return this.projectRepository.checkpoints(id).create(checkpoint)
  }

  @patch('/projects/{id}/checkpoints', {
    responses: {
      '200': {
        description: 'Project.Checkpoint PATCH success count',
        content: { 'application/json': { schema: CountSchema } }
      }
    }
  })
  @authenticate({
    strategy: 'jwt',
    options: {
      required: [PermissionKeys.UpdateCheckpoint]
    }
  })
  async patch(
    @param.path.string('id') id: string,
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(Checkpoint, {
            exclude: ['id'],
            partial: true
          })
        }
      }
    })
    checkpoint: Partial<Checkpoint>,
    @param.query.object('where', getWhereSchemaFor(Checkpoint))
    where?: Where<Checkpoint>
  ): Promise<Count> {
    return this.projectRepository.checkpoints(id).patch(checkpoint, where)
  }

  @del('/projects/{id}/checkpoints', {
    responses: {
      '200': {
        description: 'Project.Checkpoint DELETE success count',
        content: { 'application/json': { schema: CountSchema } }
      }
    }
  })
  @authenticate({
    strategy: 'jwt',
    options: {
      required: [PermissionKeys.DeleteCheckpoint]
    }
  })
  async delete(
    @param.path.string('id') id: string,
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(IdsDTO)
        }
      }
    })
    checkpointIds: IdsDTO
  ): Promise<Count> {
    return this.projectRepository
      .checkpoints(id)
      .delete({ id: { inq: checkpointIds.ids } })
  }
}
