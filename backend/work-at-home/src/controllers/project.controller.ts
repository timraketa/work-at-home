import { authenticate, AuthenticationBindings } from '@loopback/authentication'
import { Getter, inject } from '@loopback/core'
import { repository } from '@loopback/repository'
import {
  post,
  param,
  get,
  getModelSchemaRef,
  patch,
  del,
  requestBody,
  HttpErrors
} from '@loopback/rest'
import moment from 'moment'
import { MyUserProfile } from '../auth-types'
import { PermissionKeys } from '../authorization/permission-keys'
import { GetProjectsDTO } from '../dto/GetProjectsDTO'
import { Project } from '../models'
import {
  FirmRepository,
  ProjectRepository,
  UserRepository,
  WorkHourRepository
} from '../repositories'

export class ProjectController {
  constructor(
    @repository(ProjectRepository)
    protected readonly projectRepository: ProjectRepository,
    @repository(UserRepository)
    protected readonly userRepository: UserRepository,
    @repository(FirmRepository)
    protected readonly firmRepository: FirmRepository,
    @repository(WorkHourRepository)
    protected readonly workHourRepository: WorkHourRepository,
    @inject.getter(AuthenticationBindings.CURRENT_USER)
    protected getCurrentUser: Getter<MyUserProfile>
  ) {}

  @post('/projects', {
    responses: {
      '200': {
        description: 'Project model instance',
        content: { 'application/json': { schema: getModelSchemaRef(Project) } }
      }
    }
  })
  @authenticate({
    strategy: 'jwt',
    options: {
      required: [PermissionKeys.CreateProject]
    }
  })
  async create(
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(Project, {
            title: 'NewProject',
            exclude: ['id', 'projectManagerId']
          })
        }
      }
    })
    project: Omit<Project, 'id' | 'projectManagerId'>
  ): Promise<Project> {
    const projectManagerId = (await this.getCurrentUser()).id

    const user = await this.userRepository.find({
      where: {
        id: projectManagerId
      },
      include: [
        {
          relation: 'firms'
        }
      ]
    })

    if (!user) {
      throw new HttpErrors.UnprocessableEntity('User not found!')
    } else if (
      !user[0].firms.filter(firm => firm.id === project.firmId).length
    ) {
      throw new HttpErrors.MethodNotAllowed(
        "Canno't add projects to firms you are not a part of!"
      )
    }

    return this.projectRepository.create({ ...project, projectManagerId })
  }

  @get('/projects', {
    responses: {
      '200': {
        description: 'List of projects',
        content: {
          'application/json': {
            schema: getModelSchemaRef(GetProjectsDTO)
          }
        }
      }
    }
  })
  @authenticate({
    strategy: 'jwt',
    options: {
      required: [PermissionKeys.ListProjects]
    }
  })
  async find(): Promise<GetProjectsDTO[]> {
    const userId = (await this.getCurrentUser()).id

    const user = await this.userRepository.findById(userId, {
      include: [
        {
          relation: 'firms'
        }
      ]
    })

    if (!user) {
      throw new HttpErrors.UnprocessableEntity('User not found!')
    }

    const projects = await this.projectRepository.find({
      where: {
        firmId: {
          inq: user.firms.map(firm => firm.id)
        }
      },
      include: [
        {
          relation: 'projectManager'
        },
        {
          relation: 'firm'
        }
      ]
    })

    const projectIds = projects.map(project => project.id)

    const workHours = await this.workHourRepository.find({
      where: {
        projectId: {
          inq: projectIds
        }
      },
      include: [
        {
          relation: 'workDay'
        }
      ]
    })

    const dailyHourMap: Map<string, number> = new Map()
    const weeklyHourMap: Map<string, number> = new Map()

    workHours.forEach(workHour => {
      if (workHour.workDay.userId === userId) {
        let temp = dailyHourMap.get(workHour.projectId) || 0

        if (moment(workHour.workDay.date).diff(moment.now(), 'days') === 0) {
          temp++
          dailyHourMap.set(workHour.projectId, temp)
        }

        temp = weeklyHourMap.get(workHour.projectId) || 0

        if (
          moment(workHour.workDay.date).week() === moment(moment.now()).week()
        ) {
          temp++
          weeklyHourMap.set(workHour.projectId, temp)
        }
      }
    })

    return projects.map(project => {
      return {
        id: project.id,
        name: project.name,
        minDailyHours: project.minDailyHours,
        maxWeeklyHours: project.maxWeeklyHours,
        firmName: project.firm.name,
        projectManager: project.projectManager.username,
        currDailyHours: dailyHourMap.get(project.id) || 0,
        currWeeklyHours: weeklyHourMap.get(project.id) || 0
      }
    })
  }

  @get('/projects/{id}', {
    responses: {
      '200': {
        description: 'Project model instance',
        content: {
          'application/json': {
            schema: getModelSchemaRef(Project, { includeRelations: true })
          }
        }
      }
    }
  })
  @authenticate({
    strategy: 'jwt',
    options: {
      required: [PermissionKeys.ListProjects]
    }
  })
  async findById(@param.path.string('id') id: string): Promise<Project> {
    const userId = (await this.getCurrentUser()).id

    const user = await this.userRepository.findById(userId, {
      include: [
        {
          relation: 'firms'
        }
      ]
    })

    if (!user) {
      throw new HttpErrors.UnprocessableEntity('User not found!')
    }

    const project = await this.projectRepository.findById(id, {
      include: [
        {
          relation: 'projectManager'
        },
        {
          relation: 'teams',
          scope: {
            include: [
              {
                relation: 'users'
              },
              {
                relation: 'teamLead'
              }
            ]
          }
        },
        {
          relation: 'checkpoints',
          scope: {
            include: [
              {
                relation: 'checklists',
                scope: {
                  include: [
                    {
                      relation: 'checklistItems'
                    }
                  ]
                }
              }
            ]
          }
        }
      ]
    })

    if (!project) {
      throw new HttpErrors.NotFound('Project not found!')
    } else if (!user.firms.filter(firm => firm.id === project.firmId).length) {
      throw new HttpErrors.NotFound('Project not found in your firm!')
    }

    //TODO fix day off in dates
    let date = new Date(project.dueDate)
    date.setHours(1, 0, 0, 0)
    project.dueDate = date.toJSON()

    return project
  }

  @patch('/projects/{id}', {
    responses: {
      '204': {
        description: 'Project PATCH success'
      }
    }
  })
  @authenticate({
    strategy: 'jwt',
    options: {
      required: [PermissionKeys.UpdateProject]
    }
  })
  async updateById(
    @param.path.string('id') id: string,
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(Project, {
            exclude: ['id', 'projectManagerId', 'firmId'],
            partial: true
          })
        }
      }
    })
    project: Omit<Project, 'id' | 'projectManagerId' | 'firmId'>
  ): Promise<void> {
    const userId = (await this.getCurrentUser()).id

    const currProject = await this.projectRepository.findById(id)

    if (!currProject) {
      throw new HttpErrors.UnprocessableEntity("Project doesn't exist!")
    } else if (currProject.projectManagerId !== userId) {
      throw new HttpErrors.MethodNotAllowed(
        "Canno't edit project which you are not a manager of!"
      )
    }

    await this.projectRepository.updateById(id, project)
  }

  @del('/projects/{id}', {
    responses: {
      '204': {
        description: 'Project DELETE success'
      }
    }
  })
  @authenticate({
    strategy: 'jwt',
    options: {
      required: [PermissionKeys.DeleteProject]
    }
  })
  async deleteById(@param.path.string('id') id: string): Promise<void> {
    const userId = (await this.getCurrentUser()).id

    const currProject = await this.projectRepository.findById(id)

    if (!currProject) {
      throw new HttpErrors.UnprocessableEntity("Project doesn't exist!")
    } else if (currProject.projectManagerId !== userId) {
      throw new HttpErrors.MethodNotAllowed(
        "Canno't delete project which you are not a manager of!"
      )
    }

    await this.projectRepository.deleteById(id)
  }
}
