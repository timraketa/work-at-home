import { authenticate } from '@loopback/authentication'
import { repository } from '@loopback/repository'
import { get, getModelSchemaRef } from '@loopback/rest'
import { PermissionKeys } from '../authorization/permission-keys'
import { Role } from '../models'
import { RoleRepository } from '../repositories'

export class RoleController {
  constructor(
    @repository(RoleRepository)
    public roleRepository: RoleRepository
  ) {}

  @get('/roles', {
    responses: {
      '200': {
        description: 'Array of Role model instances',
        content: {
          'application/json': {
            schema: {
              type: 'array',
              items: getModelSchemaRef(Role, { includeRelations: true })
            }
          }
        }
      }
    }
  })
  @authenticate({
    strategy: 'jwt',
    options: {
      required: [PermissionKeys.CreateUser]
    }
  })
  async find(): Promise<Role[]> {
    return this.roleRepository.find({
      where: {
        id: {
          neq: 11
        }
      }
    })
  }
}
