import { authenticate } from '@loopback/authentication'
import {
  Count,
  CountSchema,
  Filter,
  repository,
  Where
} from '@loopback/repository'
import {
  del,
  get,
  getModelSchemaRef,
  getWhereSchemaFor,
  param,
  patch,
  post,
  requestBody
} from '@loopback/rest'
import { PermissionKeys } from '../authorization/permission-keys'
import { IdsDTO } from '../dto/IdsDTO'
import { Project, Team } from '../models'
import { ProjectRepository } from '../repositories'

export class ProjectTeamController {
  constructor(
    @repository(ProjectRepository)
    protected projectRepository: ProjectRepository
  ) {}

  @post('/projects/{id}/teams', {
    responses: {
      '200': {
        description: 'Project model instance',
        content: { 'application/json': { schema: getModelSchemaRef(Team) } }
      }
    }
  })
  @authenticate({
    strategy: 'jwt',
    options: {
      required: [PermissionKeys.CreateTeam]
    }
  })
  async create(
    @param.path.string('id') id: typeof Project.prototype.id,
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(Team, {
            title: 'NewTeamInProject',
            exclude: ['id'],
            optional: ['projectId']
          })
        }
      }
    })
    team: Omit<Team, 'id'>
  ): Promise<Team> {
    return this.projectRepository.teams(id).create(team)
  }

  @patch('/projects/{id}/teams', {
    responses: {
      '200': {
        description: 'Project.Team PATCH success count',
        content: { 'application/json': { schema: CountSchema } }
      }
    }
  })
  @authenticate({
    strategy: 'jwt',
    options: {
      required: [PermissionKeys.UpdateTeam]
    }
  })
  async patch(
    @param.path.string('id') id: string,
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(Team, {
            exclude: ['id'],
            partial: true
          })
        }
      }
    })
    team: Partial<Team>,
    @param.query.object('where', getWhereSchemaFor(Team)) where?: Where<Team>
  ): Promise<Count> {
    return this.projectRepository.teams(id).patch(team, where)
  }

  @del('/projects/{id}/teams', {
    responses: {
      '200': {
        description: 'Project.Team DELETE success count',
        content: { 'application/json': { schema: CountSchema } }
      }
    }
  })
  @authenticate({
    strategy: 'jwt',
    options: {
      required: [PermissionKeys.DeleteTeam]
    }
  })
  async delete(
    @param.path.string('id') id: string,
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(IdsDTO)
        }
      }
    })
    teamIds: IdsDTO
  ): Promise<Count> {
    return this.projectRepository.teams(id).delete({ id: { inq: teamIds.ids } })
  }
}
