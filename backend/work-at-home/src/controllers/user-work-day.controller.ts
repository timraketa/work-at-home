import { authenticate, AuthenticationBindings } from '@loopback/authentication'
import { Getter, inject } from '@loopback/core'
import {
  Count,
  CountSchema,
  Filter,
  repository,
  Where
} from '@loopback/repository'
import {
  del,
  get,
  getModelSchemaRef,
  getWhereSchemaFor,
  param,
  patch,
  post,
  requestBody
} from '@loopback/rest'
import { MyUserProfile } from '../auth-types'
import { PermissionKeys } from '../authorization/permission-keys'
import { IdsDTO } from '../dto/IdsDTO'
import { WorkDay } from '../models'
import { UserRepository } from '../repositories'

export class UserWorkDayController {
  constructor(
    @repository(UserRepository) protected userRepository: UserRepository,
    @inject.getter(AuthenticationBindings.CURRENT_USER)
    protected getCurrentUser: Getter<MyUserProfile>
  ) {}

  @get('/users/work-days', {
    responses: {
      '200': {
        description: 'Array of User has many WorkDay',
        content: {
          'application/json': {
            schema: { type: 'array', items: getModelSchemaRef(WorkDay) }
          }
        }
      }
    }
  })
  @authenticate({
    strategy: 'jwt',
    options: {
      required: [PermissionKeys.ListWorkDays]
    }
  })
  async find(
    @param.query.object('filter') filter?: Filter<WorkDay>
  ): Promise<WorkDay[]> {
    const id = (await this.getCurrentUser()).id

    return this.userRepository.workDays(id).find(filter)
  }

  @post('/users/work-days', {
    responses: {
      '200': {
        description: 'User model instance',
        content: { 'application/json': { schema: getModelSchemaRef(WorkDay) } }
      }
    }
  })
  @authenticate({
    strategy: 'jwt',
    options: {
      required: [PermissionKeys.UpdateWorkDays]
    }
  })
  async create(
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(WorkDay, {
            title: 'NewWorkDayInUser',
            exclude: ['id'],
            optional: ['userId']
          })
        }
      }
    })
    workDay: Omit<WorkDay, 'id'>
  ): Promise<WorkDay> {
    const id = (await this.getCurrentUser()).id

    return this.userRepository.workDays(id).create(workDay)
  }

  @patch('/users/work-days', {
    responses: {
      '200': {
        description: 'User.WorkDay PATCH success count',
        content: { 'application/json': { schema: CountSchema } }
      }
    }
  })
  @authenticate({
    strategy: 'jwt',
    options: {
      required: [PermissionKeys.UpdateWorkDays]
    }
  })
  async patch(
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(WorkDay, {
            exclude: ['id'],
            partial: true
          })
        }
      }
    })
    workDay: Partial<WorkDay>,
    @param.query.object('where', getWhereSchemaFor(WorkDay))
    where?: Where<WorkDay>
  ): Promise<Count> {
    const id = (await this.getCurrentUser()).id

    return this.userRepository.workDays(id).patch(workDay, where)
  }

  @del('/users/work-days', {
    responses: {
      '200': {
        description: 'User.WorkDay DELETE success count',
        content: { 'application/json': { schema: CountSchema } }
      }
    }
  })
  @authenticate({
    strategy: 'jwt',
    options: {
      required: [PermissionKeys.UpdateWorkDays]
    }
  })
  async delete(
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(IdsDTO)
        }
      }
    })
    workDaysIds: IdsDTO
  ): Promise<Count> {
    const id = (await this.getCurrentUser()).id

    return this.userRepository
      .workDays(id)
      .delete({ id: { inq: workDaysIds.ids } })
  }
}
