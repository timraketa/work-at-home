import { authenticate } from '@loopback/authentication'
import {
  Count,
  CountSchema,
  Filter,
  repository,
  Where
} from '@loopback/repository'
import {
  del,
  get,
  getModelSchemaRef,
  getWhereSchemaFor,
  HttpErrors,
  param,
  patch,
  post,
  requestBody
} from '@loopback/rest'
import { PermissionKeys } from '../authorization/permission-keys'
import { IdsDTO } from '../dto/IdsDTO'
import { WorkDay, WorkHour } from '../models'
import { WorkDayRepository } from '../repositories'

export class WorkDayWorkHourController {
  constructor(
    @repository(WorkDayRepository)
    protected workDayRepository: WorkDayRepository
  ) {}

  @post('/work-days/{id}/work-hours', {
    responses: {
      '200': {
        description: 'WorkDay model instance',
        content: { 'application/json': { schema: getModelSchemaRef(WorkHour) } }
      }
    }
  })
  @authenticate({
    strategy: 'jwt',
    options: {
      required: [PermissionKeys.UpdateWorkDays]
    }
  })
  async create(
    @param.path.string('id') id: typeof WorkDay.prototype.id,
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(WorkHour, {
            title: 'NewWorkHourInWorkDay',
            exclude: ['id'],
            optional: ['workDayId']
          })
        }
      }
    })
    workHour: Omit<WorkHour, 'id'>
  ): Promise<WorkHour> {
    try {
      return await this.workDayRepository.workHours(id).create(workHour)
    } catch (e) {
      if (e.code == '23503')
        throw new HttpErrors.BadRequest('Invalid work day id.')
      else throw e
    }
  }

  @patch('/work-days/{id}/work-hours', {
    responses: {
      '200': {
        description: 'WorkDay.WorkHour PATCH success count',
        content: { 'application/json': { schema: CountSchema } }
      }
    }
  })
  @authenticate({
    strategy: 'jwt',
    options: {
      required: [PermissionKeys.UpdateWorkDays]
    }
  })
  async patch(
    @param.path.string('id') id: string,
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(WorkHour, {
            exclude: ['id'],
            partial: true
          })
        }
      }
    })
    workHour: Partial<WorkHour>,
    @param.query.object('where', getWhereSchemaFor(WorkHour))
    where?: Where<WorkHour>
  ): Promise<Count> {
    return this.workDayRepository.workHours(id).patch(workHour, where)
  }

  @del('/work-days/{id}/work-hours', {
    responses: {
      '200': {
        description: 'WorkDay.WorkHour DELETE success count',
        content: { 'application/json': { schema: CountSchema } }
      }
    }
  })
  @authenticate({
    strategy: 'jwt',
    options: {
      required: [PermissionKeys.UpdateWorkDays]
    }
  })
  async delete(
    @param.path.string('id') id: string,
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(IdsDTO)
        }
      }
    })
    workHourIds: IdsDTO
  ): Promise<Count> {
    return this.workDayRepository
      .workHours(id)
      .delete({ id: { inq: workHourIds.ids } })
  }
}
