import { BootMixin } from '@loopback/boot'
import { ApplicationConfig, createBindingFromClass } from '@loopback/core'
import {
  RestExplorerBindings,
  RestExplorerComponent
} from '@loopback/rest-explorer'
import { RepositoryMixin } from '@loopback/repository'
import { RestApplication } from '@loopback/rest'
import { ServiceMixin } from '@loopback/service-proxy'
import path from 'path'
import { MySequence } from './sequence'
import { AuthenticationComponent } from '@loopback/authentication/dist/authentication.component'
import { JWTStrategy } from './authentication-stratgies/jwt-stratgies'
import {
  PasswordHasherBindings,
  TokenServiceBindings,
  TokenServiceConstants,
  UserServiceBindings
} from './keys'
import { BcryptHasher } from './services/hash.password'
import { MyUserService } from './services/user-service'
import { JWTService } from './services/jwt-service'
import { registerAuthenticationStrategy } from '@loopback/authentication'
import { EmailServiceBindings } from './keys'
import { EmailService } from './services/email-service'
import { SecuritySpecEnhancer } from '@loopback/authentication-jwt'

export { ApplicationConfig }

export class WorkAtHomeApplication extends BootMixin(
  ServiceMixin(RepositoryMixin(RestApplication))
) {
  constructor(options: ApplicationConfig = {}) {
    super(options)

    // setup binding
    this.setupBinding()
    this.add(createBindingFromClass(SecuritySpecEnhancer))

    this.component(AuthenticationComponent)
    // @ts-ignore
    registerAuthenticationStrategy(this, JWTStrategy)

    this.static(
      '/',
      path.join(__dirname, '../../../frontend/work-at-home/build/')
    )

    // Set up the custom sequence
    this.sequence(MySequence)

    // Customize @loopback/rest-explorer configuration here
    this.configure(RestExplorerBindings.COMPONENT).to({
      path: '/explorer'
    })
    this.component(RestExplorerComponent)

    this.projectRoot = __dirname
    // Customize @loopback/boot Booter Conventions here
    this.bootOptions = {
      controllers: {
        // Customize ControllerBooter Conventions here
        dirs: ['controllers'],
        extensions: ['.controller.js'],
        nested: true
      }
    }
  }

  setupBinding(): void {
    this.bind(PasswordHasherBindings.PASSWORD_HASHER).toClass(BcryptHasher)
    this.bind(PasswordHasherBindings.ROUNDS).to(10)
    this.bind(UserServiceBindings.USER_SERVICE).toClass(MyUserService)
    this.bind(TokenServiceBindings.TOKEN_SERVICE).toClass(JWTService)
    this.bind(TokenServiceBindings.TOKEN_SECRET).to(
      TokenServiceConstants.TOKEN_SECRET_VALUE
    )
    this.bind(TokenServiceBindings.TOKEN_EXPIRES_IN).to(
      TokenServiceConstants.TOKEN_EXPIRES_IN_VALUE
    )
    this.bind(EmailServiceBindings.SEND_MAIL).toClass(EmailService)
  }
}
