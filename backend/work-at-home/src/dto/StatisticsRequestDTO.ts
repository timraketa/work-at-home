import { model, property } from '@loopback/repository'

@model()
export class StatisticsRequestDTO {
  @property({
    type: 'date',
    required: true
  })
  from: Date

  @property({
    type: 'date',
    required: true
  })
  to: Date

  @property({
    type: 'string'
  })
  projectId: string
}
