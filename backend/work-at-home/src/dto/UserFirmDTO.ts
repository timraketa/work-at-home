import { model, property } from '@loopback/repository'

@model()
export class UserFirmDTO {
  @property({
    type: 'string',
    required: true
  })
  firmId: string

  @property({
    type: 'string',
    required: true
  })
  firmName: string

  @property({
    type: 'string',
    required: true
  })
  firmEmail: string

  @property.array(String)
  roles: string[]
}
