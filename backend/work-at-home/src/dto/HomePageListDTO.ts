import { model, property } from '@loopback/repository'

@model()
export class HomePageListDTO {
  @property({
    type: 'string',
    required: true
  })
  username: string

  @property({
    type: 'string',
    required: true
  })
  firmName: string

  @property.array(String)
  roles: string[]
}
