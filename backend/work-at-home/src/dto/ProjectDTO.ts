import { model, property } from '@loopback/repository'

@model()
export class ProjectDTO {
  @property({
    type: 'string',
    required: true
  })
  projectName: string

  @property({
    type: 'number',
    required: true
  })
  hours: number

  @property({
    type: 'number',
    required: true
  })
  price: number
}
