import { model, property } from '@loopback/repository'

@model()
export class GetProjectsDTO {
  @property({
    type: 'string',
    required: true
  })
  id: string

  @property({
    type: 'string',
    required: true
  })
  name: string

  @property({
    type: 'string',
    required: true
  })
  firmName: string

  @property({
    type: 'string',
    required: true
  })
  projectManager: string

  @property({
    type: 'number',
    required: true
  })
  minDailyHours: number

  @property({
    type: 'number',
    required: true
  })
  maxWeeklyHours: number

  @property({
    type: 'number',
    required: true
  })
  currDailyHours: number

  @property({
    type: 'number',
    required: true
  })
  currWeeklyHours: number
}
