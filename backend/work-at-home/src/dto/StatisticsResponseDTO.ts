import { model, property } from '@loopback/repository'
import { DayDTO } from './DayDTO'

@model()
export class StatisticsResponseDTO {
  @property({
    type: 'date',
    required: true
  })
  from: Date

  @property({
    type: 'date',
    required: true
  })
  to: Date

  @property.array(DayDTO)
  days: DayDTO[]
}
