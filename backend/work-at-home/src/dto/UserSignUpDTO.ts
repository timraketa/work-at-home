import { model, property } from '@loopback/repository'

@model()
export class UserSignUpDTO {
  @property({
    type: 'string',
    required: true
  })
  password: string

  @property({
    type: 'string',
    required: true
  })
  firstName: string

  @property({
    type: 'string',
    required: true
  })
  lastName: string

  @property({
    type: 'string',
    required: true
  })
  phoneNumber: string

  @property({
    type: 'number',
    required: true
  })
  hourlyRate: number
}
