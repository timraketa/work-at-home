import { model, property } from '@loopback/repository'
import { ProjectDTO } from './ProjectDTO'

@model()
export class DayDTO {
  @property.array(ProjectDTO)
  projects: ProjectDTO[]

  @property({
    type: 'number',
    required: true
  })
  dayHours: number

  @property({
    type: 'number',
    required: true
  })
  dayPrice: number
}
