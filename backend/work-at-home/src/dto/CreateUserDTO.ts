import { model, property } from '@loopback/repository'

@model()
export class CreateUserDTO {
  @property({
    type: 'string',
    required: true
  })
  email: string

  @property({
    type: 'array',
    itemType: 'number',
    required: true
  })
  roles: number[]

  @property({
    type: 'string',
    required: true
  })
  username: string

  @property({
    type: 'string',
    required: true
  })
  firmId: string
}
