import { model, property } from '@loopback/repository'

@model()
export class StatisticsProjectResponseDTO {
  @property({
    type: 'date',
    required: true
  })
  date: Date

  @property({
    type: 'number',
    required: true
  })
  workHours: number

  @property({
    type: 'number',
    required: true
  })
  spending: number
}
