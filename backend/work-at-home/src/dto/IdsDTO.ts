import { model, property } from '@loopback/repository'

@model()
export class IdsDTO {
  @property({
    type: 'array',
    itemType: 'string',
    required: true
  })
  ids: string[]
}
