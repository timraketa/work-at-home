import "chromedriver"
import { Builder, By, Capabilities, until } from 'selenium-webdriver'
import * as dotenv from 'dotenv'
import * as assert from "assert"

dotenv.config()

const signupurl = process.env.REACT_APP_URL + '/signup/test'
const teamurl = process.env.REACT_APP_URL + '/addteam'
const projecturl = process.env.REACT_APP_URL + '/addproject'
const workweekurl = process.env.REACT_APP_URL + '/calendar'

const getElementXpath = async (driver, xpath, timeout = 3000) => {
  const el = await driver.wait(until.elementLocated(By.xpath(xpath)), timeout)
  return await driver.wait(until.elementIsVisible(el), timeout)
}

const getElementName = async (driver, name, timeout = 3000) => {
  const el = await driver.wait(until.elementLocated(By.name(name)), timeout)
  return await driver.wait(until.elementIsVisible(el), timeout)
}

const getElementId = async (driver, id, timeout = 3000) => {
  const el = await driver.wait(until.elementLocated(By.id(id)), timeout)
  return await driver.wait(until.elementIsVisible(el), timeout)
}

let chromeCapabilities = Capabilities.chrome()
//let chromeOptions = { 'args': ['--disable-infobars'] };
//chromeCapabilities.set('chromeOptions', chromeOptions);
//let chromeCapabilities = { browserName: 'chrome' };

describe('Testing the app', () => {
  let browser = new Builder().withCapabilities(chromeCapabilities).build()

  afterAll(async () => {
    await browser.quit()
  }, 1500)
  describe('Testing adding team page', () => {
    it('Adding users', async () => {
      await (await browser).get(teamurl)

      const teamname = await getElementId(browser, 'control-ref_teamname')
      await teamname.sendKeys('TimRaketa')
      const description = await getElementXpath(
        browser,
        '//*[@id="control-ref"]/div[2]/div[2]/div/div/div/textarea'
      )
      await description.sendKeys('Ovo je tim.')
      const addmanager = await getElementXpath(
        browser,
        '//*[@id="control-ref"]/div[4]/div/div/div/div/div[1]/button/span'
      )
      await addmanager.click()
      const addmanager1 = await getElementXpath(
        browser,
        '/html/body/div[3]/div/div[2]/div/div[2]/div[2]/div/div/div[2]/div/div/div/div/div/div/table/tbody/tr[1]/td[1]/label/span/input'
      )
      await addmanager1.click()
      const donebutton = await getElementXpath(
        browser,
        '/html/body/div[3]/div/div[2]/div/div[2]/div[3]/button/span'
      )
      await donebutton.click()
      const done = await getElementXpath(
        browser,
        '//*[@id="control-ref"]/div[4]/div/div/div/div/div[2]/a/button/span'
      )
      await done.click()
      browser.wait(until.urlContains('projectinfo'))
      assert.strictEqual(process.env.REACT_APP_URL +'/projectinfo“', (await browser.getCurrentUrl()))
    })
  })

  describe('Testing signup page', () => {
    let db

    beforeAll(async () => {
      const promise = require('bluebird')
      const initOptions = {
        promiseLib: promise
      }
      const pgp = require('pg-promise')(initOptions)

      const cn = {
        port: 55432,
        host: 'localhost',
        database: 'work-at-home',
        user: 'user',
        password: 'pass',
        max: 30
      }

      db = pgp(cn)
      await db.none('DELETE FROM "user" WHERE username = \'test\'')
      await db.oneOrNone('insert into "user"(username) values($1)', 'test')
    })

    afterAll(async () => {
      await db.none('DELETE FROM "user" WHERE username = \'test\'')
    }, 15000)

    it('Edge case: len(password)<4', async () => {
      await browser.get(signupurl)

      const password = await getElementId(browser, 'control-ref_password')
      await password.sendKeys('123')
      const button = await getElementXpath(
        browser,
        '//*[@id="control-ref"]/div[7]/div/div/div/button'
      )
      await button.click()
      browser.wait(until.urlIs(signupurl))
      assert.strictEqual(signupurl, (await browser.getCurrentUrl()))
      
    })

    it('Sign up', async () => {
      await browser.get(signupurl)

      const password = await getElementId(browser, 'control-ref_password')
      await password.sendKeys('1234')
      const firstname = await getElementId(browser, 'control-ref_firstName')
      await firstname.sendKeys('Ivan')
      const lastname = await getElementId(browser, 'control-ref_lastName')
      await lastname.sendKeys('Horvat')
      const phonenumber = await getElementId(browser, 'control-ref_phoneNumber')
      await phonenumber.sendKeys('09xxxxxxxx')
      const hourlyrate = await getElementId(browser, 'control-ref_hourlyRate')
      await hourlyrate.sendKeys('50')
      const button = await getElementXpath(
        browser,
        '//*[@id="control-ref"]/div[7]/div/div/div/button'
      )
      await button.click()
      browser.wait(until.urlIs(process.env.REACT_APP_URL))
      assert.strictEqual(process.env.REACT_APP_URL, (await browser.getCurrentUrl()))
    })
  })

  describe('Testing adding project page', () => {
    it('Adding users', async () => {
      await browser.get(projecturl)

      const projectname = await getElementId(browser, 'control-ref_projectname')
      await projectname.sendKeys('Project3')
      const selectfirm = await getElementXpath(browser, '//*[@id="control-ref"]/div[2]/div[2]/div/div/div/div')
      await selectfirm.click()
      const selectedfirm = await getElementXpath(browser, '/html/body/div[3]/div/div/div/div[2]/div[1]/div/div/div[1]/div')
      await selectedfirm.click()
      const duedate = await getElementXpath(
        browser,
        '//*[@id="control-ref"]/div[3]/div[2]/div/div/div/div/input'
      )
      await duedate.sendKeys('2020-01-01')
      const minhrsday = await getElementXpath(
        browser,
        '//*[@id="control-ref"]/div[4]/div[2]/div/div/div/div[2]/input'
      )
      await minhrsday.sendKeys('5')
      const maxhrsweek = await getElementXpath(
        browser,
        '//*[@id="control-ref"]/div[5]/div[2]/div/div/div/div[2]/input'
      )
      await maxhrsweek.sendKeys('20')
      const budget = await getElementXpath(
        browser,
        '//*[@id="control-ref"]/div[6]/div[2]/div/div/div/div[2]/input'
      )
      await budget.sendKeys('200')
      const description = await getElementXpath(
        browser,
        '//*[@id="control-ref"]/div[7]/div[2]/div/div/input'
      )
      await description.sendKeys('Ovo je projekt.')
      const donebutton = await getElementXpath(
        browser,
        '//*[@id="control-ref"]/div[9]/div/div/div/div/div/button'
      )
      await donebutton.click()
      browser.wait(until.urlContains('myprojects'))
      assert.strictEqual(process.env.REACT_APP_URL+'/myprojects', (await browser.getCurrentUrl()))
    })
  })
  describe('Testing adding work day', () => {
    it('Adding hours', async () => {
      await browser.get(workweekurl)

      const add1 = await getElementXpath(
        browser,
        '//*[@id="root"]/section/main/div/div/div/div/div/div[3]/div/div/div[2]/div/div/table/tbody/tr[2]/td[3]/div/div[2]'
      )
      await add1.click()
      const description = await getElementXpath(browser, '/html/body/div[2]/div/div[2]/div/div[2]/div[2]/div/div/div/li[1]/input')
      await description.sendKeys('Opis')
      const selectproject = await getElementXpath(
        browser,
        '/html/body/div[2]/div/div[2]/div/div[2]/div[2]/div/div/div/li[2]/div/div/span[2]'
      )
      await selectproject.click()
      const selectproject1 = await getElementXpath(
        browser,
        '/html/body/div[3]/div/div/div/div[2]/div[1]/div/div/div[1]/div'
      )
      await selectproject1.click()
      const add = await getElementXpath(
        browser,
        '/html/body/div[2]/div/div[2]/div/div[2]/div[2]/div/div/div/li[3]/div/div/button/span'
      )
      await add.click()
      const done = await getElementXpath(
        browser,
        '/html/body/div[2]/div/div[2]/div/div[2]/div[3]/button[2]/span'
      )
      await done.click()
      const del = await getElementXpath(
        browser,
        '//*[@id="root"]/section/main/div/div/div/div/div/div[3]/div/div/div[2]/div/div/table/tbody/tr[2]/td[4]/div/div[2]/ul/li/div/div[2]/span/svg'
      )
      await del.click()

      browser.wait(until.urlContains('calendar'))
      assert.strictEqual(process.env.REACT_APP_URL+'/calendar', (await browser.getCurrentUrl()))
    })
  })
})
