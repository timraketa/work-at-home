import { Role } from '../models/Role'
import { CreateUser, LoginUser, SignupUser, User } from '../models/User'
import { ChecklistItem } from '../models/ChecklistItem'
import { Checklist } from '../models/Checklist'
import { WorkHour } from '../models/WorkHour'
import { WorkDay } from '../models/WorkDay'
import {
  UserStatsRequest,
  ProjectStatsRequest,
  ProjectWorkHoursStats,
  ProjectSpendingDayStats,
  UserWorkHoursStats
} from '../models/Statistics'
import { Firm } from '../models/Firm'
import {
  ProjectById,
  Project,
  CreateProject,
  GetProjectsType,
  UpdateProject
} from '../models/Project'
import { Team, CreateTeam } from '../models/Team'
import { Checkpoint } from '../models/Checkpoint'

export type LoginReturnType = {
  token: string
}

export type SignupReturnType = User

export type CreateUserReturnType = {
  token: string
}
export type MembersId = {
  ids: string[]
}
export type CheckpointReturn = {
  id: string
  date: string
  projectId: string
  description: string
  name: string
}
export type CheckpointPostType = {
  date: string
  projectId: string
  description: string
  name: string
}
export type ChecklistPostType = {
  checkpointId: string
  name: string
}
export type ChecklistItemPostType = {
  checklistId: string
  description: string
  isChecked: boolean
}

export type ChecklistItemIds = {
  ids: string[]
}

export type UserFirm = {
  firmId: string
  firmName: string
  firmEmail: string
  roles: string[]
}

export type HomePageUser = {
  username: string
  firmName: string
  roles: string[]
}

export type AdminFirm = {
  id: string
  name: string
}

export type WorkDayPostType = {
  isFree: boolean
  date: string
  userId: string
}
export type WorkHourPostType = {
  description: string
  workDayId: string
  projectId: string
}
export type GetRolesReturnType = Role[]
export type ChecklistReturnType = Checklist
export type ChecklistsReturnType = Checklist[]
export type ChecklistItemReturnType = ChecklistItem
export type ChecklistItemsReturnType = ChecklistItem[]
export type WorkHourReturnType = WorkHour
export type WorkDayReturnType = WorkDay
export type WorkDaysReturnType = WorkDay[]
export type ProjectReturnType = ProjectById
export type ProjectsReturnType = GetProjectsType[]
export type TeamReturnType = Team
export type TeamsReturnType = Team[]
export type ProjectWorkHoursStatsReturnType = ProjectWorkHoursStats[]
export type UserWorkHoursStatsReturnType = UserWorkHoursStats
export type UndividedUserWorkHoursStatsReturnType = UserWorkHoursStats
export type ProjectSpendingStatsReturnType = ProjectSpendingDayStats[]
export type CheckpointsReturnType = Checkpoint[]
export type CheckpointReturnType = CheckpointReturn
export type FirmReturnType = Firm[]
export type Count = { count: number }
export type userIds = { ids: string[] }
export type CheckpointIds = { ids: string[] }
export type ChecklistIds = { ids: string[] }
export type ChecklistItemId = { ids: string[] }
export type WorkDayId = { ids: string[] }
export type WorkHourId = { ids: string[] }

export interface ApiClient {
  createUser(createUser: CreateUser): Promise<CreateUserReturnType>
  login(loginUser: LoginUser): Promise<LoginReturnType>
  signup(username: string, signupUser: SignupUser): Promise<SignupReturnType>
  getRoles(): Promise<GetRolesReturnType>
  getUsers(firmId: string): Promise<User[]>
  getCheckpoint(projectId: string): Promise<CheckpointsReturnType>
  addCheckpoint(
    projectId: string,
    checkpoint: CheckpointPostType
  ): Promise<CheckpointReturnType>
  deleteCheckpoint(
    checkpointIds: CheckpointIds,
    projectId: string
  ): Promise<Count>
  addChecklist(
    checkpointId: string,
    checklist: ChecklistPostType
  ): Promise<ChecklistReturnType>
  deleteChecklist(idCheckpoint: string, ids: ChecklistIds): Promise<Count>
  addChecklistItem(
    checklistId: string,
    checklistItem: ChecklistItemPostType
  ): Promise<ChecklistItemReturnType>
  deleteChecklistItem(
    ChecklistItemId: string,
    ids: ChecklistItemIds
  ): Promise<Count>
  editChecklistItem(
    checklistId: string,
    checklistItem: ChecklistItemPostType,
    checklistItemId: string
  ): Promise<Count>
  addWorkDay(workDay: WorkDayPostType): Promise<WorkDayReturnType>
  deleteWorkDay(workDayId: WorkDayId): Promise<Count>
  getWorkDays(): Promise<WorkDaysReturnType>
  addWorkHour(
    workDayId: string,
    workHour: WorkHourPostType
  ): Promise<WorkHourReturnType>
  addProject(project: Project): Promise<ProjectReturnType>
  addTeam(teamId: string, team: Team): Promise<TeamReturnType>
  getProjectSpendingStats(
    projectStatsRequest: ProjectStatsRequest
  ): Promise<ProjectSpendingStatsReturnType>
  getUserWorkHoursStats(
    userStatsRequest: UserStatsRequest
  ): Promise<UserWorkHoursStatsReturnType>
  getUndividedUserWorkHoursStats(
    userStatsRequest: UserStatsRequest
  ): Promise<UndividedUserWorkHoursStatsReturnType>
  getProjectWorkHoursStats(
    projectStatsRequest: ProjectStatsRequest
  ): Promise<ProjectWorkHoursStatsReturnType>
  deleteWorkHour(workDayId: string, workHourId: WorkDayId): Promise<Count>
  addProject(project: CreateProject): Promise<ProjectReturnType>
  getProjects(): Promise<ProjectsReturnType>
  getProject(id: string): Promise<ProjectReturnType>
  addTeam(projectId: string, team: CreateTeam): Promise<TeamReturnType>
  addTeamMembers(teamId: string, membersId: MembersId): Promise<User[]>
  deleteTeamMembers(teamId: string, usersIds: userIds): Promise<Count>
  getUser(): Promise<User>
  getUserFirms(): Promise<UserFirm[]>
  getAdminFirms(): Promise<AdminFirm[]>
  getHomePageList(): Promise<HomePageUser[]>
  updateUser(user: User): Promise<void>
  updateProject(project: UpdateProject, projectId: string): Promise<void>
}
