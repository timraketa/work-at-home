import { CreateUser, LoginUser, SignupUser, User } from '../models/User'
import { WorkDay } from '../models/WorkDay'
import { CreateProject, UpdateProject } from '../models/Project'
import { Team } from '../models/Team'
import { ProjectStatsRequest, UserStatsRequest } from '../models/Statistics'
import {
  ApiClient,
  CreateUserReturnType,
  GetRolesReturnType,
  LoginReturnType,
  SignupReturnType,
  ChecklistReturnType,
  ChecklistItemReturnType,
  WorkHourReturnType,
  WorkDayReturnType,
  WorkDaysReturnType,
  ProjectReturnType,
  ProjectsReturnType,
  TeamReturnType,
  ProjectSpendingStatsReturnType,
  UserWorkHoursStatsReturnType,
  ProjectWorkHoursStatsReturnType,
  UndividedUserWorkHoursStatsReturnType,
  MembersId,
  CheckpointReturnType,
  Count,
  userIds,
  CheckpointsReturnType,
  CheckpointPostType,
  CheckpointIds,
  ChecklistPostType,
  ChecklistIds,
  ChecklistItemPostType,
  ChecklistItemIds,
  UserFirm,
  WorkHourPostType,
  WorkDayId,
  AdminFirm,
  HomePageUser
} from './ApiClient'
import axios from 'axios'

export class AxiosApiClient implements ApiClient {
  async login(loginUser: LoginUser): Promise<LoginReturnType> {
    return (
      await axios.post(
        `${process.env.REACT_APP_API_URL}/users/login`,
        loginUser
      )
    ).data
  }

  async signup(
    username: string,
    signupUser: SignupUser
  ): Promise<SignupReturnType> {
    return (
      await axios.post(
        `${process.env.REACT_APP_API_URL}/users/signup/${username}`,
        signupUser
      )
    ).data
  }

  async createUser(createUser: CreateUser): Promise<CreateUserReturnType> {
    return (
      await axios.post(`${process.env.REACT_APP_API_URL}/users`, createUser)
    ).data
  }

  async getRoles(): Promise<GetRolesReturnType> {
    return (await axios.get(`${process.env.REACT_APP_API_URL}/roles`)).data
  }

  async getUsers(firmId: string): Promise<User[]> {
    return (
      await axios.get(`${process.env.REACT_APP_API_URL}/users/firmId=${firmId}`)
    ).data
  }

  async getUserFirms(): Promise<UserFirm[]> {
    return (await axios.get(`${process.env.REACT_APP_API_URL}/users/firms`))
      .data
  }

  async getAdminFirms(): Promise<AdminFirm[]> {
    return (
      await axios.get(`${process.env.REACT_APP_API_URL}/users/firms/admin`)
    ).data
  }

  async getCheckpoint(projectId: string): Promise<CheckpointsReturnType> {
    return (
      await axios.get(
        `${process.env.REACT_APP_API_URL}/projects/${projectId}/checkpoints`
      )
    ).data
  }

  async addCheckpoint(
    projectId: string,
    checkpoint: CheckpointPostType
  ): Promise<CheckpointReturnType> {
    return (
      await axios.post(
        `${process.env.REACT_APP_API_URL}/projects/${projectId}/checkpoints`,
        checkpoint
      )
    ).data
  }

  async deleteCheckpoint(
    checkpointIds: CheckpointIds,
    projectId: string
  ): Promise<Count> {
    return (
      await axios.request({
        method: 'delete',
        url: `${process.env.REACT_APP_API_URL}/projects/${projectId}/checkpoints`,
        data: checkpointIds
      })
    ).data
  }

  async addChecklist(
    checkpointId: string,
    checklist: ChecklistPostType
  ): Promise<ChecklistReturnType> {
    return (
      await axios.post(
        `${process.env.REACT_APP_API_URL}/checkpoints/${checkpointId}/checklists`,
        checklist
      )
    ).data
  }

  async deleteChecklist(
    idCheckpoint: string,
    ids: ChecklistIds
  ): Promise<Count> {
    return (
      await axios.request({
        method: 'delete',
        url: `${process.env.REACT_APP_API_URL}/checkpoints/${idCheckpoint}/checklists`,
        data: ids
      })
    ).data
  }

  async addChecklistItem(
    checklistId: string,
    checklistItem: ChecklistItemPostType
  ): Promise<ChecklistItemReturnType> {
    return (
      await axios.post(
        `${process.env.REACT_APP_API_URL}/checklists/${checklistId}/checklist-items`,
        checklistItem
      )
    ).data
  }

  async deleteChecklistItem(
    idChecklist: string,
    ids: ChecklistItemIds
  ): Promise<Count> {
    return (
      await axios.request({
        method: 'delete',
        url: `${process.env.REACT_APP_API_URL}/checklists/${idChecklist}/checklist-items`,
        data: ids
      })
    ).data
  }
  async editChecklistItem(
    checklistId: string,
    checklistItem: ChecklistItemPostType,
    checklistItemId: string
  ): Promise<Count> {
    let where = { id: checklistItemId }
    return (
      await axios.request({
        method: 'patch',
        url:
          `${process.env.REACT_APP_API_URL}/checklists/${checklistId}/checklist-items?where=` +
          JSON.stringify(where),
        data: checklistItem
      })
    ).data
  }
  async addWorkHour(
    workDayId: string,
    workHour: WorkHourPostType
  ): Promise<WorkHourReturnType> {
    return (
      await axios.post(
        `${process.env.REACT_APP_API_URL}/work-days/${workDayId}/work-hours`,
        workHour
      )
    ).data
  }
  async addWorkDay(workDay: WorkDay): Promise<WorkDayReturnType> {
    return (
      await axios.post(
        `${process.env.REACT_APP_API_URL}/users/work-days`,
        workDay
      )
    ).data
  }
  async deleteWorkDay(workDayId: WorkDayId): Promise<Count> {
    return (
      await axios.request({
        method: 'delete',
        url: `${process.env.REACT_APP_API_URL}/users/work-days`,
        data: workDayId
      })
    ).data
  }
  async deleteWorkHour(
    workDayId: string,
    workHourId: WorkDayId
  ): Promise<Count> {
    return (
      await axios.request({
        method: 'delete',
        url: `${process.env.REACT_APP_API_URL}/work-days/${workDayId}/work-hours`,
        data: workHourId
      })
    ).data
  }

  async getWorkDays(): Promise<WorkDaysReturnType> {
    let filter = { include: [{ relation: 'workHours' }] }
    return (
      await axios.get(
        `${process.env.REACT_APP_API_URL}/users/work-days?filter=` +
          JSON.stringify(filter)
      )
    ).data
  }
  async addProject(project: CreateProject): Promise<ProjectReturnType> {
    return (
      await axios.post(`${process.env.REACT_APP_API_URL}/projects`, project)
    ).data
  }
  async getProjects(): Promise<ProjectsReturnType> {
    return (await axios.get(`${process.env.REACT_APP_API_URL}/projects`)).data
  }
  async getProject(id: string): Promise<ProjectReturnType> {
    return (await axios.get(`${process.env.REACT_APP_API_URL}/projects/${id}`))
      .data
  }

  async addTeam(projectId: string, team: Team): Promise<TeamReturnType> {
    return (
      await axios.post(
        `${process.env.REACT_APP_API_URL}/projects/${projectId}/teams`,
        team
      )
    ).data
  }
  async getProjectSpendingStats(
    projectStatsRequest: ProjectStatsRequest
  ): Promise<ProjectSpendingStatsReturnType> {
    return (
      await axios.post(
        `${process.env.REACT_APP_API_URL}/statistics/spending/projects`,
        projectStatsRequest
      )
    ).data
  }
  async getUserWorkHoursStats(
    userStatsRequest: UserStatsRequest
  ): Promise<UserWorkHoursStatsReturnType> {
    return (
      await axios.post(
        `${process.env.REACT_APP_API_URL}/statistics/user/projects`,
        userStatsRequest
      )
    ).data
  }
  async getUndividedUserWorkHoursStats(
    userStatsRequest: UserStatsRequest
  ): Promise<UndividedUserWorkHoursStatsReturnType> {
    return (
      await axios.post(
        `${process.env.REACT_APP_API_URL}/statistics/user`,
        userStatsRequest
      )
    ).data
  }
  async getProjectWorkHoursStats(
    projectStatsRequest: ProjectStatsRequest
  ): Promise<ProjectWorkHoursStatsReturnType> {
    return (
      await axios.post(
        `${process.env.REACT_APP_API_URL}/statistics/workHours/projects`,
        projectStatsRequest
      )
    ).data
  }

  async addTeamMembers(teamId: string, membersId: MembersId): Promise<User[]> {
    return (
      await axios.post(
        `${process.env.REACT_APP_API_URL}/teams/${teamId}/users`,
        membersId
      )
    ).data
  }

  async deleteTeamMembers(teamId: string, usersIds: userIds): Promise<Count> {
    return (
      await axios.request({
        method: 'delete',
        url: `${process.env.REACT_APP_API_URL}/teams/${teamId}/users`,
        data: usersIds
      })
    ).data
  }

  async getUser(): Promise<User> {
    const data = (await axios.get(`${process.env.REACT_APP_API_URL}/users/id`))
      .data
    return {
      ...data,
      hourlyRate: parseInt(data.hourlyRate)
    }
  }

  async getHomePageList(): Promise<HomePageUser[]> {
    return (await axios.get(`${process.env.REACT_APP_API_URL}/users/home`)).data
  }

  async updateUser(user: User): Promise<void> {
    const { id, firmRoles, ...data } = { ...user }

    return await axios.patch(`${process.env.REACT_APP_API_URL}/users`, data)
  }

  async updateProject(
    project: UpdateProject,
    projectId: string
  ): Promise<void> {
    return await axios.patch(
      `${process.env.REACT_APP_API_URL}/projects/${projectId}`,
      project
    )
  }
}
