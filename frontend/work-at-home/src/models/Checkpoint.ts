import { Checklist } from './Checklist'

export type Checkpoint = {
  id: string
  date: string
  projectId: string
  description: string
  name: string
  checklists: Checklist[]
}
