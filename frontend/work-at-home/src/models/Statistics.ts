export type UserStatsRequest = {
  from: string
  to: string
}
export type ProjectStatsRequest = {
  from: string
  to: string
  projectId: string
}
export type ProjectDayStats = {
  projectName: string
  hours: number
  price: number
}

export type DayStats = {
  projects: ProjectDayStats[]
  dayHours: number
  dayPrice: number
}

export type UserWorkHoursStats = {
  from: string
  to: string
  days: DayStats[]
}

export type ProjectSpendingDayStats = {
  date: string
  spending: number
}

export type ProjectWorkHoursStats = {
  date: string
  workHours: number
}
