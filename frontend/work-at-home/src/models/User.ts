export type LoginUser = {
  username: string
  password: string
}
export type SignupUser = {
  firstName: string
  lastName: string
  phoneNumber: string
  password: string
  hourlyRate: number
}

export type CreateUser = {
  username: string
  email: string
  roles: number[]
  firmId: string
}

export class User {
  public id: string
  public username: string
  public isRegistered: boolean
  public firstName?: string
  public lastName?: string
  public phoneNumber?: string
  public hourlyRate?: number
  public firmRoles?: string[]

  constructor(user: User) {
    this.id = user.id
    this.firstName = user.firstName
    this.lastName = user.lastName
    this.username = user.username
    this.phoneNumber = user.phoneNumber
    this.hourlyRate = user.hourlyRate
    this.isRegistered = user.isRegistered
  }
}
