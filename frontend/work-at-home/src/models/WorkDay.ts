import { WorkHour } from './WorkHour'

export type WorkDay = {
  id: string
  isFree: boolean
  date: string
  userId: string
  workHours: WorkHour[]
}
