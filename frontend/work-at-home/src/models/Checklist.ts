import { ChecklistItem } from './ChecklistItem'
export type Checklist = {
  id: string
  checkpointId: string
  name: string
  checklistItems: ChecklistItem[]
}
