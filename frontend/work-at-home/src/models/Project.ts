import { WorkHour } from './WorkHour'
import { Checkpoint } from './Checkpoint'
import { Team } from './Team'
import { User } from './User'

export type GetProjectsType = {
  id: string
  name: string
  minDailyHours: number
  maxWeeklyHours: number
  firmName: string
  projectManager: string
  currDailyHours: number
  currWeeklyHours: number
}

export type UpdateProject = {
  minDailyHours: number
  maxWeeklyHours: number
  reminderEnabled: boolean
  description: string
  budget: number
  dueDate: string
  name: string
}

export type Project = {
  id: string
  minDailyHours: number
  maxWeeklyHours: number
  reminderEnabled: boolean
  projectManagerId: string
  name: string
  firmId: string
  description: string
  budget: number
  dueDate: string
  workHours: WorkHour[]
  checkpoints: Checkpoint[]
  teams: Team[]
  firm: {
    name: string
  }
  projectManager: {
    username: string
  }
}

export type ProjectById = {
  id: string
  minDailyHours: number
  maxWeeklyHours: number
  reminderEnabled: boolean
  projectManager: User
  teams?: Team[]
  name: string
  firmId: string
  description: string
  budget: number
  dueDate: string
}

export type CreateProject = {
  minDailyHours: number
  maxWeeklyHours: number
  reminderEnabled: boolean
  name: string
  firmId: string
  description: string
  budget: number
  dueDate: string
}
