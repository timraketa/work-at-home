import { User } from './User'

export type Team = {
  id: string
  teamLeadId: string
  name: string
  description: string
  projectId: string
  users: User[]
}

export type CreateTeam = {
  teamLeadId: string
  name: string
  description: string
  projectId: string
}
