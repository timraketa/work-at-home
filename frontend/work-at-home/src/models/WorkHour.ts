export type WorkHour = {
  id: string
  description: string
  workDayId: string
  projectId: string
}
