import { Role } from './Role'

export type Firm = {
  firmId: string
  firmName: string
  firmEmail: string
  roles: Role[]
}
