export type ChecklistItem = {
  id: string
  checklistId: string
  description: string
  isChecked: boolean
}
