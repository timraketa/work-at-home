import React from 'react'
import ReactDOM from 'react-dom'
import App from './App'
import * as dotenv from 'dotenv'
import { AxiosApiClient } from './api-client/AxiosApiClient'
import axios from 'axios'

dotenv.config()

axios.interceptors.request.use(config => {
  const token = localStorage.getItem('token')

  if (token) {
    config.headers.Authorization = 'Bearer ' + token
  }

  return config
})

axios.interceptors.response.use(
  (response: any) => {
    return response
  },
  (error: any) => {
    if (error && error.response && error.response.status === 401) {
      return window.location.assign('/login')
    } else {
      return Promise.reject(error)
    }
  }
)

const apiClient = new AxiosApiClient()

ReactDOM.render(<App apiClient={apiClient} />, document.getElementById('root'))
