import { notification } from 'antd'
import React, { Component } from 'react'
import { ApiClient } from '../api-client/ApiClient'
import {
  ProjectStatsRequest,
  ProjectWorkHoursStats
} from '../models/Statistics'
import { BarGraph, BarGraphInputData } from './BarGraph'

type State = {
  responseValue: ProjectWorkHoursStats[]
  barGraphInputData: BarGraphInputData
}
type Props = {
  apiClient: ApiClient
  projectStatsRequest: ProjectStatsRequest
}

export class ProjectWorkHoursBarGraph extends Component<Props, State> {
  public state: State = {
    responseValue: [],
    barGraphInputData: { keys: ['Work hours'], bars: [], index: 'date' }
  }

  private date_format = (date: Date): string => {
    return date.getDate() + '.' + (date.getMonth() + 1) + '.'
  }
  private isSameDay = (d1: Date, d2: Date) => {
    return (
      d1.getFullYear() === d2.getFullYear() &&
      d1.getMonth() === d2.getMonth() &&
      d1.getDate() === d2.getDate()
    )
  }
  format_response() {
    let temp_bars: any[] = []

    let startdate: Date = new Date(this.props.projectStatsRequest.from)
    let enddate: Date = new Date(this.props.projectStatsRequest.to)

    /* Day after requested end date for  */
    let enddate_p1: Date = new Date(enddate.getTime())
    enddate_p1.setDate(enddate_p1.getDate() + 1)

    for (
      let c_date: Date = startdate;
      !this.isSameDay(c_date, enddate_p1);
      c_date.setDate(c_date.getDate() + 1)
    ) {
      let hasData: boolean = false
      for (let i = 0; i < this.state.responseValue.length; i++) {
        let date: Date = new Date(this.state.responseValue[i].date)
        if (this.isSameDay(c_date, date)) {
          let workHours: number = this.state.responseValue[i].workHours
          let temp = { date: this.date_format(date), 'Work hours': workHours }
          temp_bars.push(temp)
          hasData = true
        }
      }
      if (!hasData) {
        let temp = { date: this.date_format(c_date), 'Work hours': 0 }
        temp_bars.push(temp)
      }
    }

    this.setState({
      barGraphInputData: {
        bars: temp_bars,
        keys: this.state.barGraphInputData.keys,
        index: this.state.barGraphInputData.index
      }
    })
  }

  private async getWorkHours() {
    try {
      const value = await this.props.apiClient.getProjectWorkHoursStats(
        this.props.projectStatsRequest
      )

      this.setState({ responseValue: value })
      this.format_response()
    } catch (e) {
      notification['error']({
        message: e.message.includes('status code 40')
          ? "You don't have permission to view project statistics."
          : 'Error statistic data data'
      })
    }
  }
  componentDidMount() {
    this.getWorkHours()
  }

  componentDidUpdate(prevProps: Props) {
    if (
      prevProps.projectStatsRequest.from !==
        this.props.projectStatsRequest.from ||
      prevProps.projectStatsRequest.to !== this.props.projectStatsRequest.to
    ) {
      this.getWorkHours()
    }
  }

  render() {
    return <BarGraph data={this.state.barGraphInputData} />
  }
}
