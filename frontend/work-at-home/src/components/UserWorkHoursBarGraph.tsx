import React, { Component } from 'react'
import { ApiClient } from '../api-client/ApiClient'
import { UserStatsRequest } from '../models/Statistics'
import { BarGraph, BarGraphInputData } from './BarGraph'
import { UserWorkHoursStats } from '../models/Statistics'
import { notification } from 'antd'

type State = {
  responseValue: UserWorkHoursStats
  barGraphInputData: BarGraphInputData
}
type Props = {
  apiClient: ApiClient
  userStatsRequest: UserStatsRequest
}

export class UserWorkHoursBarGraph extends Component<Props, State> {
  public state: State = {
    responseValue: { days: [], from: '', to: '' },
    barGraphInputData: { keys: [], bars: [], index: 'date' }
  }
  private date_format = (date: Date): string => {
    return date.getDate() + '.' + (date.getMonth() + 1) + '.'
  }

  format_response() {
    let startDate: Date = new Date(this.state.responseValue.from)
    let days = this.state.responseValue.days
    let current_date: Date = new Date(startDate)
    let temp_keys: string[] = []
    let temp_bars: any[] = []
    for (let i = 0; i < days.length; i++) {
      let temp: any = {}
      temp['date'] = this.date_format(current_date)
      current_date.setDate(current_date.getDate() + 1) //Sets current_date to its relative tomorrow

      for (let j = 0; j < days[i].projects.length; j++) {
        let element = days[i].projects[j]
        temp_keys.push(element.projectName)
        temp[element.projectName] = element.hours
      }

      temp_bars.push(temp)
    }

    temp_keys = Array.from(new Set(temp_keys).values())

    this.setState({
      barGraphInputData: {
        keys: temp_keys,
        bars: temp_bars,
        index: this.state.barGraphInputData.index
      }
    })
  }
  private async getWorkHours() {
    try {
      const value = await this.props.apiClient.getUserWorkHoursStats(
        this.props.userStatsRequest
      )

      this.setState({ responseValue: value })
      this.format_response()
    } catch (e) {
      notification['error']({
        message: "Couldn't fetch data."
      })
    }
  }

  componentDidMount() {
    this.getWorkHours()
  }
  componentDidUpdate(prevProps: Props) {
    if (
      prevProps.userStatsRequest.from !== this.props.userStatsRequest.from ||
      prevProps.userStatsRequest.to !== this.props.userStatsRequest.to
    ) {
      this.getWorkHours()
    }
  }
  render() {
    return <BarGraph data={this.state.barGraphInputData} />
  }
}
