import { Table, Tag } from 'antd'
import { ColumnsType } from 'antd/lib/table'
import React, { ReactText } from 'react'
import { User } from '../models/User'

import '../style/UserList.css'

export type UserListDataType = User & {
  key: string
}

const columns: ColumnsType<UserListDataType> = [
  {
    title: 'Username',
    dataIndex: 'username',
    key: 'username'
  },
  {
    title: 'First Name',
    dataIndex: 'firstName',
    key: Math.random()
  },
  {
    title: 'Last Name',
    dataIndex: 'lastName',
    key: Math.random()
  },
  {
    title: 'Phone number',
    dataIndex: 'phoneNumber',
    key: Math.random()
  },
  {
    title: 'Hourly rate',
    dataIndex: 'hourlyRate',
    key: Math.random()
  },
  {
    title: 'Roles',
    dataIndex: 'firmRoles',
    key: Math.random(),
    align: 'center',
    render: (roles: string[]) => (
      <>
        {roles.map(role => {
          let color = 'red'

          switch (role) {
            case 'Admin':
              color = 'green'
              break
            case 'Worker':
              color = 'geekblue'
              break
            case 'Team Lead':
              color = 'magenta'
              break
            case 'Project Manager':
              color = 'volcano'
              break
          }

          return (
            <Tag color={color} key={role}>
              {role.toUpperCase()}
            </Tag>
          )
        })}
      </>
    )
  },
  {
    title: 'Registered',
    dataIndex: 'isRegistered',
    key: Math.random(),
    render: isRegistered => {
      return (
        <div
          style={{
            background: isRegistered ? 'green' : 'red',
            borderRadius: '50%',
            width: '10px',
            height: '10px'
          }}
        ></div>
      )
    }
  }
]

type Props = {
  data: UserListDataType[]
  selectEnabled: boolean
  onSelectChange?: (selectedRows: ReactText[]) => void
  loading: boolean
  type?: 'radio' | 'checkbox' | undefined
  selected?: string[]
}

type State = {
  selected: string[]
}

export class UserList extends React.Component<Props, State> {
  state = {
    selected: []
  }

  render() {
    return (
      <Table
        pagination={false}
        rowSelection={
          this.props.selectEnabled
            ? {
                selectedRowKeys: this.props.selected?.map((item: any) => {
                  return item
                }),
                onChange: this.props.onSelectChange,

                type: this.props.type
              }
            : undefined
        }
        columns={columns}
        dataSource={this.props.data}
        loading={this.props.loading}
      />
    )
  }
}
