import React from 'react'
import { Modal, Button } from 'antd'

type State = {
  isModalVisible: boolean
}
type Props = {
  data: any
  title: string
  titleButton: string
  selection: string
  callback: (selection: string) => void
  style?: any
}

export class CustomModal extends React.Component<Props, State> {
  state = {
    isModalVisible: false
  }

  showModalHandler = () => {
    this.setState({ isModalVisible: true })
    this.props.callback(this.props.selection)
  }

  handleOkHandler = () => {
    this.setState({ isModalVisible: false })
    this.props.callback(this.props.selection)
  }

  handleCancelHandle = () => {
    this.setState({ isModalVisible: false })
    this.props.callback('')
  }

  render() {
    return (
      <>
        <Button
          type="primary"
          onClick={this.showModalHandler}
          style={this.props.style}
        >
          {this.props.titleButton}
        </Button>

        <Modal
          closable={false}
          maskClosable={false}
          width="70%"
          title={this.props.title}
          visible={this.state.isModalVisible}
          footer={[
            <Button
              key="back"
              onClick={this.handleOkHandler}
              size={'large'}
              type="primary"
            >
              Done
            </Button>
          ]}
        >
          <div style={{ overflow: 'auto', textAlign: 'center' }}>
            {this.props.data}
          </div>
        </Modal>
      </>
    )
  }
}
