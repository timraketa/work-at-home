import React, { Component } from 'react'
import '../style/Frame.css'
export class Frame extends Component {
  render() {
    return (
      <div
        className="coolFrame"
        style={{ textAlign: 'center', marginTop: '15px', marginBottom: '15px' }}
      >
        {this.props.children}
      </div>
    )
  }
}
