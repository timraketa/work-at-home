import React, { Component } from 'react'
import { ApiClient } from '../api-client/ApiClient'
import { UserStatsRequest } from '../models/Statistics'
import { BarGraph, BarGraphInputData } from './BarGraph'
import { UserWorkHoursStats } from '../models/Statistics'
import { notification } from 'antd'

type State = {
  responseValue: UserWorkHoursStats
  barGraphInputData: BarGraphInputData
}
type Props = {
  apiClient: ApiClient
  userStatsRequest: UserStatsRequest
}

export class UndividedUserWorkHoursStats extends Component<Props, State> {
  public state: State = {
    responseValue: { days: [], from: '', to: '' },
    barGraphInputData: { keys: ['Work hours'], bars: [], index: 'date' }
  }
  private date_format = (date: Date): string => {
    return date.getDate() + '.' + (date.getMonth() + 1) + '.'
  }
  format_response() {
    let startDate: Date = new Date(this.state.responseValue.from)
    let days = this.state.responseValue.days
    let current_date: Date = new Date(startDate)
    let temp_bars: any[] = []
    for (let i = 0; i < days.length; i++) {
      let temp: { date: string; 'Work hours': number } = {
        date: this.date_format(current_date),
        'Work hours': days[i].dayHours
      }
      current_date.setDate(current_date.getDate() + 1) //Sets current_date to its relative tomorrow

      temp_bars.push(temp)
    }

    this.setState({
      barGraphInputData: {
        keys: this.state.barGraphInputData.keys,
        bars: temp_bars,
        index: this.state.barGraphInputData.index
      }
    })
  }
  private async getUndividedWorkHours() {
    try {
      await this.props.apiClient
        .getUndividedUserWorkHoursStats(this.props.userStatsRequest)
        .then(value => {
          this.setState({ responseValue: value })
          this.format_response()
        })
        .catch(() => {})
    } catch (e) {
      notification['error']({
        message: "Couldn't fetch data."
      })
    }
  }
  // private keys: string[] = []
  componentDidMount() {
    this.getUndividedWorkHours()
  }
  componentDidUpdate(prevProps: Props) {
    if (
      prevProps.userStatsRequest.from !== this.props.userStatsRequest.from ||
      prevProps.userStatsRequest.to !== this.props.userStatsRequest.to
    ) {
      this.getUndividedWorkHours()
    }
  }
  render() {
    return <BarGraph data={this.state.barGraphInputData} />
  }
}
