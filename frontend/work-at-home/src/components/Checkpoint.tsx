import {
  Collapse,
  Popover,
  Radio,
  Steps,
  List,
  Button,
  Input,
  ConfigProvider,
  Checkbox,
  Space,
  notification
} from 'antd'
import React, { Component } from 'react'
import { CloseOutlined } from '@ant-design/icons'
import '../style/Button.css'
import { CustomModal } from './CustomModal'
import { ApiClient } from '../api-client/ApiClient'
import { Checkpoint as Check } from '../models/Checkpoint'
import { Empty } from 'antd'
const { Step } = Steps
const { Panel } = Collapse

type State = {
  currentChecklist: number
  currentCheckpoint: number
  projectId: string
  checkpoint: Check[]
  checkpointValue: string
  checklistValue: string[]
  checklistItemValue: string[]
}

type Props = {
  projectId: string
  apiClient: ApiClient
}

export class Checkpoint extends Component<Props, State> {
  constructor(props: Props) {
    super(props)
    this.state = {
      currentChecklist: 0,
      currentCheckpoint: 0,
      projectId: '',
      checkpoint: [
        {
          id: '',
          date: '',
          projectId: '',
          description: '',
          name: '',
          checklists: []
        }
      ],
      checkpointValue: '',
      checklistValue: [],
      checklistItemValue: []
    }
  }
  onChange = (current: number) => {
    this.setState({ currentChecklist: 0, currentCheckpoint: current })
  }

  customDot = (dot: any, { status, index }: any) => (
    <Popover content={<span></span>}>{dot}</Popover>
  )

  handlePositionChange = ({ target: { value } }: any) => {
    this.setState({ currentChecklist: value })
  }

  private async getCheckpoints(id: string) {
    try {
      let checkpointResponse = await this.props.apiClient.getCheckpoint(id)

      this.setState({
        checkpoint: checkpointResponse,
        projectId: id
      })
    } catch {
      notification['error']({
        message: "Couldn't fetch data."
      })
    }
  }

  fillDescriptions() {
    let descriptions: string[] = []
    for (let i = 0; i < this.state.checkpoint.length - 1; i++) {
      descriptions.push('')
    }
    this.setState({ checklistItemValue: descriptions })
  }

  componentDidMount() {
    let id = window.location.search.split('=')[1]
    this.getCheckpoints(id)
    this.fillDescriptions()
  }

  onChangeHandler = async (e: any) => {
    //TODO isChecked where filter
    let data = JSON.stringify(this.state.checkpoint)
    let polje: Check[] = JSON.parse(data)
    let index: any = e.target.id.split(' ')
    let item = polje[index[0]].checklists[index[1]].checklistItems[index[2]]
    polje[index[0]].checklists[index[1]].checklistItems[index[2]].isChecked =
      e.target.checked
    this.setState({ checkpoint: polje })

    let x = {
      checklistId: item.checklistId,
      description: item.description,
      isChecked: e.target.checked
    }
    await this.props.apiClient.editChecklistItem(item.checklistId, x, item.id)
  }

  onChangeAddCheckpoint = (e: any) => {
    this.setState({ checkpointValue: e.target.value })
  }

  onChangeChecklistDescription = (e: any, index: number) => {
    let descriptions = [...this.state.checklistValue]
    descriptions[index] = e.target.value
    this.setState({ checklistValue: descriptions })
  }

  async deleteChecklistHandler(
    indexCheckpoint: number,
    indexChecklist: number
  ) {
    await this.props.apiClient.deleteChecklist(
      this.state.checkpoint[indexCheckpoint + 1].id,
      {
        ids: [
          this.state.checkpoint[indexCheckpoint + 1].checklists[indexChecklist]
            .id
        ]
      }
    )

    let checkpoint1 = JSON.parse(JSON.stringify(this.state.checkpoint))
    checkpoint1[indexCheckpoint + 1].checklists.splice(indexChecklist, 1)

    this.setState({ checkpoint: checkpoint1 })
  }

  addCheckpointHandler = async (e: any) => {
    let checkpoint = {
      date: new Date().toISOString(),
      description: this.state.checkpointValue,
      name: this.state.checkpointValue,
      projectId: this.state.projectId
    }
    let res = await this.props.apiClient.addCheckpoint(
      this.state.projectId,
      checkpoint
    )

    if (this.state.checkpoint.length > 0) {
      this.setState({
        checkpointValue: '',
        checkpoint: [...this.state.checkpoint, { ...res, checklists: [] }]
      })
    } else {
      this.setState({ checkpoint: [{ ...res, checklists: [] }] })
    }
  }

  async deleteCheckpoint(id: number) {
    let ids = this.state.checkpoint[id].id
    await this.props.apiClient.deleteCheckpoint(
      { ids: [ids] },
      this.state.projectId
    )

    let checkpoint1 = this.state.checkpoint
    checkpoint1.splice(id, 1)
    this.setState({ checkpoint: checkpoint1, currentCheckpoint: 0 })
  }

  async addChecklist(index: number) {
    let arr = [...this.state.checklistValue]
    arr[index - 1] = ''
    let cheklist1 = [...this.state.checkpoint]

    let response = await this.props.apiClient.addChecklist(
      this.state.checkpoint[index].id,
      {
        checkpointId: this.state.checkpoint[index].id,
        name: this.state.checklistValue[index - 1]
      }
    )
    cheklist1[index].checklists.push(response)
    this.setState({ checklistValue: arr })
  }

  onChangeChecklistItemDescription = (
    e: any,
    indexCheckpoint: number,
    indexChecklist: number
  ) => {
    let descriptions: string[] = []
    descriptions = [...this.state.checklistItemValue]
    descriptions[indexCheckpoint - 1] = e.target.value
    this.setState({ checklistItemValue: descriptions })
  }
  addChecklistItem = async (
    checklistId: string,
    indexCheckpoint: number,
    index: number
  ) => {
    let checklistItem = {
      checklistId: checklistId,
      description: this.state.checklistItemValue[indexCheckpoint - 1],
      isChecked: false
    }

    let x = [...this.state.checklistItemValue]
    x[indexCheckpoint - 1] = ''
    let response = await this.props.apiClient.addChecklistItem(
      checklistId,
      checklistItem
    )
    let checkpoint1 = JSON.parse(JSON.stringify(this.state.checkpoint))
    if (
      checkpoint1[indexCheckpoint].checklists[index].checklistItems !==
      undefined
    ) {
      checkpoint1[indexCheckpoint].checklists[index].checklistItems.push(
        response
      )
    } else {
      checkpoint1[indexCheckpoint].checklists[index]['checklistItems'] = []
      checkpoint1[indexCheckpoint].checklists[index].checklistItems.push(
        response
      )
    }
    this.setState({ checkpoint: checkpoint1, checklistItemValue: x })
  }
  async deleteChecklistItem(
    checkpointId: number,
    checklistId: number,
    checklistItemId: number
  ) {
    let ids = this.state.checkpoint[checkpointId].checklists[checklistId]
      .checklistItems[checklistItemId].id
    await this.props.apiClient.deleteChecklistItem(
      this.state.checkpoint[checkpointId].checklists[checklistId].id,
      { ids: [ids] }
    )
    let checkpoint1 = JSON.parse(JSON.stringify(this.state.checkpoint))
    checkpoint1[checkpointId].checklists[checklistId].checklistItems.splice(
      checklistItemId,
      1
    )
    this.setState({ checkpoint: checkpoint1 })
  }
  render() {
    const genExtra = (id: number) => (
      <CloseOutlined
        style={{ color: 'red', cursor: 'pointer' }}
        onClick={event => {
          event.stopPropagation()
          this.deleteCheckpoint(id)
        }}
      />
    )

    let dataCheckpoint: any = []
    let newSteps1: {
      name: string
      checklists: {
        name: string
        content: JSX.Element
      }[]
    }[] = []
    let checklist1: {
      name: string
      content: JSX.Element
    }[] = []
    this.state.checkpoint.forEach((checkpoint, index2) => {
      checklist1 = []
      if (checkpoint.checklists.length > 0) {
        checkpoint.checklists.forEach((checklist, ind) => {
          checklist1.push({
            name: checklist.name,
            content: (
              <ul style={{ listStyleType: 'none' }}>
                {checklist.checklistItems !== undefined ? (
                  checklist.checklistItems.map((checklistItem, index) => {
                    return (
                      <li
                        key={
                          index2.toString() +
                          ' ' +
                          ind.toString() +
                          ' ' +
                          index.toString()
                        }
                      >
                        <Checkbox
                          id={
                            index2.toString() +
                            ' ' +
                            ind.toString() +
                            ' ' +
                            index.toString()
                          }
                          checked={checklistItem.isChecked}
                          onChange={this.onChangeHandler}
                        >
                          {checklistItem.description}{' '}
                          <CloseOutlined
                            style={{ color: 'red', cursor: 'pointer' }}
                            onClick={event => {
                              event.preventDefault()
                              this.deleteChecklistItem(index2, ind, index)
                            }}
                          />
                        </Checkbox>
                      </li>
                    )
                  })
                ) : (
                  <>{<Empty description={''} style={{ margin: '7px' }} />}</>
                )}
                <li
                  key={
                    index2.toString() + index2.toString() + index2.toString()
                  }
                >
                  <Space style={{ width: '100%' }} direction="horizontal">
                    <Input
                      className="inpt"
                      style={{ width: '250px' }}
                      placeholder="Checklist item description..."
                      bordered={false}
                      value={this.state.checklistItemValue[index2 - 1]}
                      onChange={e =>
                        this.onChangeChecklistItemDescription(e, index2, ind)
                      }
                    />
                    <Button
                      onClick={() => {
                        this.addChecklistItem(checklist.id, index2, ind)
                      }}
                    >
                      Add
                    </Button>
                  </Space>
                </li>
              </ul>
            )
          })
        })
      }

      newSteps1.push({
        name: checkpoint.name,
        checklists: checklist1
      })
    })

    newSteps1.forEach((value, index) => {
      dataCheckpoint.push(
        <Panel extra={genExtra(index + 1)} header={value.name} key={index}>
          <List key={index * newSteps1.length}>
            {value.checklists.map((text, index1) => (
              <List.Item key={index1}>
                {text.name}{' '}
                <CloseOutlined
                  onClick={() => {
                    this.deleteChecklistHandler(index, index1)
                  }}
                  style={{ color: 'red', cursor: 'pointer' }}
                />
              </List.Item>
            ))}
            <List.Item>
              <Input
                onChange={e => this.onChangeChecklistDescription(e, index)}
                key={index}
                placeholder={'Checklist description..'}
                value={this.state.checklistValue[index]}
              />
              <Button
                onClick={() => {
                  this.addChecklist(index + 1)
                }}
              >
                Add
              </Button>
            </List.Item>
          </List>
        </Panel>
      )
    })
    let checkpointData = (
      <>
        <Collapse accordion>{dataCheckpoint}</Collapse>
        <List.Item key="213521">
          <ConfigProvider componentSize={'large'}>
            <Input
              onChange={this.onChangeAddCheckpoint}
              value={this.state.checkpointValue}
              placeholder={'Checkpoint description..'}
            />
            <Button onClick={this.addCheckpointHandler}>Add</Button>
          </ConfigProvider>
        </List.Item>
      </>
    )
    return (
      <>
        {newSteps1.length > 0 ? (
          <>
            <Steps
              current={this.state.currentCheckpoint}
              type="navigation"
              size="small"
              style={{ width: 'auto' }}
              onChange={this.onChange}
              className="site-navigation-steps"
            >
              {newSteps1.map(item => (
                <Step key={item.name} title={item.name} />
              ))}
              <CustomModal
                style={{ marginLeft: '30px' }}
                data={checkpointData}
                title={'Edit checkpoint'}
                titleButton={'Edit'}
                callback={() => {}}
                selection=""
              />
            </Steps>
            <div style={{ textAlign: 'center', margin: '5px' }}>
              <Radio.Group
                onChange={this.handlePositionChange}
                style={{ marginBottom: 8, textAlign: 'center' }}
                value={this.state.currentChecklist}
              >
                {newSteps1[this.state.currentCheckpoint].checklists.map(
                  (checklist, index) => {
                    return (
                      <Radio.Button key={index} value={index}>
                        {checklist.name}
                      </Radio.Button>
                    )
                  }
                )}
              </Radio.Group>
            </div>
            <div style={{ textAlign: 'left' }}>
              <div className="steps-content">
                {newSteps1[this.state.currentCheckpoint].checklists.length >
                0 ? (
                  newSteps1[this.state.currentCheckpoint].checklists[
                    this.state.currentChecklist
                  ].content
                ) : (
                  <>
                    <Empty image={Empty.PRESENTED_IMAGE_SIMPLE} />
                  </>
                )}
              </div>
            </div>
          </>
        ) : (
          <div style={{ textAlign: 'center' }}>
            Add checkpoint{' '}
            <CustomModal
              style={{ marginLeft: '30px' }}
              data={checkpointData}
              title={'Edit checkpoint'}
              titleButton={'Edit'}
              callback={() => {}}
              selection=""
            />
            <Empty image={Empty.PRESENTED_IMAGE_SIMPLE} />
          </div>
        )}
      </>
    )
  }
}
