import { notification } from 'antd'
import React, { Component } from 'react'
import { ApiClient } from '../api-client/ApiClient'
import {
  ProjectSpendingDayStats,
  ProjectStatsRequest
} from '../models/Statistics'
import { BarGraph, BarGraphInputData } from './BarGraph'

type State = {
  responseValue: ProjectSpendingDayStats[]
  barGraphInputData: BarGraphInputData
}
type Props = {
  apiClient: ApiClient
  projectStatsRequest: ProjectStatsRequest
}

export class ProjectSpendingBarGraph extends Component<Props, State> {
  public state: State = {
    responseValue: [],
    barGraphInputData: { keys: ['Spending'], bars: [], index: 'date' }
  }

  private date_format = (date: Date): string => {
    return date.getDate() + '.' + (date.getMonth() + 1) + '.'
  }
  private isSameDay = (d1: Date, d2: Date) => {
    return (
      d1.getFullYear() === d2.getFullYear() &&
      d1.getMonth() === d2.getMonth() &&
      d1.getDate() === d2.getDate()
    )
  }
  format_response() {
    let temp_bars: any[] = []
    if (
      this.props.projectStatsRequest.from !== '' &&
      this.props.projectStatsRequest.to !== ''
    ) {
      let startdate: Date = new Date(this.props.projectStatsRequest.from)
      let enddate: Date = new Date(this.props.projectStatsRequest.to)

      /* Day after requested end date for  */
      let enddate_p1: Date = new Date(enddate.getTime())
      enddate_p1.setDate(enddate_p1.getDate() + 1)

      for (
        let c_date: Date = startdate;
        !this.isSameDay(c_date, enddate_p1);
        c_date.setDate(c_date.getDate() + 1)
      ) {
        let hasData: boolean = false
        for (let i = 0; i < this.state.responseValue.length; i++) {
          let date: Date = new Date(this.state.responseValue[i].date)
          if (this.isSameDay(c_date, date)) {
            let spending: number = this.state.responseValue[i].spending
            let temp = { date: this.date_format(date), Spending: spending }
            temp_bars.push(temp)
            hasData = true
          }
        }
        if (!hasData) {
          let temp = { date: this.date_format(c_date), Spending: 0 }
          temp_bars.push(temp)
        }
      }

      this.setState({
        barGraphInputData: {
          bars: temp_bars,
          keys: this.state.barGraphInputData.keys,
          index: this.state.barGraphInputData.index
        }
      })
    }
  }
  private async getSpending() {
    try {
      const value = await this.props.apiClient.getProjectSpendingStats(
        this.props.projectStatsRequest
      )

      this.setState({ responseValue: value })
      this.format_response()
    } catch (e) {
      notification['error']({
        message: e.message.includes('status code 40')
          ? "You don't have permission to view project statistics."
          : 'Error statistic data data'
      })
    }
  }
  componentDidMount() {
    this.getSpending()
  }

  componentDidUpdate(prevProps: Props) {
    if (
      prevProps.projectStatsRequest.from !==
        this.props.projectStatsRequest.from ||
      prevProps.projectStatsRequest.to !== this.props.projectStatsRequest.to
    ) {
      this.getSpending()
    }
  }
  render() {
    return <BarGraph data={this.state.barGraphInputData} />
  }
}
