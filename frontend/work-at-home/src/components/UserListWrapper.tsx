import React, { ReactText } from 'react'
import { Typography, Space } from 'antd'
import { UserList, UserListDataType } from './UserList'
import { User } from '../models/User'

const { Title } = Typography

type Props = {
  callback?: any
  data: User[]
  type?: 'radio' | 'checkbox' | undefined
  selectEnabled: boolean
  selected?: string[] | undefined
}
type State = {
  data: UserListDataType[]
  selected?: string[]
}

export class UserListWrapper extends React.Component<Props, State> {
  state: State = {
    data: [],
    selected: this.props.selected
  }

  onSelectChange = (selectedRows: ReactText[]) => {
    let userList: string[] = []
    this.props.data.forEach(value => {
      const result: boolean = selectedRows.includes(value.id)

      if (result) {
        userList.push(value.id)
      }
    })
    this.props.callback(userList)
    this.setState({ selected: userList })
  }

  render() {
    return (
      <>
        <Space direction="vertical">
          <Title>List of Users</Title>
          <UserList
            selected={this.state.selected}
            data={this.props.data.map(user => {
              console.log(user.id)
              return {
                ...user,
                key: user.id
              }
            })}
            selectEnabled={this.props.selectEnabled}
            onSelectChange={this.onSelectChange}
            loading={false}
            type={this.props.type}
          />
        </Space>
      </>
    )
  }
}
