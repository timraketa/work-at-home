import React, { Component } from 'react'
import '../style/Button.css'
type State = {}
type Props = {
  onClick?:
    | ((event: React.MouseEvent<HTMLButtonElement, MouseEvent>) => void)
    | undefined
}
export class Button extends Component<Props, State> {
  render() {
    return (
      <button onClick={this.props.onClick} className="button1">
        {this.props.children}
      </button>
    )
  }
}
