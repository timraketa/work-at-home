import React, { Component } from 'react'
import { ResponsiveBar } from '@nivo/bar'

export type BarGraphInputData = {
  keys: string[]
  bars: any[]
  index: string
}
type State = {}
type Props = { data: BarGraphInputData }

export class BarGraph extends Component<Props, State> {
  MyResponsiveBar = (data: {
    bars: { string: string | number }[]
    keys: string[]
    index: string
  }) => (
    <div id="bargraph" style={{ height: '500px', width: '700px' }}>
      {/* div's height & width determine graphs size (move to props?) */}
      <ResponsiveBar
        data={data.bars}
        keys={data.keys}
        indexBy={data.index}
        margin={{ top: 50, right: 130, bottom: 50, left: 60 }}
        padding={0.3}
        valueScale={{ type: 'linear' }}
        indexScale={{ type: 'band', round: true }}
        colors={{ scheme: 'nivo' }}
        enableGridY={true}
        labelSkipWidth={12}
        labelSkipHeight={12}
        labelTextColor={{ from: 'color', modifiers: [['darker', 1.6]] }}
        borderRadius={3}
        legends={[
          {
            dataFrom: 'keys',
            anchor: 'bottom-right',
            direction: 'column',
            justify: false,
            translateX: 120,
            translateY: 0,
            itemsSpacing: 2,
            itemWidth: 100,
            itemHeight: 20,
            itemDirection: 'left-to-right',
            itemOpacity: 0.85,
            symbolSize: 20,
            effects: [
              {
                on: 'hover',
                style: {
                  itemOpacity: 1
                }
              }
            ]
          }
        ]}
        motionStiffness={90}
        motionDamping={15}
      />
    </div>
  )
  render() {
    if (
      (this.props.data.bars.length === 1 &&
        (!Object.keys(this.props.data.bars[0]).includes('Work hours') ||
          this.props.data.bars[0]['Work hours'] === 0) &&
        (!Object.keys(this.props.data.bars[0]).includes('Spending') ||
          this.props.data.bars[0]['Spending'] === 0)) ||
      !this.props.data.bars.length
    ) {
      return (
        <div>
          <span>No data to show, try adjusting the range.</span>
        </div>
      )
    }
    return <div id="graph">{this.MyResponsiveBar(this.props.data)}</div>
  }
}
