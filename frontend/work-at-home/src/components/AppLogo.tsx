import * as React from 'react'

export class AppLogo extends React.Component {
  render() {
    return (
      <label
        style={{
          pointerEvents: 'none',
          color: '#adc6ff',
          fontSize: '20px'
        }}
      >
        Work<span style={{ color: '#597ef7' }}>@</span>Home
      </label>
    )
  }
}
