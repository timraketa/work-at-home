import React from 'react'
import { Card, Layout, notification, Space, Table, Tag } from 'antd'
import { ApiClient, HomePageUser } from '../api-client/ApiClient'
import { Frame } from '../components'
import Title from 'antd/lib/typography/Title'
import { green, geekblue, magenta, volcano } from '@ant-design/colors'

const { Content } = Layout

type TableData = HomePageUser & {
  key: string
}

type Props = {
  apiClient: ApiClient
}

type State = {
  userList: TableData[]
  loading: boolean
}

const columns = [
  {
    title: 'Username',
    dataIndex: 'username',
    key: 'username'
  },
  {
    title: 'Firm name',
    dataIndex: 'firmName',
    key: 'firmName'
  },
  {
    title: 'Roles',
    dataIndex: 'roles',
    key: 'roles',
    align: 'center',
    render: (roles: string[]) => (
      <>
        {roles.map(role => {
          let color = 'red'

          switch (role) {
            case 'Admin':
              color = 'green'
              break
            case 'Worker':
              color = 'geekblue'
              break
            case 'Team Lead':
              color = 'magenta'
              break
            case 'Project Manager':
              color = 'volcano'
              break
          }

          return (
            <Tag color={color} key={role}>
              {role.toUpperCase()}
            </Tag>
          )
        })}
      </>
    )
  }
]

export class HomePage extends React.Component<Props, State> {
  public state: State = {
    userList: [],
    loading: false
  }

  componentDidMount() {
    this.getHomePageList()
  }

  private async getHomePageList() {
    try {
      this.setState({
        loading: true
      })

      const userList = await this.props.apiClient.getHomePageList()

      this.setState({
        userList: userList.map(user => {
          return {
            ...user,
            key: Math.random().toString()
          }
        }),
        loading: false
      })
    } catch (e) {
      this.setState({
        loading: false
      })

      notification['error']({
        message: 'Error getting user list.'
      })
    }
  }

  render() {
    return (
      <>
        <Content
          style={{
            padding: '0 50px',
            backgroundColor: 'white',
            minHeight: 'auto'
          }}
        >
          <div
            className="site-layout-content"
            style={{ alignItems: 'flex-start' }}
          >
            <div>
              <Space direction="vertical" size={50}>
                <Card title="Welcome:">
                  <p>
                    Welcome to Work@Home. The app helps you keep track of hours
                    spent working on different projects.
                  </p>
                </Card>
                <div>
                  <Title level={2}>App guide:</Title>
                  <Card title="1. Authentication">
                    <p>
                      All actions within the app require the user to be logged
                      in.
                    </p>
                    <p>
                      On the right, you can see a list of predefined users you
                      can use to log into the app.
                    </p>
                  </Card>
                  <br />
                  <Card title="2. User roles">
                    <p>
                      Each role has a number of permissions it is allowed to do
                      on the api. Most users have multiple roles attached to
                      them, since role permissions are only bound to one role.
                    </p>
                    <p>
                      <strong>Note:</strong> Most app actions require the user
                      to be a worker. E.g. Non-worker users canno't access the
                      calendar, list projects, ...
                    </p>
                    <ol>
                      <li>
                        <span style={{ color: green.primary }} key="Admin">
                          Admin
                        </span>{' '}
                        - Can add users
                      </li>
                      <li>
                        <span style={{ color: geekblue.primary }} key="Worker">
                          Worker
                        </span>{' '}
                        - Can view and edit work hours, view personal statistics
                        and firm projects
                      </li>
                      <li>
                        <span
                          style={{ color: magenta.primary }}
                          key="Team Lead"
                        >
                          Team Lead
                        </span>{' '}
                        - Is appointed as a team leader, no other functions
                        require this role
                      </li>
                      <li>
                        <span
                          style={{ color: volcano.primary }}
                          key="Project Manager"
                        >
                          Project Manager
                        </span>{' '}
                        - Can add, edit and delete all project related
                        information and view project statistics
                      </li>
                    </ol>
                  </Card>
                  <br />
                  <Card title="3. Statistics">
                    <p>
                      Statistics graphs are controled using a date range picker
                      displayed on the left side of the screen.
                    </p>
                    <p>
                      <strong>User statistics</strong> can be viewed on the
                      profile page. There are 2 graphs showed that display the
                      users work hours. One is a cumulative display of all
                      hours, and the other is divided into projects.
                    </p>
                    <p>
                      <strong>Project statistics</strong> can only be viewed by
                      project managers on the project info page. You can access
                      a projects' info page through the project list page. The
                      graphs display total hours that users spent working on the
                      project and the amount of money spent in the process (work
                      hour * users' hourly price).
                    </p>
                  </Card>
                  <br />
                  <Card title="4. Projects">
                    <p>
                      Projects are added by inputing its information on the Add
                      Project screen. Important things to note are: only project
                      managers can add new projects and only to the firms where
                      they have the project manager role, the{' '}
                      <i>Enable reminder</i> checkbox currently has no impact on
                      the project and is a feature that will be implemented in
                      the future.
                    </p>
                    <p>
                      Project managers are also in charge of dividing workers
                      into teams and can only appoint the team leader position
                      to a user that has the <i>Team Lead</i> role in that firm.
                      <br />
                      Team are a great way for the PM to keep track of what
                      worker is assigned to which part of the project.
                    </p>
                  </Card>
                  <br />
                  <Card title="5. Checkpoints and checklists">
                    <p>
                      Project managers have the ability to create checkpoints
                      for projects.
                    </p>
                    <p>
                      Checkpoints serve as milestones to help keep track of the
                      project progress. Checkpoints can have multiple checklists
                      attached to them that need to be finished before moving
                      onto the next checkpoint.
                    </p>
                  </Card>
                  <br />
                  <Card title="6. Calendar">
                    <p>
                      The calendar interface is used by workers to input and
                      keep track of their working hours.
                    </p>
                    <p>
                      To input an hour, the user must provide a description of
                      his/hers work and select the project that they worked on.
                    </p>
                  </Card>
                  <br />
                  <Title level={2}>API guide:</Title>
                  <Card title="API Explorer">
                    <p>
                      The api explorer can be viewed{' '}
                      <a
                        target="blank"
                        href={`${process.env.REACT_APP_API_URL}/explorer`}
                      >
                        here
                      </a>
                    </p>
                    <p>
                      The explorer provides an interface made by Swagger
                      OpenAPI3 to test all api calls. Each methond provides a
                      parameter and response type example.
                    </p>
                    <p>
                      <strong>Note:</strong> since all routes (except the route
                      for listing users on the home page) require the user to be
                      logged in, you can first use the login route to get the
                      JWT token and then input that token in the Authorize input
                      on the top of the page.
                    </p>
                  </Card>
                </div>
              </Space>
            </div>
            <div style={{ position: 'sticky', top: '50px' }}>
              <label>
                Predefined users (every password is set to <i>string</i>):
              </label>
              <Frame>
                <Table
                  dataSource={this.state.userList}
                  loading={this.state.loading}
                  // @ts-ignore
                  columns={columns}
                  pagination={false}
                />
              </Frame>
            </div>
          </div>
        </Content>
      </>
    )
  }
}
