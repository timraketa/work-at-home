import React, { ReactText } from 'react'
import { UserListWrapper, Frame } from '../components'
import { ApiClient, UserFirm } from '../api-client/ApiClient'
import { User } from '../models/User'
import { Layout, notification, Select, Space } from 'antd'

const { Content } = Layout
const { Option } = Select

type Props = {
  apiClient: ApiClient
}

type State = {
  selected: ReactText[]
  data: User[]
  firms: UserFirm[]
  selectedFirm: UserFirm
}

export class Colleagues extends React.Component<Props, State> {
  public state: State = {
    data: [],
    selected: [],
    firms: [],
    selectedFirm: {
      firmId: '',
      firmName: '',
      firmEmail: '',
      roles: []
    }
  }

  componentDidMount() {
    this.getFirms()
  }

  private async getUsers(firmId: string) {
    try {
      const users: User[] = await this.props.apiClient.getUsers(firmId)
      this.setState({ data: users })
    } catch (e) {
      notification['error']({
        message: 'Error fetching users!'
      })
    }
  }

  onFirmSelectChange = (value: any): any => {
    this.setState({ selectedFirm: this.state.firms[value] })
    this.getUsers(this.state.firms[value].firmId)
  }

  private async getFirms() {
    try {
      let response = await this.props.apiClient.getUserFirms()
      this.setState({
        firms: response,
        selectedFirm: response.length ? response[0] : this.state.selectedFirm
      })

      if (response.length) {
        await this.getUsers(response[0].firmId)
      }
    } catch {
      notification['error']({
        message: 'Error while fetching data.'
      })
    }
  }

  render() {
    return (
      <>
        <Content
          style={{
            backgroundColor: 'white',
            minHeight: 'auto'
          }}
        >
          <div className="site-layout-content container">
            <div>
              <label style={{ marginRight: '15px' }}>Choose a firm:</label>
              <Select
                defaultValue={0}
                onChange={this.onFirmSelectChange}
                style={{ maxWidth: '150px' }}
              >
                {this.state.firms.map((firm, index) => {
                  return (
                    <Option key={firm.firmName} value={index}>
                      {firm.firmName}
                    </Option>
                  )
                })}
              </Select>
              <Frame>
                <Space direction="vertical">
                  <UserListWrapper
                    callback={() => {}}
                    data={this.state.data}
                    selectEnabled={false}
                  />
                </Space>
              </Frame>
            </div>
          </div>
        </Content>
      </>
    )
  }
}
