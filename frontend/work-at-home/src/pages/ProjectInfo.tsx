import React from 'react'
import {
  Layout,
  Typography,
  Form,
  Cascader,
  DatePicker,
  Spin,
  notification,
  Input,
  Button as AntdButton,
  InputNumber
} from 'antd'
import { Link } from 'react-router-dom'
import {
  CaretDownOutlined,
  CloseOutlined,
  EditOutlined
} from '@ant-design/icons'
import {
  Button,
  Frame,
  ProjectWorkHoursBarGraph,
  ProjectSpendingBarGraph,
  Checkpoint,
  CustomModal,
  UserListWrapper
} from '../components'
import '../style/ProjectInfo.css'
import { ProjectStatsRequest } from '../models/Statistics'
import { ApiClient, ProjectReturnType } from '../api-client/ApiClient'
import '../style/ProjectInfo.css'
import { User } from '../models/User'
import { UpdateProject } from '../models/Project'
import moment from 'moment'

const { Title, Text } = Typography
const { Content } = Layout
const { RangePicker } = DatePicker
const layout = {
  labelCol: { span: 10 },
  wrapperCol: { span: 18 }
}

type optns = [
  {
    value: string
    label: string | any
  }
]

type State = {
  project?: ProjectReturnType
  users: User[]
  selected: string[][]
  selectedTeam: string
  current: number
  projectStatsRequest: ProjectStatsRequest
  isEditing: boolean
  editings: {
    minDailyHours: boolean
    maxWeeklyHours: boolean
    name: boolean
    budget: boolean
    dueDate: boolean
    description: boolean
  }
  updatedField: string
}
type Props = {
  apiClient: ApiClient
}
export class ProjectInfo extends React.Component<Props, State> {
  public state: State = {
    users: [],
    selected: [],
    selectedTeam: '',
    current: 0, // == step-1
    projectStatsRequest: {
      from: new Date().toISOString(),
      to: new Date().toISOString(),
      projectId: window.location.search.split('=')[1]
    },
    isEditing: false,
    editings: {
      minDailyHours: false,
      maxWeeklyHours: false,
      name: false,
      budget: false,
      dueDate: false,
      description: false
    },
    updatedField: ''
  }

  private async getProject(projectId: string) {
    try {
      const response = await this.props.apiClient.getProject(projectId)

      this.setState({
        project: response
      })

      this.getUsers()
    } catch (e) {
      notification['error']({
        message: 'e'
      })
    }
  }

  onChange = (current: number) => {
    this.setState({ current })
  }

  onChangeDateRange = (value: any) => {
    let startDate: Date = value[0]._d
    let endDate: Date = value[1]._d
    this.setState({
      projectStatsRequest: {
        from: startDate.toISOString(),
        to: endDate.toISOString(),
        projectId: this.state.projectStatsRequest.projectId
      }
    })
  }

  callback = (selectedTeam: string) => {
    this.setState({ selectedTeam })
  }

  private async updateTeamMembers() {
    let changed: number[] = []
    this.state.selected.forEach((value, index) => {
      if (!(value[0] === 'prazan')) {
        changed.push(index)
      }
    })
    let del: string[] = []
    let post: string[] = []
    changed.forEach(async value => {
      if (!this.state.project || !this.state.project.teams) {
        return
      }

      const team = this.state.project.teams[value]
      team.users.forEach(user => {
        if (!this.state.selected[value].includes(user.id)) {
          if (!(user.id === team.teamLeadId)) {
            del.push(user.id)
          }
        }
      })
      await this.props.apiClient.deleteTeamMembers(team.id, {
        ids: del
      })
      this.state.selected[value].forEach(userId => {
        if (!team.users.filter(user => user.id === userId).length) {
          if (!(userId === team.teamLeadId)) {
            post.push(userId)
          }
        }
      })
      await this.props.apiClient.addTeamMembers(team.id, {
        ids: post
      })
    })
  }
  private async getUsers() {
    if (!this.state.project) {
      return
    }

    const users: User[] = await this.props.apiClient.getUsers(
      this.state.project.firmId
    )
    this.setState({ users })
  }

  componentDidMount() {
    const id = window.location.search.split('=')[1]

    this.getProject(id)
  }

  componentWillUnmount() {
    this.updateTeamMembers()
  }

  addMemeberHandler = (user: any) => {
    if (!this.state.project) {
      return
    }

    let data: string[][] = []
    if (this.state.selected.length === 0 && this.state.project.teams) {
      for (let i = 0; i < this.state.project.teams.length; i++) {
        data.push(['prazan'])
      }
    } else {
      data = this.state.selected
    }
    data[Number(this.state.selectedTeam)] = user
    this.setState({ selected: data })
  }

  render() {
    if (!this.state.project) {
      return (
        <>
          <Content
            style={{
              backgroundColor: 'white',
              minHeight: 'auto',
              padding: '0 50px'
            }}
          >
            <div style={{ position: 'absolute', top: '48%', left: '48%' }}>
              <Spin size="large" />
            </div>
          </Content>
        </>
      )
    }

    return (
      <>
        <Content
          style={{
            backgroundColor: 'white',
            minHeight: 'auto',
            padding: '0 50px'
          }}
        >
          <div
            className="site-layout-content cont2"
            style={{ alignItems: 'flex-start' }}
          >
            <div style={{ position: 'sticky', top: '0px', flexBasis: '40%' }}>
              <div className="info-field-wrapper">
                {this.state.editings.name ? (
                  <Input
                    type="text"
                    value={this.state.updatedField}
                    onChange={e => {
                      this.setState({
                        updatedField: e.target.value
                      })
                    }}
                  />
                ) : (
                  <Title>{this.state.project.name}</Title>
                )}
                <div className="editing-button--wrapper">
                  <EditOutlined onClick={() => this.handleEditClick('name')} />
                  <AntdButton
                    disabled={!this.state.editings.name}
                    onClick={() => this.handleSaveClick('name')}
                  >
                    Save
                  </AntdButton>
                  <CloseOutlined
                    onClick={() => {
                      this.setState({
                        isEditing: false,
                        editings: {
                          minDailyHours: false,
                          maxWeeklyHours: false,
                          name: false,
                          budget: false,
                          dueDate: false,
                          description: false
                        }
                      })
                    }}
                  />
                </div>
              </div>
              <Form labelAlign="left" {...layout} name="nest-messages1">
                <Form.Item label="Description">
                  <div className="info-field-wrapper">
                    {this.state.editings.description ? (
                      <Input
                        type="text"
                        value={this.state.updatedField}
                        onChange={e => {
                          this.setState({
                            updatedField: e.target.value
                          })
                        }}
                      />
                    ) : (
                      <Text>{this.state.project.description}</Text>
                    )}
                    <div className="editing-button--wrapper">
                      <EditOutlined
                        onClick={() => this.handleEditClick('description')}
                      />
                      <AntdButton
                        disabled={!this.state.editings.description}
                        onClick={() => this.handleSaveClick('description')}
                      >
                        Save
                      </AntdButton>
                      <CloseOutlined
                        onClick={() => {
                          this.setState({
                            isEditing: false,
                            editings: {
                              minDailyHours: false,
                              maxWeeklyHours: false,
                              name: false,
                              budget: false,
                              dueDate: false,
                              description: false
                            }
                          })
                        }}
                      />
                    </div>
                  </div>
                </Form.Item>
                <Form.Item label="Manager">
                  <Text>{`${this.state.project.projectManager.firstName} ${this.state.project.projectManager.lastName}`}</Text>
                </Form.Item>
                <div style={{ textAlign: 'left' }}>
                  <Form.Item label="Teams">
                    <div className="teams--container">
                      {!this.state.project.teams ||
                      this.state.project.teams.length === 0 ? (
                        <></>
                      ) : (
                        this.state.project.teams.map((team, index) => {
                          let options: optns = [{ value: '', label: '' }]
                          options.pop()
                          this.state.selected.length === 0 ||
                          this.state.selected[index][0] === 'prazan'
                            ? team.users.forEach(user =>
                                options.push({
                                  value: `${user.firstName} ${user.lastName}`,
                                  label: `${user.firstName} ${user.lastName}`
                                })
                              )
                            : this.state.users.forEach(user => {
                                if (
                                  this.state.selected[index].includes(
                                    user.id
                                  ) ||
                                  (this.state.project &&
                                    user.id ===
                                      this.state.project.teams![index]
                                        .teamLeadId)
                                )
                                  options.push({
                                    value: `${user.firstName} ${user.lastName}`,
                                    label: `${user.firstName} ${user.lastName}`
                                  })
                              })
                          let data1: User[] = []
                          if (
                            this.state.project &&
                            this.state.project.teams!.length > 0
                          ) {
                            this.state.users.forEach(value => {
                              if (
                                this.state.project &&
                                value.id !==
                                  this.state.project.teams![index].teamLeadId
                              ) {
                                data1.push(value)
                              }
                            })
                          }
                          let data = (
                            <UserListWrapper
                              selected={team.users.map(user => user.id)}
                              callback={this.addMemeberHandler}
                              data={data1}
                              type={'checkbox'}
                              selectEnabled={true}
                            />
                          )
                          options.push({
                            value: '0',
                            label: (
                              <CustomModal
                                key={team.id}
                                selection={index.toString()}
                                data={data}
                                title={'Add new member'}
                                titleButton={'Add new member'}
                                callback={this.callback}
                              ></CustomModal>
                            )
                          })
                          return (
                            <div style={{ display: 'flex' }} key={team.name}>
                              <span style={{ flexBasis: '50%' }}>
                                {team.name}
                              </span>
                              <Cascader key={index + 2} options={options}>
                                <CaretDownOutlined
                                  style={{ border: '1px solid black' }}
                                />
                              </Cascader>
                            </div>
                          )
                        })
                      )}
                      <div style={{ marginTop: '20px' }}>
                        <Link
                          to={`/addteamfromproject?prid=${this.state.project.id}&frid=${this.state.project.firmId}`}
                        >
                          <Button>Add new team</Button>
                        </Link>
                      </div>
                    </div>
                  </Form.Item>
                </div>

                <Form.Item label="Budget">
                  <div className="info-field-wrapper">
                    {this.state.editings.budget ? (
                      <InputNumber
                        formatter={value => `${value}€`}
                        parser={(value = '€') => value.replace('€', '')}
                        value={parseInt(this.state.updatedField)}
                        onChange={e => {
                          if (e) {
                            this.setState({
                              updatedField: e.toString()
                            })
                          }
                        }}
                      />
                    ) : (
                      <Text>{this.state.project.budget}</Text>
                    )}
                    <div className="editing-button--wrapper">
                      <EditOutlined
                        onClick={() => this.handleEditClick('budget')}
                      />
                      <AntdButton
                        disabled={!this.state.editings.budget}
                        onClick={() => this.handleSaveClick('budget')}
                      >
                        Save
                      </AntdButton>
                      <CloseOutlined
                        onClick={() => {
                          this.setState({
                            isEditing: false,
                            editings: {
                              minDailyHours: false,
                              maxWeeklyHours: false,
                              name: false,
                              budget: false,
                              dueDate: false,
                              description: false
                            }
                          })
                        }}
                      />
                    </div>
                  </div>
                </Form.Item>
                <Form.Item label="Min hours per day">
                  <div className="info-field-wrapper">
                    {this.state.editings.minDailyHours ? (
                      <InputNumber
                        formatter={value => `${value}h`}
                        parser={(value = '€') => value.replace('h', '')}
                        value={parseInt(this.state.updatedField)}
                        onChange={e => {
                          if (e) {
                            this.setState({
                              updatedField: e.toString()
                            })
                          }
                        }}
                      />
                    ) : (
                      <Text>{this.state.project.minDailyHours}h</Text>
                    )}
                    <div className="editing-button--wrapper">
                      <EditOutlined
                        onClick={() => this.handleEditClick('minDailyHours')}
                      />
                      <AntdButton
                        disabled={!this.state.editings.minDailyHours}
                        onClick={() => this.handleSaveClick('minDailyHours')}
                      >
                        Save
                      </AntdButton>
                      <CloseOutlined
                        onClick={() => {
                          this.setState({
                            isEditing: false,
                            editings: {
                              minDailyHours: false,
                              maxWeeklyHours: false,
                              name: false,
                              budget: false,
                              dueDate: false,
                              description: false
                            }
                          })
                        }}
                      />
                    </div>
                  </div>
                </Form.Item>
                <Form.Item label="Max hours per week">
                  <div className="info-field-wrapper">
                    {this.state.editings.maxWeeklyHours ? (
                      <InputNumber
                        formatter={value => `${value}h`}
                        parser={(value = '€') => value.replace('h', '')}
                        value={parseInt(this.state.updatedField)}
                        onChange={e => {
                          if (e) {
                            this.setState({
                              updatedField: e.toString()
                            })
                          }
                        }}
                      />
                    ) : (
                      <Text>{this.state.project.maxWeeklyHours}h</Text>
                    )}
                    <div className="editing-button--wrapper">
                      <EditOutlined
                        onClick={() => this.handleEditClick('maxWeeklyHours')}
                      />
                      <AntdButton
                        disabled={!this.state.editings.maxWeeklyHours}
                        onClick={() => this.handleSaveClick('maxWeeklyHours')}
                      >
                        Save
                      </AntdButton>
                      <CloseOutlined
                        onClick={() => {
                          this.setState({
                            isEditing: false,
                            editings: {
                              minDailyHours: false,
                              maxWeeklyHours: false,
                              name: false,
                              budget: false,
                              dueDate: false,
                              description: false
                            }
                          })
                        }}
                      />
                    </div>
                  </div>
                </Form.Item>
                <Form.Item label="Due date">
                  <div className="info-field-wrapper">
                    {this.state.editings.dueDate ? (
                      <DatePicker
                        value={moment(this.state.updatedField)}
                        onChange={e => {
                          if (e) {
                            this.setState({
                              updatedField: e.toISOString()
                            })
                          }
                        }}
                      />
                    ) : (
                      <Text>
                        {moment(this.state.project.dueDate).format('D.M.YYYY.')}
                      </Text>
                    )}
                    <div className="editing-button--wrapper">
                      <EditOutlined
                        onClick={() => this.handleEditClick('dueDate')}
                      />
                      <AntdButton
                        disabled={!this.state.editings.dueDate}
                        onClick={() => this.handleSaveClick('dueDate')}
                      >
                        Save
                      </AntdButton>
                      <CloseOutlined
                        onClick={() => {
                          this.setState({
                            isEditing: false,
                            editings: {
                              minDailyHours: false,
                              maxWeeklyHours: false,
                              name: false,
                              budget: false,
                              dueDate: false,
                              description: false
                            }
                          })
                        }}
                      />
                    </div>
                  </div>
                </Form.Item>

                <Form.Item label="Statistics:">
                  <RangePicker onChange={this.onChangeDateRange} />
                </Form.Item>
              </Form>
            </div>
            <div>
              <div>
                <label>Checkpoints:</label>
                <Frame>
                  <Checkpoint
                    projectId={this.state.project.id}
                    apiClient={this.props.apiClient}
                  />
                </Frame>
              </div>
              <div className="statistics--container">
                <label>Project statistics:</label>
                <ProjectWorkHoursBarGraph
                  apiClient={this.props.apiClient}
                  projectStatsRequest={this.state.projectStatsRequest}
                />
                <ProjectSpendingBarGraph
                  apiClient={this.props.apiClient}
                  projectStatsRequest={this.state.projectStatsRequest}
                />
              </div>
            </div>
          </div>
        </Content>
      </>
    )
  }

  private handleEditClick(fieldName: string) {
    if (this.state.isEditing) {
      return
    }

    const editings = {
      minDailyHours: false,
      maxWeeklyHours: false,
      name: false,
      budget: false,
      dueDate: false,
      description: false
    }

    // @ts-ignore
    editings[fieldName] = true

    this.setState({
      isEditing: true,
      editings,
      // @ts-ignore
      updatedField: this.state.project[fieldName]
    })
  }

  private async handleSaveClick(fieldName: string) {
    const project: UpdateProject = {
      minDailyHours: parseInt(String(this.state.project!.minDailyHours)),
      maxWeeklyHours: parseInt(String(this.state.project!.maxWeeklyHours)),
      budget: parseInt(String(this.state.project!.budget)),
      name: this.state.project!.name,
      dueDate: this.state.project!.dueDate,
      reminderEnabled: this.state.project!.reminderEnabled,
      description: this.state.project!.description
    }

    // @ts-ignore
    project[fieldName] =
      fieldName === 'dueDate' ||
      fieldName === 'name' ||
      fieldName === 'description'
        ? this.state.updatedField
        : parseInt(this.state.updatedField)

    try {
      const id = window.location.search.split('=')[1]

      await this.props.apiClient.updateProject(project, id)
      this.setState({
        isEditing: false,
        editings: {
          minDailyHours: false,
          maxWeeklyHours: false,
          name: false,
          budget: false,
          dueDate: false,
          description: false
        }
      })

      this.getProject(id)
    } catch (e) {
      notification['error']({
        message:
          'An error occured while updating the project. Make sure you are the project manager of this project'
      })
    }
  }
}
