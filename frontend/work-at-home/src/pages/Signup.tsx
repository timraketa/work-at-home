import React from 'react'
import { Layout, Typography, Form, Input } from 'antd'

import '../style/AddUser.css'
import { Frame, Button } from '../components/'
import { SignupUser } from '../models/User'
import { ApiClient } from '../api-client/ApiClient'

const { Content } = Layout
const { Title } = Typography
type SignupProps = {
  apiClient: ApiClient
}
type SignupState = SignupUser

export class Signup extends React.Component<SignupProps, SignupState> {
  public state: SignupState = {
    firstName: '',
    lastName: '',
    phoneNumber: '',
    password: '',
    hourlyRate: 0
  }

  private username = window.location.pathname.split('/').slice(-1)[0]

  private onChangeFirstName = (e: any) =>
    this.setState({ firstName: e.target.value })
  private onChangeLastName = (e: any) =>
    this.setState({ lastName: e.target.value })
  private onChangePhoneNumber = (e: any) =>
    this.setState({ phoneNumber: e.target.value })
  private onChangePassword = (e: any) =>
    this.setState({ password: e.target.value })
  private onChangeHourlyRate = (e: any) => {
    this.setState({ hourlyRate: parseInt(e.target.value) })
  }

  private handleClick = async () => {
    const signupuser: SignupUser = {
      password: this.state.password,
      firstName: this.state.firstName,
      lastName: this.state.lastName,
      phoneNumber: this.state.phoneNumber,
      hourlyRate: this.state.hourlyRate
    }

    await this.props.apiClient.signup(this.username, signupuser)

    const response = await this.props.apiClient.login({
      username: this.username,
      password: signupuser.password
    })

    localStorage.setItem('token', response.token)
    window.location.replace('/')
  }

  render() {
    const formItemLayout = {
      labelCol: {
        xs: { span: 24 },
        sm: { span: 6 }
      },
      wrapperCol: {
        xs: { span: 24 },
        sm: { span: 18 }
      }
    }

    return (
      <>
        <Content
          style={{
            backgroundColor: 'white',
            minHeight: 'auto'
          }}
        >
          <div className="site-layout-content container">
            <Frame>
              <div className="frame">
                <Title>Sign up</Title>
                <Form name="control-ref">
                  <Form.Item
                    name="username"
                    label="Username:"
                    {...formItemLayout}
                  >
                    <Input
                      type="text"
                      defaultValue={this.username}
                      className="form-control"
                      disabled
                    />
                  </Form.Item>
                  <Form.Item
                    name="password"
                    label="Password:"
                    {...formItemLayout}
                    rules={[
                      {
                        required: true,
                        message: "Can't be blank"
                      }
                    ]}
                  >
                    <Input.Password
                      type="text"
                      value={this.state.password}
                      onChange={this.onChangePassword}
                      className="form-control"
                    />
                  </Form.Item>
                  <Form.Item
                    name="firstName"
                    label="First name:"
                    {...formItemLayout}
                    rules={[
                      {
                        required: true,
                        message: "Can't be blank"
                      }
                    ]}
                  >
                    <Input
                      type="text"
                      value={this.state.firstName}
                      onChange={this.onChangeFirstName}
                      className="form-control"
                    />
                  </Form.Item>
                  <Form.Item
                    name="lastName"
                    label="Last name:"
                    {...formItemLayout}
                    rules={[
                      {
                        required: true,
                        message: "Can't be blank"
                      }
                    ]}
                  >
                    <Input
                      type="text"
                      value={this.state.lastName}
                      onChange={this.onChangeLastName}
                      className="form-control"
                    />
                  </Form.Item>
                  <Form.Item
                    name="phoneNumber"
                    label="Phone number:"
                    {...formItemLayout}
                    rules={[
                      {
                        required: true,
                        message: "Can't be blank",
                        type: 'number'
                      }
                    ]}
                  >
                    <Input
                      type="text"
                      value={this.state.phoneNumber}
                      onChange={this.onChangePhoneNumber}
                      className="form-control"
                    />
                  </Form.Item>
                  <Form.Item
                    name="hourlyrate"
                    label="Hourly Rate"
                    {...formItemLayout}
                    rules={[
                      {
                        required: true,
                        message: "Can't be blank",
                        type: 'number'
                      }
                    ]}
                  >
                    <Input
                      type="number"
                      value={this.state.hourlyRate}
                      onChange={this.onChangeHourlyRate}
                      className="form-control"
                    />
                  </Form.Item>
                  <Form.Item>
                    <Button onClick={this.handleClick}>Done</Button>
                  </Form.Item>
                </Form>
              </div>
            </Frame>
          </div>
        </Content>
      </>
    )
  }
}
