import React from 'react'
import { Layout, Space, Table, Typography } from 'antd'
import { Frame } from '../components'
import { ColumnsType } from 'antd/es/table'
import { Link } from 'react-router-dom'
import { ApiClient } from '../api-client/ApiClient'
import { GetProjectsType } from '../models/Project'
import { yellow, red, green } from '@ant-design/colors'

const { Title } = Typography
const { Content } = Layout

type TableData = GetProjectsType & {
  key: string
  link: JSX.Element
}

const columns: ColumnsType<TableData> = [
  {
    title: 'Project',
    dataIndex: 'name'
  },
  {
    title: 'Hours left today',
    render: (project: GetProjectsType) => {
      let color = yellow.primary

      if (project.currDailyHours / project.minDailyHours < 0.4) {
        color = red.primary
      } else if (project.currDailyHours / project.minDailyHours > 0.8) {
        color = green.primary
      }

      return (
        <span style={{ color }}>
          {project.currDailyHours} / {project.minDailyHours}
        </span>
      )
    }
  },
  {
    title: 'Hours left this week',
    render: (project: GetProjectsType) => {
      let color = yellow.primary

      if (project.currWeeklyHours / project.maxWeeklyHours < 0.4) {
        color = green.primary
      } else if (project.currWeeklyHours / project.maxWeeklyHours > 0.8) {
        color = red.primary
      }

      return (
        <span style={{ color }}>
          {project.currWeeklyHours} / {project.maxWeeklyHours}
        </span>
      )
    }
  },
  {
    title: 'Firm',
    dataIndex: 'firmName'
  },
  {
    title: 'Project manager',
    dataIndex: 'projectManager'
  },
  {
    title: 'Action',
    dataIndex: 'link'
  }
]

type Props = {
  apiClient: ApiClient
}

type State = {
  projects: GetProjectsType[]
}

export class MyProjects extends React.Component<Props, State> {
  public state = {
    projects: []
  }

  private async getProjects() {
    let projects = await this.props.apiClient.getProjects()

    const data: TableData[] = []
    projects.forEach(project => {
      data.push({
        key: project.id,
        link: <Link to={`/projectinfo?id=${project.id}`}>Info</Link>,
        ...project
      })
    })
    this.setState({ projects: data })
  }

  componentDidMount() {
    this.getProjects()
  }

  render() {
    return (
      <>
        <Content
          style={{
            backgroundColor: 'white',
            minHeight: 'auto'
          }}
        >
          <div className="site-layout-content">
            <div style={{ textAlign: 'center', minWidth: '65%' }}>
              <Frame>
                <Space direction="vertical">
                  <Title>My projects</Title>
                  <Table
                    pagination={false}
                    columns={columns}
                    dataSource={this.state.projects}
                  />
                </Space>
              </Frame>
            </div>
          </div>
        </Content>
      </>
    )
  }
}
