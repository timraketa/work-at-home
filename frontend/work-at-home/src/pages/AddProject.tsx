import React from 'react'
import {
  Layout,
  Typography,
  Form,
  Input,
  DatePicker,
  InputNumber,
  Checkbox,
  Space,
  Select,
  notification
} from 'antd'

import '../style/AddProject.css'
import { Frame, Button } from '../components/'
import { CreateProject } from '../models/Project'
import { ApiClient, UserFirm } from '../api-client/ApiClient'
const { Option } = Select
const { Content } = Layout
const { Title } = Typography

type AddProjectProps = {
  apiClient: ApiClient
}
type AddProjectState = {
  projectName: string
  dueDate: string
  minHoursDay: number
  maxHoursWeek: number
  budget: number
  description: string
  checkReminders: boolean
  firms: UserFirm[]
  selectedFirm: UserFirm
}

export class AddProject extends React.Component<
  AddProjectProps,
  AddProjectState
> {
  public state: AddProjectState = {
    projectName: '',
    dueDate: '',
    minHoursDay: 0,
    maxHoursWeek: 0,
    budget: 0,
    description: '',
    checkReminders: false,
    firms: [
      {
        firmId: '',
        firmName: '',
        firmEmail: '',
        roles: []
      }
    ],
    selectedFirm: {
      firmId: '',
      firmName: '',
      firmEmail: '',
      roles: []
    }
  }

  onChangeProjectName = (e: any) =>
    this.setState({ projectName: e.target.value })
  onChangeDueDate = (e: any) => this.setState({ dueDate: e._d })
  onChangeMinHoursDay = (e: any) => this.setState({ minHoursDay: e })
  onChangeMaxHoursWeek = (e: any) => this.setState({ maxHoursWeek: e })
  onChangeBudget = (e: any) => this.setState({ budget: e })
  onChangeDescription = (e: any) =>
    this.setState({ description: e.target.value })
  onChangeCheckReminders = (e: any) =>
    this.setState({ checkReminders: e.target.checked })
  handleClick = async () => {
    try {
      let firm = this.state.selectedFirm
      const project: CreateProject = {
        minDailyHours: this.state.budget,
        maxWeeklyHours: this.state.maxHoursWeek,
        reminderEnabled: this.state.checkReminders,
        name: this.state.projectName,
        firmId: firm.firmId,
        description: this.state.description,
        budget: this.state.budget,
        dueDate: this.state.dueDate
      }

      await this.props.apiClient.addProject(project)

      window.location.replace('/myprojects')
    } catch (e) {
      notification['error']({
        message: e.message?.includes('status code 40')
          ? 'You have to be a project manager to add a project'
          : 'Error adding Project.'
      })
    }
  }
  private async getFirms() {
    try {
      let response = await this.props.apiClient.getUserFirms()
      this.setState({ firms: response })
    } catch {
      notification['error']({
        message: 'Error while getting firms.'
      })
    }
  }
  componentDidMount() {
    this.getFirms()
  }
  fillStateFirm = (value: any): any => {
    this.setState({ selectedFirm: this.state.firms[value] })
  }

  render() {
    const formItemLayout = {
      labelCol: {
        xs: { span: 24 },
        sm: { span: 7 }
      },
      wrapperCol: {
        xs: { span: 24 },
        sm: { span: 17 }
      }
    }

    return (
      <>
        <Content
          style={{
            backgroundColor: 'white',
            minHeight: 'auto'
          }}
        >
          <div className="site-layout-content container">
            <Frame>
              <div className="frame">
                <Title>Add new project</Title>
                <Form name="control-ref">
                  <Form.Item
                    name="projectname"
                    label="Project name:"
                    {...formItemLayout}
                    rules={[
                      {
                        required: true,
                        message: "Can't be blank"
                      }
                    ]}
                  >
                    <Input
                      type="text"
                      value={this.state.projectName}
                      onChange={this.onChangeProjectName}
                      className="form-control"
                    />
                  </Form.Item>
                  <Form.Item
                    name="select"
                    label="Select firm:"
                    {...formItemLayout}
                    rules={[
                      {
                        required: true,
                        message: "Can't be blank"
                      }
                    ]}
                  >
                    <Select
                      onChange={this.fillStateFirm}
                      style={{ width: '100%' }}
                    >
                      {this.state.firms.map((firm, index) => {
                        return (
                          <Option key={firm.firmName} value={index}>
                            {firm.firmName}
                          </Option>
                        )
                      })}
                    </Select>
                  </Form.Item>
                  <Form.Item
                    label="Due date:"
                    {...formItemLayout}
                    rules={[
                      {
                        required: true,
                        message: "Can't be blank"
                      }
                    ]}
                  >
                    <DatePicker
                      dropdownClassName="datum"
                      placeholder="Due date"
                      style={{ width: '100%' }}
                      onChange={this.onChangeDueDate}
                    />
                  </Form.Item>
                  <Form.Item
                    label="Min hours per day: "
                    {...formItemLayout}
                    rules={[
                      {
                        required: true,
                        message: "Can't be blank"
                      }
                    ]}
                  >
                    <InputNumber
                      style={{ width: '100%' }}
                      min={1}
                      max={24}
                      defaultValue={1}
                      onChange={this.onChangeMinHoursDay}
                      name="minHoursDay"
                    />
                  </Form.Item>
                  <Form.Item
                    label="Max hours per week: "
                    {...formItemLayout}
                    rules={[
                      {
                        required: true,
                        message: "Can't be blank"
                      }
                    ]}
                  >
                    <InputNumber
                      style={{ width: '100%' }}
                      min={1}
                      max={168}
                      defaultValue={1}
                      onChange={this.onChangeMaxHoursWeek}
                      name="maxHoursWeek"
                    />
                  </Form.Item>
                  <Form.Item
                    label="Budget: "
                    {...formItemLayout}
                    rules={[
                      {
                        required: true,
                        message: "Can't be blank"
                      }
                    ]}
                  >
                    <InputNumber
                      style={{ width: '100%' }}
                      defaultValue={0}
                      min={0}
                      formatter={value => `${value}€`}
                      parser={(value = '€') => value.replace('€', '')}
                      onChange={this.onChangeBudget}
                    />
                  </Form.Item>
                  <Form.Item
                    label="Description: "
                    {...formItemLayout}
                    rules={[
                      {
                        required: true,
                        message: "Can't be blank"
                      }
                    ]}
                  >
                    <Input
                      type="text"
                      value={this.state.description}
                      onChange={this.onChangeDescription}
                      className="form-control"
                    />
                  </Form.Item>

                  <Form.Item>
                    <Checkbox onChange={this.onChangeCheckReminders}>
                      Enable reminders
                    </Checkbox>
                  </Form.Item>
                  <Form.Item>
                    <Space direction="horizontal">
                      <Button onClick={this.handleClick}>Done</Button>
                    </Space>
                  </Form.Item>
                </Form>
              </div>
            </Frame>
          </div>
        </Content>
      </>
    )
  }
}
