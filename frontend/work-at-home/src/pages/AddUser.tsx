import React from 'react'
import {
  Layout,
  Typography,
  Select,
  Form,
  Input,
  Button,
  Space,
  notification
} from 'antd'
import '../style/AddUser.css'
import { Frame } from '../components/'
import { AdminFirm, ApiClient } from '../api-client/ApiClient'
import { Role } from '../models/Role'
import { UserList } from '../components/UserList'
import { User } from '../models/User'

const { Content } = Layout
const { Title } = Typography
const { Option } = Select
const { Text } = Typography

type Props = {
  apiClient: ApiClient
}

type State = {
  username: string
  email: string
  allRoles: Role[]
  selectedRoles: number[]
  users: User[]
  loadingUsers: boolean
  canAddUsers: boolean
  firms: AdminFirm[]
  selectedFirm: AdminFirm
}

export class AddUser extends React.Component<Props, State> {
  public state: State = {
    username: '',
    email: '',
    allRoles: [],
    selectedRoles: [],
    users: [],
    loadingUsers: false,
    canAddUsers: false,
    firms: [],
    selectedFirm: {
      id: '',
      name: ''
    }
  }

  componentDidMount() {
    this.getRoles()
    this.getAdminFirms()
  }

  private async getRoles() {
    try {
      this.setState({
        allRoles: await this.props.apiClient.getRoles(),
        canAddUsers: true
      })
    } catch (e) {
      this.setState({
        canAddUsers: false
      })
      notification['error']({
        message: "You don't have permission to add users!"
      })
    }
  }

  private async getUsers(firmId: string) {
    this.setState({
      loadingUsers: true
    })

    this.setState({
      users: await this.props.apiClient.getUsers(firmId),
      loadingUsers: false
    })
  }

  onFirmSelectChange = (value: any): any => {
    this.setState({ selectedFirm: this.state.firms[value] })
    this.getUsers(this.state.firms[value].id)
  }

  private async getAdminFirms() {
    try {
      let response = await this.props.apiClient.getAdminFirms()
      this.setState({
        firms: response,
        selectedFirm: response.length ? response[0] : this.state.selectedFirm
      })

      if (response.length) {
        await this.getUsers(response[0].id)
      }
    } catch {
      notification['error']({
        message: 'Error while fetching data.'
      })
    }
  }

  private onChangeUsername = (e: any) =>
    this.setState({ username: e.target.value })
  private onChangeEmail = (e: any) => this.setState({ email: e.target.value })
  private onChangeRole = (value: number[]) => {
    this.setState({
      selectedRoles: value
    })
  }

  private roleOutput = (role: Role) => {
    return (
      <Option key={role.id} value={role.id}>
        {role.name}
      </Option>
    )
  }

  private handleClick = async () => {
    try {
      await this.props.apiClient.createUser({
        username: this.state.username,
        email: this.state.email,
        roles: this.state.selectedRoles,
        firmId: this.state.selectedFirm.id
      })

      await this.getUsers(this.state.selectedFirm.id)

      this.setState({
        username: '',
        email: '',
        selectedRoles: []
      })
    } catch (e) {
      notification['error']({
        message: 'Error while adding a user'
      })
    }
  }

  render() {
    const formItemLayout = {
      labelCol: {
        xs: { span: 24 },
        sm: { span: 6 }
      },
      wrapperCol: {
        xs: { span: 24 },
        sm: { span: 18 }
      }
    }
    return (
      <>
        <Content
          style={{
            backgroundColor: 'white',
            minHeight: 'auto'
          }}
        >
          <div className="container--row">
            <div>
              <label style={{ marginRight: '15px' }}>Choose a firm:</label>
              <Select
                defaultValue={0}
                onChange={this.onFirmSelectChange}
                style={{ maxWidth: '150px' }}
              >
                {this.state.firms.map((firm, index) => {
                  return (
                    <Option key={firm.name} value={index}>
                      {firm.name}
                    </Option>
                  )
                })}
              </Select>
              <Frame>
                <div className="frame frame--add-user">
                  <Title>Add new user</Title>
                  <Form name="control-ref">
                    <Form.Item
                      label="Username:"
                      {...formItemLayout}
                      rules={[
                        {
                          required: true,
                          message: "Can't be blank"
                        }
                      ]}
                    >
                      <Input
                        type="text"
                        value={this.state.username}
                        onChange={this.onChangeUsername}
                        disabled={!this.state.canAddUsers}
                      />
                    </Form.Item>

                    <Form.Item
                      label="Email:"
                      {...formItemLayout}
                      rules={[
                        {
                          required: true,
                          message: "Can't be blank",
                          type: 'email'
                        }
                      ]}
                    >
                      <Input
                        type="text"
                        value={this.state.email}
                        onChange={this.onChangeEmail}
                        className="form-control"
                        disabled={!this.state.canAddUsers}
                      />
                    </Form.Item>

                    <Form.Item
                      label="Role:"
                      {...formItemLayout}
                      rules={[
                        {
                          required: true,
                          message: "Can't be blank"
                        }
                      ]}
                    >
                      <Select
                        className="select"
                        placeholder="Select role"
                        onChange={this.onChangeRole}
                        mode="multiple"
                        value={this.state.selectedRoles}
                        disabled={!this.state.canAddUsers}
                      >
                        {this.state.allRoles.map(this.roleOutput)}
                      </Select>
                      <Text type="secondary" style={{ fontSize: '10px' }}>
                        By adding, the new user receives a confirmation email
                      </Text>
                    </Form.Item>
                    <Form.Item>
                      <Button
                        onClick={this.handleClick}
                        disabled={!this.state.canAddUsers}
                      >
                        Done
                      </Button>
                    </Form.Item>
                  </Form>
                </div>
              </Frame>
            </div>
            <div
              style={{ textAlign: 'center', maxWidth: '65%', overflow: 'auto' }}
            >
              <Frame>
                <Space direction="vertical">
                  <Title>List of Users</Title>
                  <UserList
                    data={this.state.users.map(user => {
                      return {
                        ...user,
                        key: user.id
                      }
                    })}
                    selectEnabled={false}
                    loading={this.state.loadingUsers}
                  />
                </Space>
              </Frame>
            </div>
          </div>
        </Content>
      </>
    )
  }
}
