import React from 'react'
import {
  Layout,
  Calendar,
  Typography,
  Space,
  Badge,
  Button as Btn,
  Input,
  Select,
  List
} from 'antd'
import { Frame } from '../components/'
import { CloseOutlined } from '@ant-design/icons'
import '../style/CalendarView.css'
import { ApiClient } from '../api-client/ApiClient'
import Modal from 'antd/lib/modal/Modal'
import { GetProjectsType } from '../models/Project'

export type WorkDayData = {
  type: 'warning' | 'success' | 'processing' | 'error' | 'default' | undefined
  content: string
  id: string
  workHourId: string
}
const { Content } = Layout
const { Title } = Typography
const { Option } = Select
type CalendarProps = {
  apiClient: ApiClient
}
type CalendarState = {
  id: string
  selected: string
  projects: GetProjectsType[]
  listData: { [k: string]: WorkDayData[] }
  isModalVisible: boolean
  WorkHourDescription: string
  selectedProject: string
}
export class CalendarView extends React.Component<
  CalendarProps,
  CalendarState
> {
  public state: CalendarState = {
    id: '',
    selected: '',
    listData: {
      '-1': [{ type: 'error', content: 'prazno', id: '', workHourId: '' }]
    },
    isModalVisible: false,
    WorkHourDescription: '',
    projects: [],
    selectedProject: ''
  }
  onSelect = (e: any) => {
    let day = e._d.getDate()
    let day1: string = ''
    let month1: string = ''
    day < 10 ? (day1 = '0' + day) : (day1 = day)
    let month = e._d.getMonth() + 1
    month < 10 ? (month1 = '0' + month) : (month1 = month)
    let date = e._d.getFullYear() + '-' + month1 + '-' + day1
    let selectedDate = this.state.selected.split('-')
    if (
      date.split('-')[1] === selectedDate[1] &&
      date.split('-')[0] === selectedDate[0]
    ) {
      this.setState({ isModalVisible: true })
    }
    this.setState({ selected: new Date(date).toISOString() })
  }

  private async getWorkday() {
    let res = await this.props.apiClient.getWorkDays()
    let listData: { [k: string]: WorkDayData[] } = {}

    res.forEach((workday, index) => {
      let polje: WorkDayData[] = []
      if (workday.workHours !== undefined) {
        workday.workHours.forEach(workhour => {
          polje.push({
            type: 'success',
            content: workhour.description,
            id: workday.id,
            workHourId: workhour.id
          })
        })
      }
      if (polje.length > 0) {
        let date = new Date(workday.date)
        date.setDate(date.getDate() + 1)
        let day = date.toISOString().slice(0, 10)

        //let day = workday.date.slice(0,10).split("-")
        listData[day] = polje
      }
    })

    this.setState({ listData })
  }
  handleOkHandler = () => {
    this.setState({ isModalVisible: false })
  }
  private async getProjects() {
    let res = await this.props.apiClient.getProjects()

    this.setState({ projects: res, selectedProject: res[0].id })
  }
  componentDidMount() {
    this.getWorkday()
    this.getProjects()
    let date = new Date()

    let day: string = date.getDate().toString()
    let day1: string = ''
    let month1: string = ''
    Number(day) < 10 ? (day1 = '0' + day) : (day1 = day)
    let month: string = (date.getMonth() + 1).toString()
    Number(month) < 10 ? (month1 = '0' + month) : (month1 = month)
    let date1 = date.getFullYear() + '-' + month1 + '-' + day1
    this.setState({ selected: new Date(date1).toISOString() })
  }

  onChangeWorkHour = (e: any) => {
    this.setState({ WorkHourDescription: e.target.value })
  }

  addWorkHour = async () => {
    let day = this.state.selected.slice(0, 10)
    let days = JSON.parse(JSON.stringify(this.state.listData))
    let workhour: {
      description: string
      workDayId: string
      projectId: string
    } = { description: '', workDayId: '', projectId: '' }

    if (day in this.state.listData) {
      workhour = {
        description: this.state.WorkHourDescription,
        workDayId: this.state.listData[day][0].id,
        projectId: this.state.selectedProject
      }
    } else {
      const workDay = {
        isFree: false,
        date: this.state.selected,
        userId: window.location.search.split('=')[1]
      }

      let workday = await this.props.apiClient.addWorkDay(workDay)

      workhour = {
        description: this.state.WorkHourDescription,
        workDayId: workday.id,
        projectId: this.state.selectedProject
      }
      days[day] = []
    }
    let res = await this.props.apiClient.addWorkHour(
      workhour.workDayId,
      workhour
    )

    let save = {
      type: 'success',
      content: res.description,
      id: workhour.workDayId,
      workHourId: res.id
    }
    days[day].push(save)
    this.setState({ WorkHourDescription: '', listData: days })
  }

  async deleteHour(
    workDayId: string,
    workHourId: string,
    index: number,
    day: any
  ) {
    this.props.apiClient.deleteWorkHour(workDayId, { ids: [workHourId] })
    let arr = JSON.parse(JSON.stringify(this.state.listData))

    arr[day].splice(index, 1)
    if (arr[day].length === 0) {
      await this.props.apiClient.deleteWorkDay({ ids: [workDayId] })
    }

    this.setState({ listData: arr })
  }

  dateCellRender = (value: any) => {
    let day = value.toISOString().slice(0, 10)
    let listData = this.state.listData[day]
    if (listData !== undefined && listData.length > 0) {
      return (
        <ul className="events" style={{ listStyle: 'none' }}>
          {listData.map((item, index) => (
            <li key={item.content}>
              <Space>
                <Badge status={item.type} text={item.content} />
                <CloseOutlined
                  onClick={e => {
                    this.setState({ isModalVisible: false })
                    e.preventDefault()
                    e.stopPropagation()
                    this.deleteHour(item.id, item.workHourId, index, day)
                  }}
                  style={{ color: 'red', cursor: 'pointer' }}
                />
              </Space>
            </li>
          ))}
        </ul>
      )
    } else {
      return <ul></ul>
    }
  }
  fillStateProject = (value: any): any => {
    this.setState({ selectedProject: this.state.projects[value].id })
  }
  deleteWorkDayBtn = async () => {
    let dan = this.state.selected.slice(0, 10)

    if (dan in this.state.listData) {
      await this.props.apiClient.deleteWorkDay({
        ids: [this.state.listData[dan][0].id]
      })

      let polje = JSON.parse(JSON.stringify(this.state.listData))
      delete polje[dan]
      this.setState({ listData: polje })
    }
  }

  render() {
    return (
      <>
        <Content
          style={{
            backgroundColor: 'white',
            minHeight: 'auto',
            display: 'flex',
            justifyContent: 'center',
            alignItems: 'center',
            overflow: ''
          }}
        >
          <div className="site-layout-content">
            <div
              className="site-calendar center"
              style={{ flexBasis: 'unset' }}
            >
              <Frame>
                <div className="cont">
                  <Space direction="vertical">
                    <Title>Calendar</Title>
                    <Modal
                      style={{ overflow: 'auto', minWidth: '450px' }}
                      visible={this.state.isModalVisible}
                      width="18%"
                      title={'Work day'}
                      closable={false}
                      footer={[
                        <Btn
                          key={4}
                          type="primary"
                          onClick={this.deleteWorkDayBtn}
                          danger
                          size="large"
                        >
                          Delete day
                        </Btn>,
                        <Btn
                          key="back"
                          onClick={this.handleOkHandler}
                          size={'large'}
                          type="primary"
                        >
                          Done
                        </Btn>
                      ]}
                    >
                      {
                        <>
                          <List>
                            <List.Item key={0}>
                              <Input
                                onChange={this.onChangeWorkHour}
                                value={this.state.WorkHourDescription}
                                placeholder={'Work hour description...'}
                              />
                            </List.Item>
                            <List.Item key={1}>
                              {' '}
                              <Select
                                defaultValue={0}
                                onChange={this.fillStateProject}
                                style={{ width: '100%' }}
                              >
                                {this.state.projects.map((project, index) => {
                                  return (
                                    <Option key={project.name} value={index}>
                                      {project.name}
                                    </Option>
                                  )
                                })}
                              </Select>
                            </List.Item>

                            <List.Item
                              key={2}
                              style={{
                                display: 'flex',
                                justifyContent: 'flex-end'
                              }}
                            >
                              <Space>
                                <Btn
                                  type="primary"
                                  ghost={true}
                                  onClick={this.addWorkHour}
                                >
                                  Add
                                </Btn>
                              </Space>
                            </List.Item>
                          </List>
                        </>
                      }
                    </Modal>
                    <div className="Wrapper2">
                      <Calendar
                        onSelect={this.onSelect}
                        dateCellRender={this.dateCellRender}
                      />
                    </div>
                  </Space>
                </div>
              </Frame>
            </div>
          </div>
        </Content>
      </>
    )
  }
}
