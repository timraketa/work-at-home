import React from 'react'
import { Layout, Typography, Form, Input, Button, notification } from 'antd'

import '../style/AddUser.css'
import { Frame } from '../components/'
import { LoginUser } from '../models/User'
import { ApiClient } from '../api-client/ApiClient'

const { Content } = Layout
const { Title } = Typography
type LoginProps = {
  apiClient: ApiClient
  onAuthSuccess: () => void
  logout: () => void
}

type LoginState = LoginUser & {
  wrongCredentials: boolean
}

export class Login extends React.Component<LoginProps, LoginState> {
  public state: LoginState = {
    username: '',
    password: '',
    wrongCredentials: false
  }

  private onChangeUsername = (e: any) =>
    this.setState({ username: e.target.value })
  private onChangePassword = (e: any) =>
    this.setState({ password: e.target.value })
  private handleClick = async () => {
    try {
      const loginUser: LoginUser = {
        username: this.state.username,
        password: this.state.password
      }

      const response = await this.props.apiClient.login(loginUser)

      localStorage.setItem('token', response.token)

      this.props.onAuthSuccess()

      window.location.replace('/')
    } catch (e) {
      this.setState({
        wrongCredentials: true
      })
      notification['error']({
        message: 'Wrong credentials!'
      })
    }
  }

  componentDidMount() {
    this.props.logout()
  }

  render() {
    const formItemLayout = {
      labelCol: {
        xs: { span: 24 },
        sm: { span: 6 }
      },
      wrapperCol: {
        xs: { span: 24 },
        sm: { span: 18 }
      }
    }
    return (
      <>
        <Content
          style={{
            backgroundColor: 'white',
            minHeight: 'auto'
          }}
        >
          <div className="site-layout-content container">
            <Frame>
              <div className="frame">
                <Title>Log in</Title>
                <Form name="control-ref">
                  <Form.Item
                    name="username"
                    label="Username:"
                    {...formItemLayout}
                    validateStatus={this.state.wrongCredentials ? 'error' : ''}
                  >
                    <Input
                      type="text"
                      value={this.state.username}
                      onChange={this.onChangeUsername}
                      className="form-control"
                    />
                  </Form.Item>
                  <Form.Item
                    name="password"
                    label="Password:"
                    {...formItemLayout}
                    validateStatus={this.state.wrongCredentials ? 'error' : ''}
                  >
                    <Input.Password
                      type="text"
                      value={this.state.password}
                      onChange={this.onChangePassword}
                      className="form-control"
                    />
                  </Form.Item>
                  <Form.Item>
                    <Button onClick={this.handleClick}>Done</Button>
                  </Form.Item>
                </Form>
              </div>
            </Frame>
          </div>
        </Content>
      </>
    )
  }
}
