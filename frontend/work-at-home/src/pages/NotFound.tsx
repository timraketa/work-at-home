import { Layout } from 'antd'
import React from 'react'

const { Content } = Layout

export class NotFound extends React.Component {
  render() {
    if (window.location.pathname.includes('signup')) {
      return (
        <>
          <Content
            style={{
              padding: '0 50px',
              backgroundColor: 'white',
              minHeight: 'auto'
            }}
          >
            <div style={{ position: 'absolute', top: '48%', left: '42%' }}>
              Signup has to includes username in the URL!
              <div>
                For example:{' '}
                <a href="http://localhost:3001/signup/newUsername">
                  /signup/newUsername
                </a>
              </div>
            </div>
          </Content>
        </>
      )
    }
    return (
      <Content
        style={{
          padding: '0 50px',
          backgroundColor: 'white',
          minHeight: 'auto'
        }}
      >
        <div className="site-layout-content">Page not found!</div>
      </Content>
    )
  }
}
