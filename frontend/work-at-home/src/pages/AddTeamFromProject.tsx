import React from 'react'
import {
  Layout,
  Typography,
  Form,
  Input,
  Space,
  Button,
  notification
} from 'antd'
import { Link } from 'react-router-dom'
import '../style/AddUser.css'
import { Frame, CustomModal, UserListWrapper } from '../components/'
import { ApiClient } from '../api-client/ApiClient'
import { CreateTeam } from '../models/Team'
import { User } from '../models/User'

const { TextArea } = Input
const { Content } = Layout
const { Title } = Typography

type Props = {
  apiClient: ApiClient
  location?: any
}

type State = {
  teamName: string
  description: string
  users: string[]
  teamLeadId: string
  teamLeadName: string
  projectId: string
  stage: boolean
  data: User[]
}

export class AddTeamFromProject extends React.Component<Props, State> {
  public state: State = {
    teamName: '',
    description: '',
    users: [],
    teamLeadId: '',
    teamLeadName: '',
    projectId: '',
    stage: true,
    data: []
  }

  onChangeTeamName = (e: any) => this.setState({ teamName: e.target.value })
  onChangeDescription = (e: any) =>
    this.setState({ description: e.target.defaultValue })
  pickTeamLeadHandler = () => {}
  handleClick = async () => {
    try {
      const team: CreateTeam = {
        teamLeadId: this.state.teamLeadId,
        name: this.state.teamName,
        description: this.state.description,
        projectId: this.state.projectId
      }

      const response = await this.props.apiClient.addTeam(
        this.state.projectId,
        team
      )

      await this.props.apiClient.addTeamMembers(response.id, {
        ids: this.state.users
      })
    } catch (e) {
      notification['error']({
        message: "Couldn't fetch data."
      })
    }
  }
  private async getUsers() {
    let search = new URLSearchParams(window.location.search)
    let firmId = search.get('frid')

    const users: User[] = await this.props.apiClient.getUsers(firmId || '')
    this.setState({ data: users, projectId: search.get('prid') || '' })
  }
  componentDidMount() {
    this.getUsers()
  }
  updateMembers = (users: string[]) => {
    this.setState({ users })
  }
  chooseTeamLead = (teamLeadId: any) => {
    const index = this.state.data.map(user => user.id).indexOf(teamLeadId[0])

    this.setState({
      teamLeadId: teamLeadId[0],
      teamLeadName: `${this.state.data[index].firstName} ${this.state.data[index].lastName}`
    })
  }
  render() {
    const formItemLayout = {
      labelCol: {
        xs: { span: 24 },
        sm: { span: 6 }
      },
      wrapperCol: {
        xs: { span: 24 },
        sm: { span: 18 }
      }
    }
    {
      let data1: User[] = []
      this.state.data.forEach((value, index) => {
        if (value.id !== this.state.teamLeadId) {
          data1.push(value)
        }
      })

      let data = (
        <UserListWrapper
          callback={this.chooseTeamLead}
          data={this.state.data.filter(
            user => user.firmRoles && user.firmRoles.includes('Team Lead')
          )}
          type={'radio'}
          selectEnabled={true}
        />
      )

      return (
        <>
          <Content
            style={{
              backgroundColor: 'white',
              minHeight: 'auto'
            }}
          >
            <div className="container--row">
              <Frame>
                <div className="frame frame--add-user">
                  <Title>Add new team</Title>
                  <Form name="control-ref">
                    <Form.Item
                      name="teamname"
                      label="Team name:"
                      {...formItemLayout}
                    >
                      <Input
                        type="text"
                        value={this.state.teamName}
                        onChange={this.onChangeTeamName}
                        className="form-control"
                      />
                    </Form.Item>
                    <Form.Item label="Description:" {...formItemLayout}>
                      <TextArea
                        showCount
                        maxLength={100}
                        autoSize={{ maxRows: 2 }}
                        onChange={this.onChangeDescription}
                      />
                    </Form.Item>
                    <Form.Item label="Manager:" {...formItemLayout}>
                      <Input
                        readOnly
                        maxLength={100}
                        onChange={this.onChangeDescription}
                        value={this.state.teamLeadName}
                      />
                    </Form.Item>
                    <Form.Item>
                      <Space direction="horizontal">
                        <CustomModal
                          data={data}
                          title={'Pick team lead'}
                          titleButton={'Add team lead'}
                          callback={() => {}}
                          selection=""
                        ></CustomModal>
                        <Link to={`/projectinfo?id=${this.state.projectId}`}>
                          <Button onClick={this.handleClick}>Done</Button>
                        </Link>
                      </Space>
                    </Form.Item>
                  </Form>
                </div>
              </Frame>

              <Frame>
                <Space direction="vertical">
                  <UserListWrapper
                    callback={this.updateMembers}
                    data={data1}
                    type={'checkbox'}
                    selectEnabled={true}
                  />
                </Space>
              </Frame>
            </div>
          </Content>
        </>
      )
    }
  }
}
