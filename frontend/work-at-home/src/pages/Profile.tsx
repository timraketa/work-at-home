import React from 'react'
import {
  Layout,
  Form,
  Avatar,
  Typography,
  Space,
  DatePicker,
  Select,
  List,
  notification,
  Button,
  Input
} from 'antd'
import { Link } from 'react-router-dom'
import {
  AntDesignOutlined,
  CloseOutlined,
  EditOutlined
} from '@ant-design/icons'
import {
  UndividedUserWorkHoursStats,
  UserWorkHoursBarGraph
} from '../components'
import { UserFirm, ApiClient } from '../api-client/ApiClient'
import { UserStatsRequest } from '../models/Statistics'
import { User } from '../models/User'

const { RangePicker } = DatePicker
const { Title, Text } = Typography
const { Option } = Select

const { Content } = Layout
const layout = {
  labelCol: { span: 5 },
  wrapperCol: { span: 15 }
}
type State = {
  user: User
  userStatsRequest: UserStatsRequest
  firms: UserFirm[]
  selectedFirm: UserFirm
  isEditing: boolean
  editings: {
    hourlyRate: boolean
    firstName: boolean
    lastName: boolean
    phoneNumber: boolean
  }
  updatedField: string
}

type Props = {
  apiClient: ApiClient
}

export class Profile extends React.Component<Props, State> {
  public state: State = {
    user: {
      username: '',
      id: '',
      isRegistered: false
    },
    userStatsRequest: {
      from: new Date().toISOString(),
      to: new Date().toISOString()
    },
    firms: [],
    selectedFirm: {
      firmId: '',
      firmName: '',
      firmEmail: '',
      roles: []
    },
    updatedField: '',
    isEditing: false,
    editings: {
      hourlyRate: false,
      firstName: false,
      lastName: false,
      phoneNumber: false
    }
  }

  onChangeDateRange = (value: any) => {
    let startDate: Date = value[0]._d
    let endDate: Date = value[1]._d
    this.setState({
      userStatsRequest: {
        from: startDate.toISOString(),
        to: endDate.toISOString()
      }
    })
  }

  onFirmSelectChange = (value: any): any => {
    this.setState({ selectedFirm: this.state.firms[value] })
  }

  componentDidMount() {
    this.getUserInfo()
    this.getFirms()
  }

  render() {
    return (
      <>
        <Content
          style={{
            padding: '0 50px',
            backgroundColor: 'white',
            minHeight: 'auto'
          }}
        >
          <div
            className="site-layout-content"
            style={{ alignItems: 'flex-start' }}
          >
            <div style={{ position: 'sticky', top: '0px', flexBasis: '40%' }}>
              <Space>
                <Avatar
                  size={{ xs: 24, sm: 32, md: 40, lg: 64, xl: 80, xxl: 100 }}
                  icon={<AntDesignOutlined />}
                  style={{ margin: '20px' }}
                />
                <Space direction="vertical" size={-10}>
                  <Title>My profile</Title>
                  <Title level={5}>
                    <List
                      header={'Roles'}
                      bordered={false}
                      dataSource={this.state.selectedFirm.roles}
                      renderItem={item => <List.Item>{item}</List.Item>}
                    />
                  </Title>
                </Space>
              </Space>
              <Form labelAlign="left" {...layout} name="nest-messages">
                <Form.Item name={['user', 'firmname']} label="Select firm">
                  <span className="ant-form-text">
                    <Select
                      defaultValue={0}
                      onChange={this.onFirmSelectChange}
                      style={{ maxWidth: '150px' }}
                    >
                      {this.state.firms.map((firm, index) => {
                        return (
                          <Option key={firm.firmName} value={index}>
                            {firm.firmName}
                          </Option>
                        )
                      })}
                    </Select>
                  </span>
                </Form.Item>
                <Form.Item name={['user', 'firstname']} label="First name">
                  <div className="info-field-wrapper">
                    {this.state.editings.firstName ? (
                      <Input
                        type="text"
                        value={this.state.updatedField}
                        onChange={e => {
                          this.setState({
                            updatedField: e.target.value
                          })
                        }}
                      />
                    ) : (
                      <Text>{this.state.user.firstName}</Text>
                    )}
                    <div className="editing-button--wrapper">
                      <EditOutlined
                        onClick={() => this.handleEditClick('firstName')}
                      />
                      <Button
                        disabled={!this.state.editings.firstName}
                        onClick={() => this.handleSaveClick('firstName')}
                      >
                        Save
                      </Button>
                      <CloseOutlined
                        onClick={() => {
                          this.setState({
                            isEditing: false,
                            editings: {
                              firstName: false,
                              lastName: false,
                              hourlyRate: false,
                              phoneNumber: false
                            }
                          })
                        }}
                      />
                    </div>
                  </div>
                </Form.Item>
                <Form.Item name={['user', 'lastname']} label="Last name">
                  <div className="info-field-wrapper">
                    {this.state.editings.lastName ? (
                      <Input
                        type="text"
                        value={this.state.updatedField}
                        onChange={e => {
                          this.setState({
                            updatedField: e.target.value
                          })
                        }}
                      />
                    ) : (
                      <Text>{this.state.user.lastName}</Text>
                    )}
                    <div className="editing-button--wrapper">
                      <EditOutlined
                        onClick={() => this.handleEditClick('lastName')}
                      />
                      <Button
                        disabled={!this.state.editings.lastName}
                        onClick={() => this.handleSaveClick('lastName')}
                      >
                        Save
                      </Button>
                      <CloseOutlined
                        onClick={() => {
                          this.setState({
                            isEditing: false,
                            editings: {
                              firstName: false,
                              lastName: false,
                              hourlyRate: false,
                              phoneNumber: false
                            }
                          })
                        }}
                      />
                    </div>
                  </div>
                </Form.Item>
                <Form.Item name={['user', 'email']} label="Email">
                  <Text>{this.state.selectedFirm.firmEmail}</Text>
                </Form.Item>
                <Form.Item name={['user', 'hourlyRate']} label="Hourly Rate">
                  <div className="info-field-wrapper">
                    {this.state.editings.hourlyRate ? (
                      <Input
                        type="number"
                        value={this.state.updatedField}
                        onChange={e => {
                          this.setState({
                            updatedField: e.target.value
                          })
                        }}
                      />
                    ) : (
                      <Text>{this.state.user.hourlyRate}</Text>
                    )}
                    <div className="editing-button--wrapper">
                      <EditOutlined
                        onClick={() => this.handleEditClick('hourlyRate')}
                      />
                      <Button
                        disabled={!this.state.editings.hourlyRate}
                        onClick={() => this.handleSaveClick('hourlyRate')}
                      >
                        Save
                      </Button>
                      <CloseOutlined
                        onClick={() => {
                          this.setState({
                            isEditing: false,
                            editings: {
                              firstName: false,
                              lastName: false,
                              hourlyRate: false,
                              phoneNumber: false
                            }
                          })
                        }}
                      />
                    </div>
                  </div>
                </Form.Item>
                <Form.Item name={['user', 'phoneNumber']} label="Phone">
                  <div className="info-field-wrapper">
                    {this.state.editings.phoneNumber ? (
                      <Input
                        type="text"
                        value={this.state.updatedField}
                        onChange={e => {
                          this.setState({
                            updatedField: e.target.value
                          })
                        }}
                      />
                    ) : (
                      <Text>{this.state.user.phoneNumber}</Text>
                    )}
                    <div className="editing-button--wrapper">
                      <EditOutlined
                        onClick={() => this.handleEditClick('phoneNumber')}
                      />
                      <Button
                        disabled={!this.state.editings.phoneNumber}
                        onClick={() => this.handleSaveClick('phoneNumber')}
                      >
                        Save
                      </Button>
                      <CloseOutlined
                        onClick={() => {
                          this.setState({
                            isEditing: false,
                            editings: {
                              firstName: false,
                              lastName: false,
                              hourlyRate: false,
                              phoneNumber: false
                            }
                          })
                        }}
                      />
                    </div>
                  </div>
                </Form.Item>
                <Form.Item name={['user', 'about']}>
                  <Space direction="vertical">
                    <Link to="/calendarview">
                      <Text type="secondary">My calendar</Text>
                    </Link>
                    <Link to="/myprojects">
                      <Text type="secondary">My projects</Text>
                    </Link>
                  </Space>
                </Form.Item>
                <Form.Item name={['user', 'stats']} label="Statistics">
                  <Space direction="vertical" size={12}>
                    <RangePicker onChange={this.onChangeDateRange} />
                  </Space>
                </Form.Item>
              </Form>
            </div>
            <div className="statistics--container">
              <h2 style={{ minWidth: '100%' }}>Statistics:</h2>
              <label>Work hours:</label>
              <UndividedUserWorkHoursStats
                apiClient={this.props.apiClient}
                userStatsRequest={this.state.userStatsRequest}
              />
              <label>Work hours by project:</label>
              <UserWorkHoursBarGraph
                apiClient={this.props.apiClient}
                userStatsRequest={this.state.userStatsRequest}
              />
            </div>
          </div>
        </Content>
      </>
    )
  }

  private handleEditClick(fieldName: string) {
    if (this.state.isEditing) {
      return
    }

    const editings = {
      hourlyRate: false,
      firstName: false,
      lastName: false,
      phoneNumber: false
    }
    // @ts-ignore
    editings[fieldName] = true

    this.setState({
      isEditing: true,
      editings,
      // @ts-ignore
      updatedField: this.state.user[fieldName]
    })
  }

  private async handleSaveClick(fieldName: string) {
    const user = this.state.user
    // @ts-ignore
    user[fieldName] =
      fieldName === 'hourlyRate'
        ? parseInt(this.state.updatedField)
        : this.state.updatedField

    try {
      await this.props.apiClient.updateUser(user)
      this.setState({
        isEditing: false,
        editings: {
          hourlyRate: false,
          firstName: false,
          phoneNumber: false,
          lastName: false
        }
      })
      this.getUserInfo()
    } catch {
      notification['error']({
        message: 'An error accoured while updating the user'
      })
    }
  }

  private async getFirms() {
    try {
      let response = await this.props.apiClient.getUserFirms()
      this.setState({
        firms: response,
        selectedFirm: response.length ? response[0] : this.state.selectedFirm
      })
    } catch {
      notification['error']({
        message: 'Error while getting firms.'
      })
    }
  }

  private async getUserInfo() {
    try {
      let user = await this.props.apiClient.getUser()

      this.setState({
        user
      })
    } catch {
      notification['error']({
        message: 'Error while getting user info.'
      })
    }
  }
}
