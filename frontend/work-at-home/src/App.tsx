import React, { Component } from 'react'
import { BrowserRouter as Router, Route, Switch, Link } from 'react-router-dom'
import {
  HomePage,
  AddUser,
  Login,
  Signup,
  AddProject,
  AddTeamFromProject,
  CalendarView,
  Colleagues,
  MyProjects,
  Profile,
  ProjectInfo,
  NotFound
} from './pages'
import { Button, Divider, Layout, Menu } from 'antd'
import { ApiClient } from './api-client/ApiClient'

import './style/App.css'
import { AppLogo } from './components/AppLogo'

const { Header, Footer } = Layout

type Props = {
  apiClient: ApiClient
}

type State = {
  isAuthenticated: boolean
}

export default class App extends Component<Props, State> {
  public state: State = {
    isAuthenticated: false
  }

  componentDidMount() {
    if (localStorage.getItem('token')) {
      this.setState({
        isAuthenticated: true
      })
    }
  }

  render() {
    return (
      <Router>
        <Layout className="layout" style={{ height: '105vh' }}>
          <Header>
            <Menu theme="dark" mode="horizontal" selectedKeys={[]}>
              <Menu.Item key="app-logo">
                <AppLogo />
                <Divider type="vertical" />
              </Menu.Item>
              <Menu.Item key="1">
                <Link to="/">Home Page</Link>
              </Menu.Item>
              <Menu.Item key="2">
                <Link to="/adduser">Add Users</Link>
              </Menu.Item>
              <Menu.Item key="3">
                <Link to="/addproject">Add Project</Link>
              </Menu.Item>
              <Menu.Item key="4">
                <Link to="/myprojects">My Projects</Link>
              </Menu.Item>
              <Menu.Item key="5">
                <Link to="/colleagues">Colleagues</Link>
              </Menu.Item>
              <Menu.Item key="6">
                <Link to="/calendar">Calendar</Link>
              </Menu.Item>
              <Menu.Item key="7">
                <Link to="/profile">Profile</Link>
              </Menu.Item>
              {this.state.isAuthenticated ? (
                <Menu.Item key="logout">
                  <Button onClick={this.handleLogoutClick}>Logout</Button>
                </Menu.Item>
              ) : (
                <></>
              )}

              <Menu.Item key="8">
                <div
                  style={{
                    display: this.state.isAuthenticated ? 'none' : 'block'
                  }}
                >
                  <Link to="/login">
                    <span style={{ color: 'rgba(255, 255, 255, 0.65)' }}>
                      Login
                    </span>
                  </Link>
                </div>
              </Menu.Item>
              <Menu.Item key="9">
                <div
                  style={{
                    display: this.state.isAuthenticated ? 'none' : 'block'
                  }}
                >
                  <Link to="/signup">
                    <span style={{ color: 'rgba(255, 255, 255, 0.65)' }}>
                      Signup
                    </span>
                  </Link>
                </div>
              </Menu.Item>
            </Menu>
          </Header>
          <Switch>
            <Route
              exact
              path="/"
              render={() => <HomePage apiClient={this.props.apiClient} />}
            />
            <Route
              exact
              path="/adduser"
              render={() => <AddUser apiClient={this.props.apiClient} />}
            />
            <Route
              exact
              path="/login"
              render={() => (
                <Login
                  apiClient={this.props.apiClient}
                  onAuthSuccess={this.onAuthSuccess}
                  logout={this.logout}
                />
              )}
            />
            <Route
              exact
              path="/signup/:username"
              render={() => <Signup apiClient={this.props.apiClient} />}
            />
            <Route
              exact
              path="/addproject"
              render={() => <AddProject apiClient={this.props.apiClient} />}
            />
            <Route
              exact
              path="/calendar"
              render={() => <CalendarView apiClient={this.props.apiClient} />}
            />
            <Route
              exact
              path="/colleagues"
              render={() => <Colleagues apiClient={this.props.apiClient} />}
            />
            <Route
              exact
              path="/myprojects"
              render={() => <MyProjects apiClient={this.props.apiClient} />}
            />
            <Route
              exact
              path="/profile"
              render={() => <Profile apiClient={this.props.apiClient} />}
            />
            <Route
              exact
              path="/projectinfo"
              render={() => <ProjectInfo apiClient={this.props.apiClient} />}
            />
            <Route
              exact
              path="/addteamfromproject"
              render={props => (
                <AddTeamFromProject
                  {...props}
                  apiClient={this.props.apiClient}
                />
              )}
            />
            <Route path="*" render={() => <NotFound />} />
          </Switch>
          <Footer
            style={{
              textAlign: 'center',
              backgroundColor: '#001529',
              color: 'white'
            }}
          >
            <AppLogo />
            <div>
              Made by:{' '}
              <i>
                Mirko Džaja, Karlo Dinčir, Dino Nazlić, Robert Rastovski, Karla
                Baričević, Ljudevit Jelečević and Toma Puljak
              </i>
              <br />
              for the purposes of a 2020./2021. course at FER, Zagreb.
            </div>
          </Footer>
        </Layout>
      </Router>
    )
  }

  private onAuthSuccess = () => {
    this.setState({
      isAuthenticated: true
    })
  }

  private handleLogoutClick = () => {
    this.logout()

    window.location.replace('/')
  }

  private logout = () => {
    localStorage.clear()

    this.setState({
      isAuthenticated: false
    })
  }
}
