#!/bin/bash
set -e

psql -v ON_ERROR_STOP=1 --username "user" --dbname "postgres" <<-EOSQL
	DROP DATABASE IF EXISTS "work-at-home";
	CREATE DATABASE "work-at-home";

	GRANT ALL PRIVILEGES ON DATABASE "work-at-home" TO "user";
	
	SET TIMEZONE='UTC';

	\c work-at-home;

	CREATE EXTENSION IF NOT EXISTS "uuid-ossp";

	CREATE TABLE "user" (
		"id" uuid DEFAULT uuid_generate_v4() NOT NULL,
		"username" varchar(255) NOT NULL UNIQUE,
		"password" varchar(255),
		"first_name" varchar(255),
		"last_name" varchar(255),
		"hourly_rate" numeric,
		"phone_number" varchar(255),
		"is_registered" boolean DEFAULT false NOT NULL,
		CONSTRAINT "user_pk" PRIMARY KEY ("id")
	) WITH (
	OIDS=FALSE
	);

	CREATE TABLE "role" (
		"id" bigint NOT NULL,
		"name" varchar(255) NOT NULL,
		CONSTRAINT "role_pk" PRIMARY KEY ("id")
	) WITH (
	OIDS=FALSE
	);

	CREATE TABLE "work_hour" (
		"id" uuid DEFAULT uuid_generate_v4() NOT NULL,
		"description" varchar(255) NOT NULL,
		"work_day_id" uuid NOT NULL,
		"project_id" uuid NOT NULL,
		CONSTRAINT "work_hour_pk" PRIMARY KEY ("id")
	) WITH (
	OIDS=FALSE
	);

	CREATE TABLE "work_day" (
		"id" uuid DEFAULT uuid_generate_v4() NOT NULL,
		"is_free" BOOLEAN NOT NULL,
		"date" DATE NOT NULL,
		"user_id" uuid NOT NULL,
		CONSTRAINT "work_day_pk" PRIMARY KEY ("id")
	) WITH (
	OIDS=FALSE
	);

	CREATE TABLE "project" (
		"id" uuid DEFAULT uuid_generate_v4() NOT NULL,
		"min_daily_hours" numeric NOT NULL,
		"max_weekly_hours" numeric NOT NULL,
		"reminder_enabled" bool NOT NULL,
		"project_manager_id" uuid NOT NULL,
		"name" varchar(255) NOT NULL,
		"firm_id" uuid NOT NULL,
		"description" varchar(255) NOT NULL,
		"budget" numeric NOT NULL,
		"due_date" DATE NOT NULL,
		CONSTRAINT "project_pk" PRIMARY KEY ("id")
	) WITH (
	OIDS=FALSE
	);

	CREATE TABLE "team" (
		"id" uuid DEFAULT uuid_generate_v4() NOT NULL,
		"team_lead_id" uuid NOT NULL,
		"name" varchar(255) NOT NULL,
		"description" varchar(255) NOT NULL,
		"project_id" uuid NOT NULL,
		CONSTRAINT "team_pk" PRIMARY KEY ("id")
	) WITH (
	OIDS=FALSE
	);

	CREATE TABLE "checkpoint" (
		"id" uuid DEFAULT uuid_generate_v4() NOT NULL,
		"date" DATE NOT NULL,
		"project_id" uuid NOT NULL,
		"description" varchar(255),
		"name" varchar(255) NOT NULL,
		CONSTRAINT "checkpoint_pk" PRIMARY KEY ("id")
	) WITH (
	OIDS=FALSE
	);

	CREATE TABLE "checklist_item" (
		"id" uuid DEFAULT uuid_generate_v4() NOT NULL,
		"checklist_id" uuid NOT NULL,
		"description" varchar(255) NOT NULL,
		"is_checked" BOOLEAN NOT NULL,
		CONSTRAINT "checklist_item_pk" PRIMARY KEY ("id")
	) WITH (
	OIDS=FALSE
	);

	CREATE TABLE "firm" (
		"id" uuid DEFAULT uuid_generate_v4() NOT NULL,
		"admin_id" uuid NOT NULL,
		"name" varchar(255) NOT NULL,
		"model_id" bigint NOT NULL,
		CONSTRAINT "firm_pk" PRIMARY KEY ("id")
	) WITH (
	OIDS=FALSE
	);

	CREATE TABLE "checklist" (
		"id" uuid DEFAULT uuid_generate_v4() NOT NULL,
		"checkpoint_id" uuid NOT NULL,
		"name" varchar(255) NOT NULL,
		CONSTRAINT "checklist_pk" PRIMARY KEY ("id")
	) WITH (
	OIDS=FALSE
	);

	CREATE TABLE "permission" (
		"name" varchar(255) NOT NULL UNIQUE,
		"id" uuid DEFAULT uuid_generate_v4() NOT NULL,
		"role_id" int NOT NULL,
		CONSTRAINT "permission_pk" PRIMARY KEY ("id")
	) WITH (
	OIDS=FALSE
	);

	CREATE TABLE "business_model" (
		"id" bigint NOT NULL,
		"name" varchar(255) NOT NULL,
		CONSTRAINT "business_model_pk" PRIMARY KEY ("id")
	) WITH (
	OIDS=FALSE
	);

	CREATE TABLE "team-team_member" (
		"team_member_id" uuid DEFAULT uuid_generate_v4() NOT NULL,
		"team_id" uuid NOT NULL,
		CONSTRAINT "team-team_member_pk" PRIMARY KEY ("team_member_id","team_id")
	) WITH (
	OIDS=FALSE
	);

	CREATE TABLE "firm-user" (
		"firm_id" uuid NOT NULL,
		"user_id" uuid NOT NULL,
		"firm_user_email" varchar(255) NOT NULL,
		CONSTRAINT "firm-user_pk" PRIMARY KEY ("firm_id","user_id")
	) WITH (
	OIDS=FALSE
	);

	CREATE TABLE "user-user_role-firm" (
		"user_id" uuid NOT NULL,
		"role_id" bigint NOT NULL,
		"firm_id" uuid NOT NULL,
		CONSTRAINT "user-user_role-firm_pk" PRIMARY KEY ("user_id","role_id","firm_id")
	) WITH (
	OIDS=FALSE
	);

	ALTER TABLE "work_hour" ADD CONSTRAINT "work_hour_fk0" FOREIGN KEY ("work_day_id") REFERENCES "work_day"("id") ON DELETE CASCADE;
	ALTER TABLE "work_hour" ADD CONSTRAINT "work_hour_fk1" FOREIGN KEY ("project_id") REFERENCES "project"("id") ON DELETE CASCADE;

	ALTER TABLE "work_day" ADD CONSTRAINT "work_day_fk0" FOREIGN KEY ("user_id") REFERENCES "user"("id") ON DELETE CASCADE;

	ALTER TABLE "project" ADD CONSTRAINT "project_fk0" FOREIGN KEY ("project_manager_id") REFERENCES "user"("id");
	ALTER TABLE "project" ADD CONSTRAINT "project_fk1" FOREIGN KEY ("firm_id") REFERENCES "firm"("id") ON DELETE CASCADE;

	ALTER TABLE "team" ADD CONSTRAINT "team_fk0" FOREIGN KEY ("team_lead_id") REFERENCES "user"("id");
	ALTER TABLE "team" ADD CONSTRAINT "team_fk1" FOREIGN KEY ("project_id") REFERENCES "project"("id") ON DELETE CASCADE;

	ALTER TABLE "checkpoint" ADD CONSTRAINT "checkpoint_fk0" FOREIGN KEY ("project_id") REFERENCES "project"("id") ON DELETE CASCADE;

	ALTER TABLE "checklist_item" ADD CONSTRAINT "checklist_item_fk0" FOREIGN KEY ("checklist_id") REFERENCES "checklist"("id") ON DELETE CASCADE;

	ALTER TABLE "firm" ADD CONSTRAINT "firm_fk0" FOREIGN KEY ("admin_id") REFERENCES "user"("id") ON DELETE CASCADE;
	ALTER TABLE "firm" ADD CONSTRAINT "firm_fk1" FOREIGN KEY ("model_id") REFERENCES "business_model"("id");

	ALTER TABLE "checklist" ADD CONSTRAINT "checklist_fk0" FOREIGN KEY ("checkpoint_id") REFERENCES "checkpoint"("id") ON DELETE CASCADE;

	ALTER TABLE "permission" ADD CONSTRAINT "permission_fk0" FOREIGN KEY ("role_id") REFERENCES "role"("id");

	ALTER TABLE "team-team_member" ADD CONSTRAINT "team-team_member_fk0" FOREIGN KEY ("team_member_id") REFERENCES "user"("id") ON DELETE CASCADE;
	ALTER TABLE "team-team_member" ADD CONSTRAINT "team-team_member_fk1" FOREIGN KEY ("team_id") REFERENCES "team"("id") ON DELETE CASCADE;

	ALTER TABLE "firm-user" ADD CONSTRAINT "firm-user_fk0" FOREIGN KEY ("firm_id") REFERENCES "firm"("id") ON DELETE CASCADE;
	ALTER TABLE "firm-user" ADD CONSTRAINT "firm-user_fk1" FOREIGN KEY ("user_id") REFERENCES "user"("id") ON DELETE CASCADE;

	ALTER TABLE "user-user_role-firm" ADD CONSTRAINT "user-user_role-firm_fk0" FOREIGN KEY ("user_id") REFERENCES "user"("id") ON DELETE CASCADE;
	ALTER TABLE "user-user_role-firm" ADD CONSTRAINT "user-user_role-firm_fk1" FOREIGN KEY ("role_id") REFERENCES "role"("id");
	ALTER TABLE "user-user_role-firm" ADD CONSTRAINT "user-user_role-firm_fk2" FOREIGN KEY ("firm_id") REFERENCES "firm"("id") ON DELETE CASCADE;

	INSERT INTO "user" (id, username, password, first_name, last_name, hourly_rate, phone_number, is_registered) VALUES ('2cb5efd1-5e89-4713-9953-4b9457f07d02', 'matosic', '\$2a\$10\$riCn0j46F0UYMsiyf6ENW.ZuiV5AbpQvjj9puiKTvDJ/Rp1yRvDHG', 'string', 'string', 0, 'string', true);
  INSERT INTO "user" (id, username, password, first_name, last_name, hourly_rate, phone_number, is_registered) VALUES ('3ae6d543-b6f8-4b40-a92a-bfceb2fb83de', 'nadoveza', '\$2a\$10\$riCn0j46F0UYMsiyf6ENW.ZuiV5AbpQvjj9puiKTvDJ/Rp1yRvDHG', 'Petar', 'Nadoveza', 50, '981232431', false);
  INSERT INTO "user" (id, username, password, first_name, last_name, hourly_rate, phone_number, is_registered) VALUES ('a1bfbb1d-94c4-4f85-b12a-0bf1245455ac', 'bencic', '\$2a\$10\$riCn0j46F0UYMsiyf6ENW.ZuiV5AbpQvjj9puiKTvDJ/Rp1yRvDHG', 'Ljubo', 'Benčić', 75, '981231324', false);
  INSERT INTO "user" (id, username, password, first_name, last_name, hourly_rate, phone_number, is_registered) VALUES ('3ce60415-49b7-43d8-8c47-25fa34db26d3', 'lemesic', '\$2a\$10\$riCn0j46F0UYMsiyf6ENW.ZuiV5AbpQvjj9puiKTvDJ/Rp1yRvDHG', 'Leo', 'Lemešić', 75, '981232134', false);
  INSERT INTO "user" (id, username, password, first_name, last_name, hourly_rate, phone_number, is_registered) VALUES ('9921e10a-b9c6-4f15-8545-47485d213995', 'vukas', '\$2a\$10\$riCn0j46F0UYMsiyf6ENW.ZuiV5AbpQvjj9puiKTvDJ/Rp1yRvDHG', 'Bernard', 'Vukas', 50, '981231243', false);
  INSERT INTO "user" (id, username, password, first_name, last_name, hourly_rate, phone_number, is_registered) VALUES ('53176f8b-6c19-40c6-8336-38edf1934f39', 'adminTest', '\$2a\$10\$b7wa8NySe5NHIXqC3G.Dse9hxKmK3kCJcfIadipVGtffIEn00ZxUy', 'admin', 'test', 75, '0993024532', true);

	INSERT INTO business_model
	(id, name)
	VALUES 
	(1, 'starter');

	INSERT INTO business_model
	(id, name)
	VALUES 
	(2, 'professional');

	INSERT INTO business_model
	(id, name)
	VALUES 
	(3, 'enterprise');

	INSERT INTO firm
	(id, admin_id, name, model_id)
	VALUES 
	('4dd561c9-e98f-4679-b9c5-718ae85c14a3', '2cb5efd1-5e89-4713-9953-4b9457f07d02', 'Dummy', 3);
	
	INSERT INTO firm
	(id, admin_id, name, model_id)
	VALUES 
	('903bac99-4a97-4963-9aa8-da0c9caa884b', '53176f8b-6c19-40c6-8336-38edf1934f39', 'firmTest', 3);

	INSERT INTO firm
	(id, admin_id, name, model_id)
	VALUES 
	('53aa3bfc-f745-416f-a408-ec2136b22a09', '3ae6d543-b6f8-4b40-a92a-bfceb2fb83de', 'NadovezaAdmin', 3);


	INSERT INTO "role"
	(id, name)
	VALUES 
	(11, 'Admin');

	INSERT INTO "role"
	(id, name)
	VALUES 
	(12, 'Project Manager');

	INSERT INTO "role"
	(id, name)
	VALUES 
	(13, 'Team Lead');

	INSERT INTO "role"
	(id, name)
	VALUES 
	(14, 'Worker');

  INSERT INTO "permission" (name, id, role_id) VALUES ('CreateUser', '1130e661-b365-40b4-bad0-4b47d923c92c', 11);
  INSERT INTO "permission" (name, id, role_id) VALUES ('CreateProject', 'f7793034-6dd8-4197-bca2-d6a921406a90', 12);
  INSERT INTO "permission" (name, id, role_id) VALUES ('DeleteProject', 'eb1c30d0-7301-4698-94ee-625c0fd83d7c', 12);
  INSERT INTO "permission" (name, id, role_id) VALUES ('CreateTeam', '346b3df5-5cd0-419f-8926-40975a7fdea1', 13);
  INSERT INTO "permission" (name, id, role_id) VALUES ('DeleteTeam', 'f3b45f1a-ca23-403b-bf21-9e7e86a3ef1d', 13);
  INSERT INTO "permission" (name, id, role_id) VALUES ('ListUsers', '947d210e-52dd-4503-bccd-037c2191542f', 14);
  INSERT INTO "permission" (name, id, role_id) VALUES ('WorkDay', '738b0ec3-775c-45fc-b89f-068c53eb50d9', 14);
  INSERT INTO "permission" (name, id, role_id) VALUES ('WorkHour', 'c77f1744-0bab-4b26-a671-45ecf7333e1b', 14);
  INSERT INTO "permission" (name, id, role_id) VALUES ('EditUser', '4c532520-e2ef-426d-abd7-3652b59d0206', 14);
  INSERT INTO "permission" (name, id, role_id) VALUES ('DeleteUsers', '002fd2ce-d8ef-4f6a-9881-e2d0234e0631', 11);
  INSERT INTO "permission" (name, id, role_id) VALUES ('UpdateProject', '05759353-751b-4a30-b37d-ce863a8499a9', 12);
  INSERT INTO "permission" (name, id, role_id) VALUES ('ListProjects', 'b29e08d8-9af0-407f-9899-0c97ff324ea7', 14);
  INSERT INTO "permission" (name, id, role_id) VALUES ('UpdateChecklist', '566974c9-4823-4698-89de-549f4906f2a6', 12);
  INSERT INTO "permission" (name, id, role_id) VALUES ('CreateChecklist', '228c2124-3e7c-41b0-97a7-09d1be9aa8d2', 12);
  INSERT INTO "permission" (name, id, role_id) VALUES ('CreateCheckpoint', '92a94f0c-d0e1-43c8-987d-0248887ee1c7', 12);
  INSERT INTO "permission" (name, id, role_id) VALUES ('UpdateCheckpoint', 'ccb07dcb-7288-442a-89be-677c7fb50ad9', 12);
  INSERT INTO "permission" (name, id, role_id) VALUES ('DeleteCheckpoint', '0c35f786-4747-4c2a-99af-0f5bf003e0aa', 12);
  INSERT INTO "permission" (name, id, role_id) VALUES ('DeleteChecklist', '987050aa-9bd0-4253-a785-b05486e88646', 12);
  INSERT INTO "permission" (name, id, role_id) VALUES ('UpdateTeam', 'bed71213-b0b9-4d91-b8e2-d489d042418a', 12);
  INSERT INTO "permission" (name, id, role_id) VALUES ('UserStatistics', '07814cf4-096d-4796-880e-cc335c026445', 14);
  INSERT INTO "permission" (name, id, role_id) VALUES ('ProjectStatistics', 'ee199aeb-8a9f-4441-8349-d17316e6f715', 12);
  INSERT INTO "permission" (name, id, role_id) VALUES ('ListWorkDays', 'f5e9a455-0244-4465-9c8d-e0440f23fcc5', 14);
  INSERT INTO "permission" (name, id, role_id) VALUES ('UpdateWorkDays', 'c6678997-5957-412e-893f-24612e2bb6b6', 14);

	INSERT INTO "user-user_role-firm"
	(user_id, role_id, firm_id)
	VALUES
	('2cb5efd1-5e89-4713-9953-4b9457f07d02', '11', '4dd561c9-e98f-4679-b9c5-718ae85c14a3');

	INSERT INTO "user-user_role-firm"
	(user_id, role_id, firm_id)
	VALUES
	('3ce60415-49b7-43d8-8c47-25fa34db26d3', '12', '4dd561c9-e98f-4679-b9c5-718ae85c14a3');

	INSERT INTO "user-user_role-firm"
	(user_id, role_id, firm_id)
	VALUES
	('3ce60415-49b7-43d8-8c47-25fa34db26d3', '13', '4dd561c9-e98f-4679-b9c5-718ae85c14a3');

	INSERT INTO "user-user_role-firm"
	(user_id, role_id, firm_id)
	VALUES
	('a1bfbb1d-94c4-4f85-b12a-0bf1245455ac', '12', '4dd561c9-e98f-4679-b9c5-718ae85c14a3');

	INSERT INTO "user-user_role-firm"
	(user_id, role_id, firm_id)
	VALUES
	('9921e10a-b9c6-4f15-8545-47485d213995', '14', '4dd561c9-e98f-4679-b9c5-718ae85c14a3');

	INSERT INTO "user-user_role-firm"
	(user_id, role_id, firm_id)
	VALUES
	('3ae6d543-b6f8-4b40-a92a-bfceb2fb83de', '14', '4dd561c9-e98f-4679-b9c5-718ae85c14a3');

	INSERT INTO "user-user_role-firm"
	(user_id, role_id, firm_id)
	VALUES
	('53176f8b-6c19-40c6-8336-38edf1934f39', '11', '903bac99-4a97-4963-9aa8-da0c9caa884b');

	INSERT INTO "user-user_role-firm"
	(user_id, role_id, firm_id)
	VALUES
	('53176f8b-6c19-40c6-8336-38edf1934f39', '12', '903bac99-4a97-4963-9aa8-da0c9caa884b');

	INSERT INTO "user-user_role-firm"
	(user_id, role_id, firm_id)
	VALUES
	('53176f8b-6c19-40c6-8336-38edf1934f39', '13', '903bac99-4a97-4963-9aa8-da0c9caa884b');

	INSERT INTO "user-user_role-firm"
	(user_id, role_id, firm_id)
	VALUES
	('53176f8b-6c19-40c6-8336-38edf1934f39', '14', '903bac99-4a97-4963-9aa8-da0c9caa884b');

  INSERT INTO "user-user_role-firm"
	(user_id, role_id, firm_id)
	VALUES
	('2cb5efd1-5e89-4713-9953-4b9457f07d02', '14', '4dd561c9-e98f-4679-b9c5-718ae85c14a3');

  INSERT INTO "user-user_role-firm"
	(user_id, role_id, firm_id)
	VALUES
	('3ae6d543-b6f8-4b40-a92a-bfceb2fb83de', '11', '53aa3bfc-f745-416f-a408-ec2136b22a09');

  INSERT INTO "user-user_role-firm"
	(user_id, role_id, firm_id)
	VALUES
	('3ae6d543-b6f8-4b40-a92a-bfceb2fb83de', '14', '53aa3bfc-f745-416f-a408-ec2136b22a09');

  INSERT INTO "user-user_role-firm"
	(user_id, role_id, firm_id)
	VALUES
	('2cb5efd1-5e89-4713-9953-4b9457f07d02', '14', '53aa3bfc-f745-416f-a408-ec2136b22a09');

  INSERT INTO "user-user_role-firm"
	(user_id, role_id, firm_id)
	VALUES
	('3ce60415-49b7-43d8-8c47-25fa34db26d3', '14', '4dd561c9-e98f-4679-b9c5-718ae85c14a3');

  INSERT INTO "user-user_role-firm"
	(user_id, role_id, firm_id)
	VALUES
	('a1bfbb1d-94c4-4f85-b12a-0bf1245455ac', '14', '4dd561c9-e98f-4679-b9c5-718ae85c14a3');


	INSERT INTO project
	(id, min_daily_hours, max_weekly_hours, reminder_enabled, project_manager_id, name, firm_id, description, budget, due_date)
	VALUES 
	('f084b347-267d-46a5-b6d6-77ba8e806050', 2, 70, true, '3ce60415-49b7-43d8-8c47-25fa34db26d3', 'Tracker', '4dd561c9-e98f-4679-b9c5-718ae85c14a3', 'Aplication for animal tracking', 25000, '2021-04-01');

	INSERT INTO project
	(id, min_daily_hours, max_weekly_hours, reminder_enabled, project_manager_id, name, firm_id, description, budget, due_date)
	VALUES 
	('04c07a45-abb8-4a11-9a85-f3a9374c0dfc', 1, 75, false, 'a1bfbb1d-94c4-4f85-b12a-0bf1245455ac', 'Fornax', '4dd561c9-e98f-4679-b9c5-718ae85c14a3', 'Aplication for dummies', 25000, '2021-05-01');

	INSERT INTO project
	(id, min_daily_hours, max_weekly_hours, reminder_enabled, project_manager_id, name, firm_id, description, budget, due_date)
	VALUES 
	('5625d36e-44f2-11eb-b378-0242ac130002', 5, 80, true, '53176f8b-6c19-40c6-8336-38edf1934f39', 'Test', '903bac99-4a97-4963-9aa8-da0c9caa884b', 'testing', 75000, '2021-07-01');

	INSERT INTO team
	(id, team_lead_id, name, description, project_id)
	VALUES 
	('c86fcfa3-6d61-418d-93b7-2c03fb834dd5', '3ce60415-49b7-43d8-8c47-25fa34db26d3', 'BackendTeam', 'Team for back-end', 'f084b347-267d-46a5-b6d6-77ba8e806050');

	INSERT INTO "team-team_member"
	(team_member_id, team_id)
	VALUES ('3ce60415-49b7-43d8-8c47-25fa34db26d3', 'c86fcfa3-6d61-418d-93b7-2c03fb834dd5');

	INSERT INTO "team-team_member"
	(team_member_id, team_id)
	VALUES ('9921e10a-b9c6-4f15-8545-47485d213995', 'c86fcfa3-6d61-418d-93b7-2c03fb834dd5');

	INSERT INTO "team-team_member"
	(team_member_id, team_id)
	VALUES ('3ae6d543-b6f8-4b40-a92a-bfceb2fb83de', 'c86fcfa3-6d61-418d-93b7-2c03fb834dd5');

	INSERT INTO "firm-user"
	(firm_id, user_id, firm_user_email)
	VALUES 
	('4dd561c9-e98f-4679-b9c5-718ae85c14a3', '2cb5efd1-5e89-4713-9953-4b9457f07d02', 'matosic@dummy.hr');

	INSERT INTO "firm-user"
	(firm_id, user_id, firm_user_email)
	VALUES 
	('4dd561c9-e98f-4679-b9c5-718ae85c14a3', '3ce60415-49b7-43d8-8c47-25fa34db26d3', 'lemesic@dummy.hr');

	INSERT INTO "firm-user"
	(firm_id, user_id, firm_user_email)
	VALUES 
	('4dd561c9-e98f-4679-b9c5-718ae85c14a3', 'a1bfbb1d-94c4-4f85-b12a-0bf1245455ac', 'bencic@dummy.hr');

	INSERT INTO "firm-user"
	(firm_id, user_id, firm_user_email)
	VALUES 
	('4dd561c9-e98f-4679-b9c5-718ae85c14a3', '9921e10a-b9c6-4f15-8545-47485d213995', 'vukas@dummy.hr');

	INSERT INTO "firm-user"
	(firm_id, user_id, firm_user_email)
	VALUES 
	('4dd561c9-e98f-4679-b9c5-718ae85c14a3', '3ae6d543-b6f8-4b40-a92a-bfceb2fb83de', 'nadoveza@dummy.hr');

	INSERT INTO "firm-user"
	(firm_id, user_id, firm_user_email)
	VALUES 
	('903bac99-4a97-4963-9aa8-da0c9caa884b', '53176f8b-6c19-40c6-8336-38edf1934f39', 'admin.test@gmail.com');

	INSERT INTO "firm-user"
	(firm_id, user_id, firm_user_email)
	VALUES 
	('53aa3bfc-f745-416f-a408-ec2136b22a09', '3ae6d543-b6f8-4b40-a92a-bfceb2fb83de', 'nadovezaAdmin@firma.com');

  INSERT INTO "firm-user"
	(firm_id, user_id, firm_user_email)
	VALUES 
	('53aa3bfc-f745-416f-a408-ec2136b22a09', '2cb5efd1-5e89-4713-9953-4b9457f07d02', 'matosic@nadoveza.com');


	INSERT INTO work_day
	(id, is_free, date, user_id)
	VALUES 
	('9ae09cc8-514a-4bbb-a289-69da873f9aba', false, '2020-12-01', '2cb5efd1-5e89-4713-9953-4b9457f07d02');

	INSERT INTO work_day
	(id, is_free, date, user_id)
	VALUES 
	('baf7a224-6580-40dd-ab8e-fb9b1e8eca81', false, '2020-12-02', '2cb5efd1-5e89-4713-9953-4b9457f07d02');

	INSERT INTO work_day
	(id, is_free, date, user_id)
	VALUES 
	('603b7ed4-abc5-47dc-be00-0705d12e6be4', false, '2020-12-03', '2cb5efd1-5e89-4713-9953-4b9457f07d02');

	INSERT INTO work_day
	(id, is_free, date, user_id)
	VALUES 
	('08597db5-3308-4093-adbe-f20e1b67857e', false, '2020-12-01', '3ce60415-49b7-43d8-8c47-25fa34db26d3');

	INSERT INTO work_day
	(id, is_free, date, user_id)
	VALUES 
	('f0933726-d0ac-438f-9023-c7f94d6db6fc', false, '2020-12-02', '3ce60415-49b7-43d8-8c47-25fa34db26d3');

	INSERT INTO work_day
	(id, is_free, date, user_id)
	VALUES 
	('eddf89c5-381a-41b0-8bc6-21ab2b533ba4', false, '2020-12-03', '3ce60415-49b7-43d8-8c47-25fa34db26d3');

	INSERT INTO work_day
	(id, is_free, date, user_id)
	VALUES 
	('f3acec12-f6bf-403c-9274-84640e1ac477', false, '2020-12-01', 'a1bfbb1d-94c4-4f85-b12a-0bf1245455ac');

	INSERT INTO work_day
	(id, is_free, date, user_id)
	VALUES 
	('46af870a-53e0-4b54-85db-80a359165659', false, '2020-12-02', 'a1bfbb1d-94c4-4f85-b12a-0bf1245455ac');

	INSERT INTO work_day
	(id, is_free, date, user_id)
	VALUES 
	('ed7807a9-9072-481f-a36f-24d4662338b9', false, '2020-12-03', 'a1bfbb1d-94c4-4f85-b12a-0bf1245455ac');

	INSERT INTO work_day
	(id, is_free, date, user_id)
	VALUES 
	('39a4e146-4368-48ef-86d2-cebc6e415414', false, '2020-12-01', '9921e10a-b9c6-4f15-8545-47485d213995');

	INSERT INTO work_day
	(id, is_free, date, user_id)
	VALUES 
	('bab26c13-c011-461a-b42f-719d83c3b66f', true, '2020-12-02', '9921e10a-b9c6-4f15-8545-47485d213995');

	INSERT INTO work_day
	(id, is_free, date, user_id)
	VALUES 
	('a703896b-c527-46b1-b665-6eeddde67257', false, '2020-12-03', '9921e10a-b9c6-4f15-8545-47485d213995');

	INSERT INTO work_day
	(id, is_free, date, user_id)
	VALUES 
	('ab484159-3104-4aed-af36-c2f6ff7e6062', false, '2020-12-01', '3ae6d543-b6f8-4b40-a92a-bfceb2fb83de');

	INSERT INTO work_day
	(id, is_free, date, user_id)
	VALUES 
	('c26f06c2-0150-474b-836b-a88b67a39d9e', true, '2020-12-02', '3ae6d543-b6f8-4b40-a92a-bfceb2fb83de');

	INSERT INTO work_day
	(id, is_free, date, user_id)
	VALUES 
	('c8f77025-7372-4604-b656-c64f11e740f2', false, '2020-12-03', '3ae6d543-b6f8-4b40-a92a-bfceb2fb83de');

	INSERT INTO work_day
	(id, is_free, date, user_id)
	VALUES 
	('c6a74cd0-44f2-11eb-b378-0242ac130002', false, '2021-01-15', '53176f8b-6c19-40c6-8336-38edf1934f39');

	INSERT INTO work_hour
	(id, description, work_day_id, project_id)
	VALUES 
	('d70219fc-50da-4cab-b75a-a40a5c1ad2ba', 'Description..1', '9ae09cc8-514a-4bbb-a289-69da873f9aba', 'f084b347-267d-46a5-b6d6-77ba8e806050');

	INSERT INTO work_hour
	(id, description, work_day_id, project_id)
	VALUES 
	('e3967ff9-1ad6-4528-a500-ec22e83cf3b8', 'Description..2', '9ae09cc8-514a-4bbb-a289-69da873f9aba', 'f084b347-267d-46a5-b6d6-77ba8e806050');

	INSERT INTO work_hour
	(id, description, work_day_id, project_id)
	VALUES 
	('e1f0169a-a564-4e36-8773-de1f07fb364f', 'Description..3', 'baf7a224-6580-40dd-ab8e-fb9b1e8eca81', 'f084b347-267d-46a5-b6d6-77ba8e806050');

	INSERT INTO work_hour
	(id, description, work_day_id, project_id)
	VALUES 
	('fe644ff8-31a1-4b60-89bd-6dec2316084e', 'Description..4', 'baf7a224-6580-40dd-ab8e-fb9b1e8eca81', 'f084b347-267d-46a5-b6d6-77ba8e806050');

	INSERT INTO work_hour
	(id, description, work_day_id, project_id)
	VALUES 
	('15ca6643-154a-4e49-97ed-0e88daa739b8', 'Description..5', '603b7ed4-abc5-47dc-be00-0705d12e6be4', 'f084b347-267d-46a5-b6d6-77ba8e806050');

	INSERT INTO work_hour
	(id, description, work_day_id, project_id)
	VALUES 
	('92c37fe4-0c7e-41c1-af83-cdc0f5292a97', 'Description..6', '603b7ed4-abc5-47dc-be00-0705d12e6be4', 'f084b347-267d-46a5-b6d6-77ba8e806050');

	INSERT INTO work_hour
	(id, description, work_day_id, project_id)
	VALUES 
	('8eaa368c-6aa4-4152-a2d2-03f205c87ee5', 'Description..7', '08597db5-3308-4093-adbe-f20e1b67857e', 'f084b347-267d-46a5-b6d6-77ba8e806050');

	INSERT INTO work_hour
	(id, description, work_day_id, project_id)
	VALUES 
	('6a51948b-2e40-4ddb-9d06-3e0101576655', 'Description..8', '08597db5-3308-4093-adbe-f20e1b67857e', 'f084b347-267d-46a5-b6d6-77ba8e806050');

	INSERT INTO work_hour
	(id, description, work_day_id, project_id)
	VALUES 
	('7460add9-351c-496d-8298-aafee2d629fb', 'Description..9', 'f0933726-d0ac-438f-9023-c7f94d6db6fc', 'f084b347-267d-46a5-b6d6-77ba8e806050');

	INSERT INTO work_hour
	(id, description, work_day_id, project_id)
	VALUES 
	('fc05c19e-723f-41d1-933d-2676e6981a97', 'Description..10', 'f0933726-d0ac-438f-9023-c7f94d6db6fc', 'f084b347-267d-46a5-b6d6-77ba8e806050');

	INSERT INTO work_hour
	(id, description, work_day_id, project_id)
	VALUES 
	('426a8371-5973-4ef3-ba3d-d88b8c2d02c5', 'Description..11', 'eddf89c5-381a-41b0-8bc6-21ab2b533ba4', 'f084b347-267d-46a5-b6d6-77ba8e806050');

	INSERT INTO work_hour
	(id, description, work_day_id, project_id)
	VALUES 
	('81fc451a-8453-4fc5-8ac0-9dd6c2fefc79', 'Description..12', 'eddf89c5-381a-41b0-8bc6-21ab2b533ba4', 'f084b347-267d-46a5-b6d6-77ba8e806050');

	INSERT INTO work_hour
	(id, description, work_day_id, project_id)
	VALUES 
	('61ae2560-8809-4604-b763-efecce3e046b', 'Description..13', 'f3acec12-f6bf-403c-9274-84640e1ac477', 'f084b347-267d-46a5-b6d6-77ba8e806050');

	INSERT INTO work_hour
	(id, description, work_day_id, project_id)
	VALUES 
	('39d8d872-d446-4beb-a77e-aeb2becf352c', 'Description..14', 'f3acec12-f6bf-403c-9274-84640e1ac477', 'f084b347-267d-46a5-b6d6-77ba8e806050');

	INSERT INTO work_hour
	(id, description, work_day_id, project_id)
	VALUES 
	('24255924-2351-46b3-9604-060b0b7970fb', 'Description..15', '46af870a-53e0-4b54-85db-80a359165659', 'f084b347-267d-46a5-b6d6-77ba8e806050');

	INSERT INTO work_hour
	(id, description, work_day_id, project_id)
	VALUES 
	('0dc0e879-258f-4795-88b2-8aa02515ce76', 'Description..16', '46af870a-53e0-4b54-85db-80a359165659', 'f084b347-267d-46a5-b6d6-77ba8e806050');

	INSERT INTO work_hour
	(id, description, work_day_id, project_id)
	VALUES 
	('19a6e275-79fd-4fb8-a1e3-08c3c0c3fd77', 'Description..17', 'ed7807a9-9072-481f-a36f-24d4662338b9', 'f084b347-267d-46a5-b6d6-77ba8e806050');

	INSERT INTO work_hour
	(id, description, work_day_id, project_id)
	VALUES 
	('7654ed01-1c7f-4e28-98a6-b327510b1e4f', 'Description..18', 'ed7807a9-9072-481f-a36f-24d4662338b9', 'f084b347-267d-46a5-b6d6-77ba8e806050');

	INSERT INTO work_hour
	(id, description, work_day_id, project_id)
	VALUES 
	('34f7cd27-2ea8-404d-bcce-997aeabd524e', 'Description..19', '39a4e146-4368-48ef-86d2-cebc6e415414', 'f084b347-267d-46a5-b6d6-77ba8e806050');

	INSERT INTO work_hour
	(id, description, work_day_id, project_id)
	VALUES 
	('864048fd-dc78-443c-b747-05459264cf80', 'Description..20', '39a4e146-4368-48ef-86d2-cebc6e415414', 'f084b347-267d-46a5-b6d6-77ba8e806050');

	INSERT INTO work_hour
	(id, description, work_day_id, project_id)
	VALUES 
	('d21e9949-cad7-4a46-83c0-769466e3756e', 'Description..21', 'bab26c13-c011-461a-b42f-719d83c3b66f', 'f084b347-267d-46a5-b6d6-77ba8e806050');

	INSERT INTO work_hour
	(id, description, work_day_id, project_id)
	VALUES 
	('87d9826c-00e7-4697-b67e-63e9d9e8f3ed', 'Description..22', 'bab26c13-c011-461a-b42f-719d83c3b66f', 'f084b347-267d-46a5-b6d6-77ba8e806050');

	INSERT INTO work_hour
	(id, description, work_day_id, project_id)
	VALUES 
	('5ac81d67-f04a-4f6b-82a9-2a503e1b7291', 'Description..23', 'a703896b-c527-46b1-b665-6eeddde67257', 'f084b347-267d-46a5-b6d6-77ba8e806050');

	INSERT INTO work_hour
	(id, description, work_day_id, project_id)
	VALUES 
	('4bab64c0-99af-4a2e-bc84-cd6c55d442dd', 'Description..24', 'a703896b-c527-46b1-b665-6eeddde67257', 'f084b347-267d-46a5-b6d6-77ba8e806050');

	INSERT INTO work_hour
	(id, description, work_day_id, project_id)
	VALUES 
	('f21c5dcd-a071-487e-b002-5bbdfc25636d', 'Description..25', 'ab484159-3104-4aed-af36-c2f6ff7e6062', 'f084b347-267d-46a5-b6d6-77ba8e806050');

	INSERT INTO work_hour
	(id, description, work_day_id, project_id)
	VALUES 
	('3f37b1d0-650c-46dd-8c9e-4f3865f16102', 'Description..26', 'ab484159-3104-4aed-af36-c2f6ff7e6062', 'f084b347-267d-46a5-b6d6-77ba8e806050');

	INSERT INTO work_hour
	(id, description, work_day_id, project_id)
	VALUES 
	('6213792e-6f3d-4319-a0b8-949b42215aa3', 'Description..27', 'c26f06c2-0150-474b-836b-a88b67a39d9e', 'f084b347-267d-46a5-b6d6-77ba8e806050');

	INSERT INTO work_hour
	(id, description, work_day_id, project_id)
	VALUES 
	('01c21df7-6634-4d42-bcc6-4745d6594a1b', 'Description..28', 'c26f06c2-0150-474b-836b-a88b67a39d9e', 'f084b347-267d-46a5-b6d6-77ba8e806050');

	INSERT INTO work_hour
	(id, description, work_day_id, project_id)
	VALUES 
	('c4a1ecee-90cd-4c90-9857-e5538889e64b', 'Description..29', 'c8f77025-7372-4604-b656-c64f11e740f2', 'f084b347-267d-46a5-b6d6-77ba8e806050');

	INSERT INTO work_hour
	(id, description, work_day_id, project_id)
	VALUES 
	('c9ea6e5c-eeeb-457f-a46d-9a7f7c9de5a6', 'Description..30', 'c8f77025-7372-4604-b656-c64f11e740f2', 'f084b347-267d-46a5-b6d6-77ba8e806050');

	INSERT INTO "checkpoint"
	(id, date, project_id, description, name)
	VALUES 
	('31c055b1-4baa-4872-ae66-82419abab7d3', '2020-12-01', 'f084b347-267d-46a5-b6d6-77ba8e806050', 'Description checpoint 1', 'Main functionalities');

	INSERT INTO "checkpoint"
	(id, date, project_id, description, name)
	VALUES 
	('a5a0a0e4-85b0-4b2c-8d82-bab46667ccb8', '2020-12-02', 'f084b347-267d-46a5-b6d6-77ba8e806050', 'Description checpoint 2', 'Advanced functionalities');

	INSERT INTO "checkpoint"
	(id, date, project_id, description, name)
	VALUES 
	('e5ab6da8-2fcc-4126-b943-15f0fce8306b', '2020-12-01', '04c07a45-abb8-4a11-9a85-f3a9374c0dfc', 'Description checpoint 1', 'Main functionalities');

	INSERT INTO "checkpoint"
	(id, date, project_id, description, name)
	VALUES 
	('92d5e7de-af8a-47e7-81ed-ff6dfc6495e0', '2020-12-02', '04c07a45-abb8-4a11-9a85-f3a9374c0dfc', 'Description checpoint 1', 'Advanced functionalities');

	INSERT INTO checklist
	(id, checkpoint_id, name)
	VALUES 
	('d85fcb65-1efa-4512-8a6a-b3af42ff1d1b', '31c055b1-4baa-4872-ae66-82419abab7d3', 'Checklist1');

	INSERT INTO checklist
	(id, checkpoint_id, name)
	VALUES 
	('c32285db-d9df-495a-9b85-c676face3c4d', 'a5a0a0e4-85b0-4b2c-8d82-bab46667ccb8', 'Checklist2');

	INSERT INTO checklist
	(id, checkpoint_id, name)
	VALUES 
	('4dd16cc4-347d-4e3f-b7ea-089f67283eaf', 'e5ab6da8-2fcc-4126-b943-15f0fce8306b', 'Checklist3');

	INSERT INTO checklist
	(id, checkpoint_id, name)
	VALUES 
	('73e6e6a0-27de-42a1-9472-514b4cbd10bb', '92d5e7de-af8a-47e7-81ed-ff6dfc6495e0', 'Checklist4');

	INSERT INTO checklist_item
	(id, checklist_id, description, is_checked)
	VALUES 
	('90b355bb-16ff-4f05-9bc5-3f7b50d2be81', 'd85fcb65-1efa-4512-8a6a-b3af42ff1d1b', 'Checklist description 1', false);

	INSERT INTO checklist_item
	(id, checklist_id, description, is_checked)
	VALUES 
	('8b3f75d5-f4d9-4020-aaca-676bc8bb2bec', 'd85fcb65-1efa-4512-8a6a-b3af42ff1d1b', 'Checklist description 2', false);

	INSERT INTO checklist_item
	(id, checklist_id, description, is_checked)
	VALUES 
	('e419b962-f7ba-46db-96c7-d79059d8fedb', 'c32285db-d9df-495a-9b85-c676face3c4d', 'Checklist description 1', false);

	INSERT INTO checklist_item
	(id, checklist_id, description, is_checked)
	VALUES 
	('9080a2df-baae-4261-b01a-cc31f247416d', 'c32285db-d9df-495a-9b85-c676face3c4d', 'Checklist description 2', false);

	INSERT INTO checklist_item
	(id, checklist_id, description, is_checked)
	VALUES 
	('154b940c-ff54-4e98-91b3-329e42f1a949', '4dd16cc4-347d-4e3f-b7ea-089f67283eaf', 'Checklist description 1', false);

	INSERT INTO checklist_item
	(id, checklist_id, description, is_checked)
	VALUES 
	('abb3fb98-9da9-455f-a949-3e2d631439ad', '4dd16cc4-347d-4e3f-b7ea-089f67283eaf', 'Checklist description 2', false);

	INSERT INTO checklist_item
	(id, checklist_id, description, is_checked)
	VALUES 
	('1de221e0-1b3a-4605-aff3-14747582fd37', '73e6e6a0-27de-42a1-9472-514b4cbd10bb', 'Checklist description 1', false);

	INSERT INTO checklist_item
	(id, checklist_id, description, is_checked)
	VALUES 
	('2aaffb28-b0a8-4ca7-949f-dcf883665fea', '73e6e6a0-27de-42a1-9472-514b4cbd10bb', 'Checklist description 2', false);

EOSQL