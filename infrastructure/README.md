## Setup

Running the container:
```
docker-compose up
```

Running the container as a background (daemon) process:
```
docker-compose up -d
```

Shutting down the container:
```
docker-compose down
```

If database needs rebuilding:
```
docker-compose down --volumes
docker-compose up
```