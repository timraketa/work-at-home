# Work@Home

### The app is deployed on: http://206.189.60.2:3000/

## Running the app:

1. Infrastructure (database):

- `cd infrastructure && docker-compose up -d`
- Note: If using Linux run `chmod o+x init-db.sh` beforehand (inside /infrastructure)

2. Backend:

- copy the `/backend/work-at-home/.env.example` file to `/backend/work-at-home/.env`
- `cd backend/work-at-home && npm install && npm start`
- Api explorer can be viewed at http://localhost:3000/explorer

3. Frontend:

- copy the `/frontend/work-at-home/.env.example` file to `/frontend/work-at-home/.env`
- `cd frontend/work-at-home && npm install && npm start`


